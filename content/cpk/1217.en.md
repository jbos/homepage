---
cpk_affected: science users who want to change settings of their account or aliases
cpk_begin: &id001 2017-09-27 15:00:00
cpk_end: 2017-09-27 16:45:00
cpk_number: 1217
date: *id001
tags:
- medewerkers
- studenten
title: DIY down due to hardware problem
url: cpk/1217
---
The server of DHZ had a problem with the hardware, the hardware was
fixed on 2017-09-29, the website DHZ could be restored more quickly on a
different server, limiting the outage to a few hours.
