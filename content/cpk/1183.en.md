---
cpk_affected: 'Clusternodes: cn72, cn74, hinton'
cpk_begin: &id001 2016-07-05 06:00:00
cpk_end: 2016-07-05 09:45:00
cpk_number: 1183
date: *id001
title: Power distribution unit failed
url: cpk/1183
---
Three cluster nodes powered down due to a failed PDU. After replacing
the PDU, the nodes came up normally.
