---
cpk_affected: cluster-astro
cpk_begin: &id001 2017-03-12 22:17:00
cpk_end: 2017-03-13 09:08:00
cpk_number: 1200
date: *id001
tags:
- medewerkers
- studenten
title: Crash coma00 server
url: cpk/1200
---
Coma00 serves astro0, astro1, astro3 and astro4 volumes, the server
crashed on a kernel-BUG, Monday morning the server was restarted and
service resumed normally.
