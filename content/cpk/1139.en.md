---
cpk_affected: Users of network disks
cpk_begin: &id001 2015-08-06 08:45:00
cpk_end: 2015-08-06 09:45:00
cpk_number: 1139
date: *id001
title: Network shares unavailable during migration
url: cpk/1139
---
Starting from 8:45h, the network disks will not be available during a
migration to a new server.

Affected disks:

`45t, acfiles2, acs4, afric, afstud, afstudeerwerk,`
`afstudTest, albert, arb, bbb-priv, bin, bioalg, bioexamencie, birds, bko, botany-general,`
`bsweet, carta, ccs4, ccz, cczhelpdesk, cczsecr, celbio2, celbio3, CuDoTest, cursus,`
`cursusdossiers, dcnadmin, diafragma, ecogen, ecologie, embdata, evsf2data10, evsf2data15,`
`evsf2data16, evsf2data17, evsf2data18, evsf2data19, evsf2data7, evsf2data8, evsf2data9,`
`exoarchief, flaredisc, fsr, geminstr, gi1, gi3, gipsy, grafting, hfml-data, hfml-engineering,`
`hfml-secr, honours, huckgroup, hybrids, icissec, ifl, ifl-d1, imappdir, imappdir, imappsecr,`
`immbureau, iobw, iris, itt, iwwr1, kolkteaching, lambiek, localdist, logs, mailmanincludes,`
`mbaudit3, mbbioel, mbbioel2, mbcns, mbcortex, mbonderwijs, mb-read-write, mbsynid, mercator,`
`mestrelab, mi1, mi3, micro-bulk1, micro-bulk2, micro-labmembers, milkun4, milkun4rw, milkun8,`
`milkun9, mlf90, mlfalgemeen, mlfsec, molchem, molchem2, molchem3, molphtec, mol-secr, morph,`
`mpcoldmollab, mppstaf, multimedia, neuropi, nmr500data, nmrspectra, nskpromo, olcbiocie,`
`olcbiowetenschappen, ons, orgdier, owc, owc1, parp, pcb, petseq, plantkunde-hgl, practica,`
`project1mpp, rotax, seq, smol, snn, snn2, snn3, snn4, soheto, sos, spmdata1, SSNMR1,`
`studiereis2013, sucudo-transfer, tcbackup, tcscratch, tece, teceleiding, tf2devices, thchem,`
`tracegas, tracegastemp, tracelab, tracelabdb, unl, vcbio_prive, vergader, vsc16, vscxray,`
`wikis, wiskundetoernooi en zeegras`
