---
cpk_affected: Gebruikers van virtuele machines gehost door de gipphoenix
cpk_begin: &id001 2013-11-18 06:33:00
cpk_end: 2013-10-29 09:16:00
cpk_number: 1057
date: *id001
title: Netwerk probleem gipphoenix
url: cpk/1057
---
Het netwerk interface van de gipphounix kwam vanmorgen niet op. Na een
ifdown / ifup van eth2 had de machine weer een ipadres. De oorzaak van
deze storing is nog onbekend.
