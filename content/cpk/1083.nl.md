---
cpk_affected: Gebruikers van de printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-04-17 13:35:00
cpk_end: 2014-04-17 13:50:00
cpk_number: 1083
date: *id001
tags:
- medewerkers
- studenten
title: Na vervanging toch weer Print/phpMyAdmin server problemen
url: cpk/1083
---
Na de hardwarevervanging reageerde de server reageerde niet meer,
oorzaak onbekend. Na een herstart was het probleem verdwenen. Er wordt
nagedacht over manieren om dit probleem aan te pakken.
