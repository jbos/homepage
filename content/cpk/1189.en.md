---
cpk_affected: CN69  - CN83, archimedes, nauta, hubel, homedcc
cpk_begin: &id001 2016-11-14 20:40:00
cpk_end: 2016-11-14 22:50:00
cpk_number: 1189
date: *id001
title: Some machines down due to failed power distribution unit
url: cpk/1189
---
Due to a failed power distribution unit (PDU) a network switch powered
down. As a result of that the mentioned machines lost their network
connectivity. After reconnecting the switch to a different PDU, the
machines were reachable again.
