---
cpk_affected: Users of Eduroam and Ru-guest
cpk_begin: &id001 2014-05-19 20:00:00
cpk_end: 2014-05-19 23:59:00
cpk_number: 1092
date: *id001
tags:
- medewerkers
- studenten
title: Maintenance wireless infrastructure May 19
url: cpk/1092
---
At Monday May 19, a major upgrade of the wireless infrastructure and its
maintenance tool (called Prime) will take place by the ISC. Therefore
the wireless service (i.e. Eduroam) will be interrupted several times
from 8:00 pm. Existing wireless connections will be lost and new
connections will not be possible for some time. Due to the complexity of
this operation this maintenance will possibly be continued at Monday,
May 26, again with interruptions from 8:00 pm.
