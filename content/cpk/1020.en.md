---
cpk_affected: all FNWI users with a homedirectory on fileserver bundle
cpk_begin: &id001 2013-05-21 17:00:00
cpk_end: 2013-05-21 17:27:00
cpk_number: 1020
date: *id001
tags:
- medewerkers
- studenten
title: Homeserver bundle crashed
url: cpk/1020
---
The fileserver crashed. After a reboot everything was back to normal.
