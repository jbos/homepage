---
cpk_affected: Gebruikers met homedirectory op "bundle" (te zien op `<http://DHZ.science.ru.nl>`)
cpk_begin: &id001 2013-12-23 07:30:00
cpk_end: 2013-12-23 09:00:00
cpk_number: 1063
date: *id001
tags:
- medewerkers
- studenten
title: Uitgestelde vervanging van home-server "bundle"
url: cpk/1063
---
De vervanging van de oude homeserver “bundle” is uitgesteld en nu
definitief gepland op maandagochtend 23 december. Omdat de data ook nu
al gesynchroniseerd wordt naar de nieuwe server, zal er niet veel
downtijd zijn. De server is als het goed is erg betrouwbaar: hardware
[RAID-6](http://nl.wikipedia.org/wiki/Redundant_array_of_independent_disks#RAID-6),
dubbele processoren en voedingen en een 5-jaar onderhoudscontract van de
leverancier. De performance is o.a. verbeterd door het gebruik van
hardware-RAID met een 1 GB [schrijfcache met batterij
backup](http://serverfault.com/questions/65096/battery-backed-write-cache).
