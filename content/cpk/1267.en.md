---
cpk_affected: Eduroam users on campus
cpk_begin: 2020-07-01 00:00:00
cpk_end: 2020-07-10 00:00:00
cpk_number: 1267
title: Eduroam problem on campus
date: 2020-07-01 00:00:00
url: cpk/1267
---
The ISC announced: For security reasons, the certificate of the wifi
server will be replaced in the evening of Friday, July 10. This has
consequences for connecting your mobile device to Eduroam when you’re on
campus:

• If you get the message that you have to accept the new certificate to
use eduroam, choose 'yes'. You can then use eduroam again;

• If you don't get this message and can't connect to Eduroam, choose the
wireless network 'eduroam-config'. Accept the terms and conditions.
Follow the instructions to reinstall Eduroam.

More information can also be found at
[www.ru.nl/ict-uk/eduroam](https://www.ru.nl/ict-uk/eduroam) (you will
need an internet connection for this).

If you have any questions, please contact the ICT Helpdesk (024 - 36
22222).
