---
cpk_affected: Gebruikers van horde webmail en/of mailman mailinglists
cpk_begin: &id001 2012-07-12 09:09:00
cpk_end: 2012-07-12 14:00:00
cpk_number: 992
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: 'Deels aangekondigd onderhoud: mailman + horde webmail server down'
url: cpk/992
---
Vanochtend bleek horde webmail misbruikt te worden voor spam. Enkele
naieve gebruikers hadden hun Science-wachtwoord aan spammers gegeven,
waardoor dit mogelijk werd. Nadat eerst uitgezocht is welke gebruikers
dit precies waren en hun wachtwoord aangepast was, is besloten om meteen
ook een defecte cpu-ventilator te vervangen. Daardoor zal Mailman ook
getrofffen worden, van ca 13:00 uur tot ca 14:00 uur.
