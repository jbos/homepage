---
cpk_affected: Apparatuur met een dynamisch netwerkadres
cpk_begin: &id001 2018-06-09 07:00:00
cpk_end: 2018-06-12 10:49:00
cpk_number: 1232
date: *id001
tags:
- medewerkers
- studenten
title: Geen Internet (DHCP probleem voor dynamische adressen)
url: cpk/1232
---
Vorige week is de DHCP-infrastructuur bij FNWI gewijzigd om van oude
naar nieuwe servers te gaan. Een fout werd gemaakt toen we terug moesten
naar een oude server omdat DHCP niet goed liep op een virtueel adres.
Toen vanochtend de eerste klachten binnenkwamen, hebben we snel de fout
gevonden en gerepareerd. We proberen verdere serviceonderbrekingen te
voorkomen en tegelijkertijd de DHCP-service te verbeteren.
