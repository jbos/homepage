---
title: Eduroam wifi down during upgrade
author: petervc
cpk_number: 1301
cpk_begin: &id001 2022-10-20 23:30:00
cpk_end: 2022-10-21 01:30:00
onderhoud: true
cpk_affected: Eduroam wifi in many buildings, including Huygens and surrounding buildings
date: *id001
url: cpk/1301
---
ILS wifi management notified us that they will upgrade the software of wireless access points,
because security flaws have been addressed in the latest release.

The maintenance ends on 2022-10-21 01:30.

Source: [meldingen.ru.nl](https://meldingen.ru.nl/detail.php?id=1330&lang=en&tg=0&f=0&ii)
