---
cpk_affected: Users of shares/network isolator
cpk_begin: &id001 2016-03-07 06:30:00
cpk_end: 2016-03-07 09:00:00
cpk_number: 1170
date: *id001
title: Server isolator lacking private class networks
url: cpk/1170
---
For yet unknown reasons the private class network interfaces where
incorrectly numbered and activated Solution: Reboot
