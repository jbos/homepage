---
cpk_affected: CnCZ
cpk_begin: &id001 2016-03-07 06:30:00
cpk_end: 2016-03-07 07:45:00
cpk_number: 1169
date: *id001
title: emoe niet correct opgestart
url: cpk/1169
---
Tijdens reboot werd een file systeem (/opt) niet herkend, resulterend in
een kernel panic. Oplossing: Reboot
