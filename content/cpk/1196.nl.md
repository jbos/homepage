---
cpk_affected: Medewerkers met e-mail in de RU Exchange omgeving
cpk_begin: &id001 2017-02-06 09:39:00
cpk_end: 2017-02-06 09:58:00
cpk_number: 1196
date: *id001
tags:
- medewerkers
title: Storing in mail verzenden van RU Exchange
url: cpk/1196
---
Maandagochtend bleken enkele medewerkers, die via Outlook (Web Access)
gebruik maken van de RU Exchange omgeving, hun e-mail niet meer
succesvol te kunnen versturen naar adressen buiten de RU Exchange. Dit
bleek te liggen aan een tweetal RU-mailservers dat de week ervoor door
het IS was toegevoegd maar nog niet was toegestaan om e-mail buiten de
RU Exchange af te leveren. Inkomende mail, het lezen van e-mail of
gebruikers van b.v. Thunderbird hebben geen overlast hiervan
ondervonden.
