---
cpk_affected: gebruikers van GitLab en Mattermost, Licenties, DHZ
cpk_begin: &id001 2021-01-07 00:00:00
cpk_end: 2021-01-07 00:00:00
cpk_number: 1269
date: *id001
title: Switch crash; gitlab+mattermost, licenses and DHZ
url: cpk/1269
---
<itemTags>medewerkers, studenten</itemTags>


Door een eenvoudige beheershandeling crashte een switch (as-ak008-04)
die daarna met de hand gereset moest worden. De switch zit onder andere
tussen het netwerk en de servers voor gitlab+mattermost, de licenties,
en de database voor DHZ.
