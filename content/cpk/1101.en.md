---
cpk_affected: Users of shared network disks
cpk_begin: &id001 2014-08-15 12:04:00
cpk_end: 2014-08-15 14:25:00
cpk_number: 1101
date: *id001
tags:
- medewerkers
- studenten
title: Fileserver bulk problems
url: cpk/1101
---
The fileserver had problems, which were resolved after a server reboot.

Affected network shares:

`acfiles ARK-Backup Bargerveen Bargerveen-Gebruikers beevee bioniccell bio-orgchem`
`B-Ware-Arnica B-Ware-Dotter B-Ware-Erica B-Ware-Knolrus B-Ware-Lobelia B-Ware-Stratiotes`
`celbio1 celbio2 csgi-archief csg-staf cvi desda ds ehef1 evsf2dataoud evsf2schijf1`
`femtospin Floron geminstr2 giphouse gissig gmi hfml-backup hfml-backup2 highres`
`impuls introcie isis janvanhest kaartenbak kangoeroe microbiology microbiology2`
`milkun2 milkun3 milkun5 milkun6 milkun7 milkun7rw molbiol mwstudiereis neuroinf`
`neuroinf2 nfstest nwibackup olympus puc RandomWalkModel Ravon-Algemeen Ravon-Foto`
`Ravon-Projecten secres2 sigma sigmaalmanak sigmacies sigmaexchange sigmasymposium`
`splex spmdata3 spmdata4 spmdata5 spmdata6 spmdata7 stroom thalia ucm vsc vsc1`
`vsc10 vsc11 vsc12 vsc13 vsc14 vsc15 vsc2 vsc3 vsc4 vsc5 vsc6 vsc7 vsc8 vsc9`
`vscadmin wiskalg wiskunde`
