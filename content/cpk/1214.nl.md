---
cpk_affected: OpenVPN users
cpk_begin: &id001 2017-08-14 06:31:00
cpk_end: 2017-08-15 11:36:00
cpk_number: 1214
date: *id001
tags:
- medewerkers
- studenten
title: OpenVPN service niet gestart bij reboot
url: cpk/1214
---
Om onduidelijke redenen werd de OpenVPN service niet gestart bij de
maandagochtendreboot. De service is nu handmatig gestart.
