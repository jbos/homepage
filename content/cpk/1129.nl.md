---
cpk_affected: alle gebruikers die Science-mail willen lezen
cpk_begin: &id001 2015-02-16 23:00:00
cpk_end: 2015-02-17 07:45:00
cpk_number: 1129
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mailserver upgrade a.s. maandagnacht
url: cpk/1129
---
De IMAP server wordt a.s. maandagnacht vervangen door een nieuwe server.
Gedurende de nacht kan geen Science mail gelezen worden van de server,
omdat dan de meest recente mails gesynchroniseerd zullen worden naar de
nieuwe server. Mail kan wel verstuurd worden gedurende deze tijd, maar
nieuwe mail zal pas na 07:30 op de nieuwe server afgeleverd worden. N.B.
op de oude server kon men inloggen met de eerste 8 tekens van het
wachtwoord, op de nieuwe server moet het volledige wachtwoord ingetikt
worden.
