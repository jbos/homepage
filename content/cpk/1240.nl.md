---
cpk_affected: ' Ubuntu 12_04 LTS servers / Ubuntu 12 (of nog oudere) clients'
cpk_begin: &id001 2018-09-11 07:20:00
cpk_end: 2018-09-11 10:25:00
cpk_number: 1240
date: *id001
tags:
- medewerkers
- studenten
title: SSH connecties van te oude clients/servers
url: cpk/1240
---
Een configuratiewijziging die dinsdagochtend automatisch actief werd
zorgde voor een crash van de SSH daemon op Ubuntu 12\_04 LTS. Op maandag
was getest tegen Ubuntu 14\_04 LTS, Ubuntu 16\_04 LTS en Ubuntu 18\_04
LTS. Tevens ondervonden oudere SSH clients connectiviteitsproblemen maar
is het verzoek die te updaten zodat het [niveau van beveiliging voor SSH
verhoogd](https://wiki.mozilla.org/Security/Guidelines/OpenSSH) kan
worden binnen FNWI.
