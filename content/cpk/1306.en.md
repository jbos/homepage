---
title: Outgoing mail not possible after successful phishing attack
author: bram
cpk_number: 1306
cpk_begin: 2022-12-05 21:17:00
cpk_end: 2022-12-06 10:00:00
cpk_affected: Users of Science mail
date: 2022-12-06
tags: []
url: cpk/1306
---
After several persons fell for a phishing email, several tens of thousands of emails were sent via Science accounts. This causes our mail servers to be blocked by several mail servers on the internet.
We are currently cleaning SPAM mails that are still in the outgoing queues. After that, the mail servers will be made available again.
We are considering measures to prevent this form of nuisance.
