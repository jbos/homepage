---
cpk_affected: Gebruikers in Huygens vleugel 1 en straten tussens vleugels 1 en 3
cpk_begin: &id001 2014-01-13 19:00:00
cpk_end: 2014-01-13 23:59:00
cpk_number: 1066
date: *id001
tags:
- medewerkers
title: Netwerkonderbreking in Huygens vleugel 1
url: cpk/1066
---
Op maandagavond 13 januari tussen 19:00 - 24:00 uur vinden
onderhoudswerkzaamheden plaats aan de netwerkapparatuur in Huygens
vleugel 1. Zowel het bedrade als draadloze netwerk zullen daardoor op
enkele momenten niet beschikbaar zijn op vele locaties in vleugel 1 en
in centrale straat tussens vleugels 1 en 3, op alle verdiepingen.
