---
cpk_affected: Gebruikers van diverse websites.
cpk_begin: &id001 2020-06-18 15:45:00
cpk_end: 2020-06-18 16:25:00
cpk_number: 1265
date: *id001
title: Webserver 'havik' offline
url: cpk/1265
---
Diverse onderdelen zijn vervangen, we gaan ervan uit dat het tweemaal
opgetreden probleem hiermee verholpen is. Voor dual-boot pc's werd
tijdens de reparatie een alternatief bootmenu verzorgd.
