---
cpk_affected: gebruikers van Ceph filesystemen
cpk_begin: &id001 2021-03-24 19:00:00
cpk_end: 2021-03-24 21:00:00
cpk_number: 1282
date: *id001
tags:
- medewerkers
- ceph
title: Ceph probleem
url: cpk/1282
---
Bij een routine upgrade proces bleek dat er een bug in de laatste versie
zit waardoor de ceph manager onbereikbaar werd. Het upgrade proces is
afgebroken en met hulp van de ceph-users mailinglijst is alles weer
bereikbaar door een work-around.
