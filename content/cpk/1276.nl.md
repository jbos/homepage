---
cpk_affected: iedereen die iets in ru.nl wil benaderen van buiten de campus
cpk_begin: &id001 2021-02-21 07:10:00
cpk_end: 2021-02-23 14:30:00
cpk_number: 1276
date: *id001
tags:
- medewerkers
- studenten
- websites
title: DNS-problemen vanaf buiten met ru.nl
url: cpk/1276
---
De centrale DNS-servers van ru.nl voor externe requests werkten niet
goed doordat ze te veel bevraagd werden, waardoor ook o.a. science.ru.nl
soms niet gevonden kon worden: DNS-namen onder ru.nl resolven dan niet
naar een IP-adres. We hebben enkele TTL's (time-to-lives) vergroot om te
proberen de overlast iets minder te maken. Deze kleine TTL's waren
bedoeld om in het geval van problemen met deze servers de service snel
te kunnen verhuizen, maar dragen nu bij aan de overlast. Wie VPN
gebruikt heeft na het starten van de VPN hier geen probleem meer mee,
omdat dan de interne DNS-servers gebruikt worden. Door aanpassingen aan
de RU DNS-servers zijn hopelijk sinds 2021-02-23 14:30 de DNS-problemen
minder of verdwenen.
