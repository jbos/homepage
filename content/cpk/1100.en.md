---
cpk_affected: Users in Mercator1 4th floor
cpk_begin: &id001 2014-08-14 16:09:00
cpk_end: 2014-08-15 11:46:00
cpk_number: 1100
date: *id001
tags:
- medewerkers
title: M1.04 no network
url: cpk/1100
---
While administering the network switchports for the move of ISIS to
Mercator1 floor 0 to 3, ISC network management erroneously also changed
the configuration of the network switchports of floor 4. When this was
reported this morning, it could be readily remedied.
