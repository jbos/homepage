---
cpk_affected: all FNWI users using the wireless network to connect to bass.ru.nl.
cpk_begin: &id001 2013-08-12 00:00:00
cpk_end: 2013-08-23 15:00:00
cpk_number: 1036
date: *id001
tags:
- medewerkers
- studenten
title: Wireless access to BASS problem
url: cpk/1036
---
From August 12 the wireless network in the Science buildings is being
replaced. The new IP numbers appear to not be able to connect directly
to BASS. We expect that this will be changed soon.
