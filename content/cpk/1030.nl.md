---
cpk_affected: Gebruikers van lilo (lilo3)
cpk_begin: &id001 2013-07-15 11:00:00
cpk_end: 2013-07-15 11:15:00
cpk_number: 1030
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Lilo3 reboot voor ander IP-nummer
url: cpk/1030
---
Vandaag bleek dat lilo3 een verkeerd gekozen IP-nummer had. Om dat te
verhelpen, moest lilo3 rebooten.
