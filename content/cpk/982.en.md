---
cpk_begin: &id001 2011-01-24 00:00:00
cpk_end: 2011-03-11 00:00:00
cpk_number: 982
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Printer lp5
url: cpk/982
---
Printer lp5 has been moved to HG00.089. You can’t use this printer at
the moment, there’s a problem with the power supply unit.
