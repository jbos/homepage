---
cpk_affected: All users of the RU Internet connection
cpk_begin: &id001 2015-11-23 15:52:00
cpk_end: 2015-11-23 16:09:00
cpk_number: 1152
date: *id001
tags:
- medewerkers
- studenten
title: RU Internet connection down
url: cpk/1152
---
Yesterday at ca. 15:52 a [SURFnet](http://www.surf.nl) core router
crashed, taking down the RU Internet connection. At 16:09 the connection
was restored. This was reported yesterday on [a temporary SURFnet
service interruptions page](http://grotestoring.surfnet.nl/) and on the
[ISC/CIM service interruptions
page](http://www.ru.nl/systeem-meldingen/). Details can be found on the
[SURFnet Network Tickets mailing
list](https://list.surfnet.nl/pipermail/nettic/).
