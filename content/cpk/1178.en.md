---
cpk_affected: GitLab users
cpk_begin: &id001 2016-04-29 14:30:00
cpk_end: 2016-04-25 14:45:00
cpk_number: 1178
date: *id001
tags:
- medewerkers
- studenten
title: Gitlab backup accidentially restored on production server
url: cpk/1178
---
A backup file, dated “Fri Apr 29 05:07:04 CEST 2016” has accidentially
been restored on our production [GitLab server](/en/howto/gitlab/), where
it should have happened on a test server. This caused all repositories,
wiki’s, issues etc. to be reset to the state of today 5.07 AM. Commits
pushed between 5 am and 2:30 PM should be pushed again. Sorry for the
inconvenience.
