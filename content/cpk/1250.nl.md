---
cpk_affected: Ontvangers van Science mail
cpk_begin: &id001 2019-07-01 10:33:00
cpk_end: 2019-07-02 12:28:00
cpk_number: 1250
date: *id001
title: Mailproblemen met nieuw ingevoerde inkomende mailserver
url: cpk/1250
---
Een nieuwe server voor de inkomende mail is op 1 juli toegevoegd. Deze
heeft de nieuwste versie van Ubuntu en andere software en instellingen
meer conform [de
richtlijnen](https://www.forumstandaardisatie.nl/open-standaarden/lijst/verplicht?f%5B0%5D=field_keywords%3A68).
Na de invoering bleken er problemen met het ontvangen van mail vanuit
Microsoft Office 365 Exchange Online (mail.protection.outlook.com). Een
nog groter probleem was dat sommige geaccepteerde mails niet
doorgestuurd (geforward) werden, maar teruggestuurd (gebouncet). Omdat
dat probleem niet snel opgelost kon worden, is de nieuw ingevoerde
server op 2 juli weer uitgezet. Eind augustus gaat C&CZ de
waarschijnlijke oplossing testen. Als de test slaagt, zal de nieuwe
inkomende mailserver in bedrijf genomen worden.
