---
cpk_affected: all users of the wired network at FNWI
cpk_begin: &id001 2014-11-11 06:00:00
cpk_end: 2014-11-20 15:32:00
cpk_number: 1119
date: *id001
tags:
- medewerkers
- studenten
title: Network problems FNWI
url: cpk/1119
---
After the network maintenance of 20141111 a single user reported later a
problem with the wired network. These problems intensified and grew
faculty wide on 20141119 at 13:34 hours. The network switches in the
Faculty of Science received so many [topology
changes](http://www.dummies.com/how-to/content/how-spanning-tree-protocol-stp-manages-network-cha.html)
of the [Spanning Tree Protocol
(STP)](http://en.wikipedia.org/wiki/Spanning_Tree_Protocol) that network
traffic often stuttered. By curbing the STP traffic on 20141120 at 15:32
the stuttering disappeared. We still search for the exact cause,
probably it is a housing move.
