---
cpk_affected: Off-campus users wanting to access Science servers
cpk_begin: &id001 2019-05-14 16:13:00
cpk_end: 2019-05-15 21:22:00
cpk_number: 1247
date: *id001
tags:
- medewerkers
- studenten
title: DNS nameservice interruption for Science domains
url: cpk/1247
---
Yesterday afternoon a DNS error with science.ru.nl (due to the
introduction of the secure DNSSEC) made science.ru.nl disappear from the
internet for many users, when viewed from outside campus. In the evening
this error has been corrected. Because of the caching of DNS responses,
the last problems should disappear tonight. In the meantime one can use
workarounds like rebooting the home router and/or pc, using a different
network (mobile provider) or using rainloop.science.ru.nl instead of
roundcube.science.ru.nl for webmail access.
