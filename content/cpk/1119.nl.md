---
cpk_affected: alle gebruikers van het bedrade netwerk in FNWI
cpk_begin: &id001 2014-11-11 06:00:00
cpk_end: 2014-11-20 15:32:00
cpk_number: 1119
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkproblemen FNWI
url: cpk/1119
---
Na het netwerkonderhoud van 20141111 is er achteraf al een enkel
probleem met het bedrade netwerk. Deze verhevigen zich en worden
faculteitsbreed op 20141119 om 13:34. De netwerkswitches in FNWI krijgen
zo veel
[topologiewijzigingen](http://www.dummies.com/how-to/content/how-spanning-tree-protocol-stp-manages-network-cha.html)
van het [Spanning Tree Protocol
(STP)](http://nl.wikipedia.org/wiki/Spanning_tree_protocol) dat het
netwerkverkeer vaak hapert. Door het STP-verkeer te beteugelen zijn op
20141120 om 15:32 de haperingen voorbij. Naar de exacte oorzaak wordt
nog gezocht, waarschijnlijk is dat een verhuizing.
