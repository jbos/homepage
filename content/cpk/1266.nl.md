---
cpk_affected: FNWI-medewerkers met Science mail
cpk_begin: &id001 2020-03-25 17:52:00
cpk_end: 2020-07-07 13:13:00
cpk_number: 1266
date: *id001
title: RU mail onterecht in Spamfolder
url: cpk/1266
---
Aan het Science spamfilter is op 25 maart 2020 een regel "2020 Radboud
Universiteit" toegevoegd, die de laatste tijd ook in RU-centrale
mailings voorkomt. Daardoor zijn RU-brede mailings van o.a. het CvB en
Radboud Recharge onterecht in de Spam folder van FNWI-medewerkers
afgeleverd. Met het Science spamfilter proberen we zo goed als mogelijk
spam en phishing tegen te gaan, dit is deels handwerk, hierbij zijn
fouten niet uitgesloten. C&CZ biedt excuses aan voor de overlast die
hierdoor veroorzaakt is.
