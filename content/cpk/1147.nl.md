---
cpk_affected: Gebruikers van KM MFP's
cpk_begin: &id001 2015-10-08 08:00:00
cpk_end: 2015-10-08 12:00:00
cpk_number: 1147
date: *id001
tags:
- medewerkers
- studenten
title: Omruil Konica Minolta MFP's
url: cpk/1147
---
Op 8 oktober ’s ochtends zullen enkele KM MFP’s gewisseld worden, om
snellere/grotere machines te hebben op plaatsen waar meer geprint wordt.
De overlast per locatie zal gering zijn.

Getroffen printers:

`pr-hg-00-011 pr-hg-00-089 pr-hg-00-627 pr-hg-00-825`
`pr-hg-02-038 pr-hg-02-556 pr-hg-02-832`
