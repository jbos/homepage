---
cpk_affected: Users of the printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-04-14 13:05:00
cpk_end: 2014-04-14 13:12:00
cpk_number: 1080
date: *id001
tags:
- medewerkers
- studenten
title: And yet again Print/phpMyAdmin server problem
url: cpk/1080
---
Just like four days ago, the server didn’t react for unknown reasons. A
reboot of the machine solved the problem. We plan to replace the server
next Thursday 08:30-09:00 hours, in order to prevent further service
interruptions.
