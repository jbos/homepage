---
cpk_affected: users of lilo
cpk_begin: &id001 2021-03-17 21:00:00
cpk_end: 2021-03-17 21:15:00
cpk_number: 1280
date: *id001
tags:
- medewerkers
- studenten
title: Lilo7 restart
url: cpk/1280
---
To change the network of lilo7, we need to reboot this loginserver. If
you want a stable connection to a loginserver during this downtime,
please use lilo6 or the soon to be taken down lilo5. For more info see
[the page on C&CZ
loginservers](https://wiki.cncz.science.ru.nl/index.php?title=Hardware_servers&setlang=en#Linux_.5Bloginservers.5D.5Blogin_servers.5D).
