---
cpk_affected: students, teachers, people working with timetrex
cpk_begin: &id001 2017-11-14 20:50:00
cpk_end: 2017-11-16 00:00:00
cpk_number: 1219
date: *id001
tags:
- medewerkers
- studenten
title: SPIB, reviewapp, thesis submission, timetrex intermittent failures
url: cpk/1219
---
Currently the hardware has problems, causing the machine to randomly
turn off and on. We are in the process of resolving this with our
supplier.

The problem doesn’t appear to be hardware related, but a problem with
the configuration of the OS, which we changed and it seems to be stable
now.
