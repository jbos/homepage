---
cpk_affected: gebruikers van lilo
cpk_begin: &id001 2021-03-17 21:00:00
cpk_end: 2021-03-17 21:15:00
cpk_number: 1280
date: *id001
tags:
- medewerkers
- studenten
title: Lilo7 herstart
url: cpk/1280
---
Om het netwerk van lilo7 aan te passen, is het helaas noodzakelijk om
deze loginserver te herstarten. Wie gedurende deze onderhoudstijd een
stabiele verbinding wil hebben met een loginserver, kan beter lilo6 of
de binnenkort uitgefaseerde lilo5 gebruiken. Zie evt. [de pagina over de
C&CZ
loginservers](https://wiki.cncz.science.ru.nl/index.php?title=Hardware_servers&setlang=nl#Linux_.5Bloginservers.5D.5Blogin_servers.5D).
