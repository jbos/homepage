---
author: petervc
date: '2022-06-24T12:55:42Z'
keywords: []
lang: nl
tags:
- studenten
- medewerkers
- contactpersonen
title: Website
wiki_id: '910'
---
Er zijn globaal gezien drie typen websites bij FNWI in gebruik:

-   Generieke LAMP (Linux, Apache, MySQL, PHP/Perl/Python) of Mediawiki
    gebaseerde webservers, al dan niet verrijkt met een content
    management systeem. Deze sites worden gehost, technisch beheerd en
    ondersteund door C&CZ. Alle inhoudelijke beheertaken worden
    uitgevoerd door de eigenaren van de sites: instituten en
    wetenschappelijke afdelingen, studentenorganisaties, stafafdelingen,
    enz. Neem contact op met C&CZ voor meer informatie over
    [WWW\_Services](/nl/howto/www-service/).
-   Extern gehoste websites (waaronder de sites die door ILS worden
    gehost), waarvoor aparte contracten worden afgesloten voor beheer en
    ondersteuning.
-   Websites gebaseerd op het Content Management Systeem
    ([CMS](/nl/howto/cms/)) van de Radboud Universiteit (ook wel bekend
    als IPROX omdat dit de naam is van de betreffende software suite).
