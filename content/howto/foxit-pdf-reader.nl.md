---
author: arranc
date: '2022-09-12T06:31:56Z'
keywords: []
lang: nl
tags: []
title: FoxIt PDF Reader
wiki_id: '674'
---
Het programma [FoxIt PDF
Reader](http://www.foxitsoftware.com/pdf/rd_intro.php) kan gebruikt
worden als alternatief voor Adobe Reader, het is een gratis programma om
PDF-bestanden te bekijken en te printen. FoxIt is te vinden op de
[S-schijf](/nl/howto/s-schijf/) in de map “FoxIt-PDF-Reader”, waar ook
een handleiding in PDF formaat te vinden is.

Een reden om soms Adobe Reader niet te gebruiken is dat Adobe Reader 8
PostScript produceert waar de PostScript engine in de HP 4250/4350
printers niet mee overweg kan, zelfs niet na een firmware upgrade.
Daarom heeft C&CZ voor de meeste HP4250/HP4350 printers de (langzamere)
PCL-driver als standaard driver ingezet in plaats van de PS-driver.

Er kunnen problemen voorkomen met het openen van PDF bestanden op
computers die geen beheerde werkplek zijn indien FoxIt als standaard
staat ingesteld. Als er geen Network Drive naar de S-Disk is, of als er
nog niet op ingelogd is, dan krijg je een foutmelding. Oplossing
hiervoor is ,indien er geen Network Drive is, om die aan te maken, of om
de S-Disk te openen en eventueel in te loggen.
