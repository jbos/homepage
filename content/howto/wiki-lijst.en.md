---
author: polman
date: '2020-12-04T14:58:04Z'
keywords: []
lang: en
tags:
- wiki
title: Wiki lijst
wiki_id: '633'
---
### Wikis maintained by C&CZ

On [wiki.cncz.science.ru.nl](https://wiki.cncz.science.ru.nl/) C&CZ
provides the following wikis:

#### lab.cs.ru.nl

-   [algemeen](https://lab.cs.ru.nl/algemeen)

#### Science

-   [biophysics](https://wiki.biophysics.science.ru.nl/)
-   [clean](https://wiki.clean.cs.ru.nl/clean)
-   [cncz](/)
-   [ccnlab](https://wiki.ccnlab.science.ru.nl/)
-   [exaktueel](https://wiki.exaktueel.science.ru.nl/)
-   [gi](https://wiki.gi.science.ru.nl/)
-   [hef](https://wiki.hef.science.ru.nl/)
-   [icis-intra](https://wiki.icis-intra.cs.ru.nl/)
-   [isp](https://wiki.isp.science.ru.nl/)
-   [mbs](https://wiki.mbs.science.ru.nl/)
-   [microbiology](https://wiki.microbiology.science.ru.nl/)
-   [natuurkundepracticum](https://wiki.natuurkundepracticum.science.ru.nl/)
-   [new-devices-lab](https://new-devices-lab.cs.ru.nl)
-   [physchem](https://wiki.science.ru.nl/physchem)
-   [Planco](https://wiki.planco.science.ru.nl/)
-   [privacy](https://wiki.privacy.cs.ru.nl)
-   [seida](https://wiki.seida.cs.ru.nl)
-   [tfpie](https://wiki.tfpie.science.ru.nl)
-   [theochem](https://wiki.theochem.ru.nl/)

#### Others

-   [Titus-Brandsma-Instituut/spirin](https://spirin.org)
