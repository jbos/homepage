---
author: remcoa
date: '2022-10-06T15:13:58Z'
keywords: []
lang: en
tags:
- internet
title: TLS Certificaten
wiki_id: '68'
---
# TLS certificates

Any service accessible via TLS (https) must have an TLS certificate.
This includes any web server with encrypted or “secure” content. An TLS
(Transport Layer Security, see [RFC 8446](https://www.rfc-editor.org/rfc/rfc8446))
certificate is a signed electronic guarantee that
a particular server is the server it claims to be. Certificates are used
primarily (but not exclusively) for providing web pages via an encrypted
connection. A certificate is signed by a Certificate Authority (CA)
which ensures the integrity of the certificate.

A few Certificate Authorities such as Let's Encrypt, Verisign, Thawte, and Terena are
automatically trusted by TLS clients (including web browsers), so
certificates signed by these companies are validated without user
confirmation. All C&CZ certificates of servers and web applications are
signed by Terena (through SURFdiensten).

# Obtaining a certificate

Because TLS certificates are used as **proof** of the validity of the
web site or server, it is not possible to acquire a signed TLS
certificate for just any domain name. The Certificate Authorities check
if the person or organisation requesting a certificate is indeed the
owner of the domain name for which the certificate is requested. Domain
names [registered](/en/howto/domeinnaam-registratie/) through C&CZ are
owned by the Radboud University. Therefore C&CZ can also request TLS
Certificates for these domain names.

# Heartbleed OpenSSL bug

April 7, 2014, a two-year-old vulnerability was announced in several
versions of [OpenSSL](https://www.openssl.org/). OpenSSL is used for
encrypting network traffic. Because of this vulnerability, an attacker
could have retrieved the secret key of a service, with which traffic
could be decrypted. In addition to this, due to this vulnerability an
attacker could read the memory of the service, thereby retrieving
sensitive information like passwords.

After the announcement of this [Heart Bleed OpenSSL
leak](http://heartbleed.com/) all vulnerable C&CZ services were
automatically repaired. On Thursday, April 10th, we have deployed new
certificates for these services. The old certificates will be
[revoked](http://nl.wikipedia.org/wiki/Certificate_revocation_list). If
one has used the following C&CZ services, it is wise to change the
Science password on the [DIY website](https://diy.science.ru.nl). The
old password could have become known to an attacker.

The list of vulnerable C&CZ services that employees or students of the
Faculty of Science may have used:

-   Mail users:
    -   squirrel.science.ru.nl: The old Science webmail
        service was vulnerable since January 29, 2014.
    -   roundcube.science.ru.nl: The new Science webmail service.
    -   autoconfig.science.ru.nl: The website for automatic configuration of mail
        clients like Thunderbird and Outlook.

-   Lecturers: dossiers.science.ru.nl and
    eduview.science.ru.nl

-   MySQL database owners: phpmyadmin.science.ru.nl

-   FNWI news letter editors: newsroom.science.ru.nl

-   Websites of departments and study association:
    -   www.sos.cs.ru.nl
    -   prover.cs.ru.nl
    -   demo.irmacard.org
    -   molchem.science.ru.nl
    -   www.beevee.nl
    -   fmsresearch.nl

Other organisations than C&CZ will also inform users about the need to
change passwords due to this vulnerability. A few examples:

-   The
    [ISC](http://www.ru.nl/ictservicecentrum/actueel/news/@938292/security-leak/)
    about a.o. the RU password.
-   The big Dutch banks let know [according to the
    NOS](http://nos.nl/artikel/633432-beveiligde-internetverbindingen-lek.html)
    that they do not use OpenSSL and thus not have been vulnerable to
    this bug. The NOS links to a [Twitter page that says
    different](https://twitter.com/cducroix/status/453452094268510208/photo/1).
-   A (Dutch) [list van Internet companies put together by the Dutch
    newspaper de
    Volkskrant](http://www.volkskrant.nl/vk/nl/2694/Tech-Media/article/detail/3632963/2014/04/10/Heartbleed--lek-deze-wachtwoorden-kunt-u-het-best-zo-snel-mogelijk-wijzigen.dhtml).
-   An [overview of big Internet
    companies](http://mashable.com/2014/04/09/heartbleed-bug-websites-affected/?utm_campaign=Feed%3A+Mashable+%28Mashable%29&utm_cid=Mash-Prod-RSS-Feedburner-All-Partial&utm_medium=feed&utm_source=feedburner&utm_content=Google+International).
