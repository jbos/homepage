---
author: bram
date: '2022-11-02T10:08:04+01:00'
keywords: []
lang: nl
tags: ['faq']
title: Contact
wiki_id: '816'
weight: 1000
aliases:
- helpdesk
---
Voor alle ICT-gerelateerde vragen en problemen bij FNWI kun je contact
opnemen met C&CZ.

## Helpdesk & [Systeembeheer](../systeemontwikkeling)
| telefoon                           | mail
| -----------------------------------| ----
| [+31 24 36 20000](tel:+31243620000)| helpdesk@science.ru.nl
| [+31 24 36 53535](tel:+31243653535)| postmaster@science.ru.nl

## Bezoekadres

Wij zijn te vinden in kamer **HG03.055**. De helpdesk is bereikbaar tijdens reguliere kantoortijden
van 8:30 tot 17:00. Op de afdeling zijn dan ook de medewerkers aanwezig.

De afdeling C&CZ bevindt zich in het Huygensgebouw, op de 3e verdieping,
net naast de ingang van vleugel 5. Met grote oranje plakletters staat
daar C&CZ aangegeven. Het ruimtenummer is HG03.055, dat gedecodeerd kan
worden als Huygensgebouw (HG), 3e verdieping (03), centrale straat (.0),
ongeveer in het midden (055). In het Huygensgebouw zijn de vleugels
genummerd van 1 tot 8, van zuid naar noord, in diezelfde richting lopen
ook de kamernummers op. De centrale straat heeft vleugelnummer 0.

> **Volledig adres**:
> Huygensgebouw, HG03.055, Heyendaalseweg 135, 6525 AJ Nijmegen
>
> **Postadres**:
> Radboud Universiteit Nijmegen, Faculteit NWI, C&CZ, Postbus 9010, 6500 GL Nijmegen

# Andere helpdesks

## ILS Helpdesk

Bij de ILS helpdesk kun je terecht met vragen over, oa:
- ILS beheerde werkplekken
- Printen
- `@ru.nl` mailadressen
- Bureau en mobiele telefoons
- Netwerkaansluitingen

| telefoon                           | email             | web
| -----------------------------------| ----------------- | ---
| [+31 24 36 22222](tel:+31243622222)| icthelpdesk@ru.nl | https://www.ru.nl/ict/

Studenten met vragen over het Radboud-account of `@ru.nl` adressen kunnen het
beste direct contact opnemen met [Studentenzaken](https://www.ru.nl/afdelingen/academic-affairs-afdelingen/student-affairs).
