---
author: petervc
date: '2018-01-10T15:14:49Z'
keywords: []
lang: en
tags:
- software
title: Install share
wiki_id: '811'
---
Most campus license software that can be freely used by students and
staff of the Science faculty, can be found in the
[install](http://install.science.ru.nl/science/) directory of C&CZ
servers.

On a Windows PC the [network share](/en/howto/netwerkschijf/) connection
can be made using the service
`\\install-srv.science.ru.nl\install`. On a
Mac press command-K in the Finder and use the path
`smb://install.science.ru.nl/install>`. On a
managed Unix/Linux machine this drive can be found at
`/vol/install/science`.

Most network disks are available directly from within the Radboud
University network. From outside the RU use a [VPN](/en/howto/vpn/)
tunnel to connect to a network disk.

It is often possible to install this software directly from the [install
share](/en/howto/install-share/). Often a license key is needed, it can
be requested by mail to postmaster. Every software package has its own
license requirements. Sometimes it can only be used on university owned
computers, sometimes it can even be used by students at home.

- On this network drive browse to the **science** folder and then to the software package of your choice.
- Install the software, usually by executing ‘setup.exe’.
- Often a license code is needed. These can can requested by mail to [postmaster](/en/howto/contact).
