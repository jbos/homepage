---
author: bram
date: '2022-09-19T16:26:21Z'
keywords: []
lang: nl
tags:
- netwerk
title: Netwerk ntp
wiki_id: '96'
---
## Netwerk tijd (ntp)

De systeemklokken op de door C&CZ beheerde Linux machines worden up to
date gehouden via het zogenaamde Network Time Protocol (ntp). Elke
machine controleert eens in de zoveel tijd zijn eigen systeemklok aan de
hand van enkele andere aangewezen machines in het netwerk en stelt hem
zo nodig bij. Als het verschil van de *eigen* tijd vergeleken met de
door vergelijking met andere servers verkregen richttijd groter is dan
een halve seconde, dan wordt de systeemtijd gewoon bijgesteld. Is het
verschil kleiner, dan wordt de systeemklok een beetje vertraagd of
versneld, om te verhinderen dat de klok vaak verspringt. Omdat alle
systemen die mee doen hun klokken uiteindelijk op dezelfde ntp servers
afstemmen, lopen als alles goed gaat hun klokken gelijk. De lokale
servers zijn ntp1.science.ru.nl en ntp2.science.ru.nl.

Deze halen hun tijd op van externe *stratum 1* servers, nl van:

-   ntp0.nl.uu.net
-   ntp1.nl.uu.net

Een van die ntp servers, de *ntp1.science.ru.nl*, kunt u gebruiken om de
klok van uw machine te synchroniseren.

Externe informatie:

-   [\<http://www.ntp.org\>](http://www.ntp.org/)
-   <http://ntp.isc.org/bin/view/Main/WebHome>
-   [\<http://www.pool.ntp.org\>](http://www.pool.ntp.org/)
