---
author: caspar
date: '2015-01-28T12:27:45Z'
keywords: []
lang: nl
tags:
- studenten
- medewerkers
title: Prezi
wiki_id: '930'
---
# Prezi

[Prezi](https://prezi.com) is een cloud gebaseerde applicatie om
presentaties te maken en te geven. Het is een functioneel aantrekkelijk
alternatief voor powerpoint. Prezi (spreek uit: prezzi) presentaties
staan in de cloud, d.w.z. op de website van Prezi. Er is dus een
internetverbinding nodig om een presentatie te tonen, al is het met de
duurdere licentievormen mogelijk om presentaties ook offline te tonen
(Prezi Desktop). Met de standaard publieke licentie zijn al je
presentaties zichtbaar voor de hele wereld.

Prezi voorbeelden, handleidingen en instructievideo’s zijn beschikbaar
via de website.

## Edu Enjoy licentie

Voor docenten en studenten is gratis een uitgebreidere licentievorm
beschikbaar, de zogenaamde “Edu Enjoy” license. Aan de hand van het
e-mailadres controleert Prezi of iemand bij een educatieve instelling
hoort. Gebruik dus een RU adres dat eindigt op “ru.nl” zoals b.v.
“science.ru.nl” of “fnwi.ru.nl” om voor een Edu licentie in aanmerking
te komen. De registratie wijst zich vanzelf.

Let op: De verificatie email kan in de “spam” map binnen komen.

De voordelen van Edu Enjoy boven de Public licentie zijn:

-   Afgeschermde presentaties (kies zelf met wie je ze deelt)
-   Gebruik je eigen logo (i.p.v. het Prezi logo)
-   Maximaal 500MB aan Prezis i.p.v. 100MB

I.t.t. de reguliere Enjoy license biedt de Edu Enjoy license echter geen
gebruik van “Premium support” (e-mail helpdesk) van Prezi.

## Edu Enjoy upgrade

Heb je al een persoonlijke (public) Prezi licentie en wil je deze
upgraden naar Edu Enjoy, ga dan als volgt te werk:

-   Log in op de [Prezi](https://prezi.com) website.
-   Klik op je naam bovenin rechts en kies “Settings & Account”

-   -   Verander zonodig je mailadres (tevens je loginnaam) onder
        “Email” (een “Save changes” knop verschijnt automatisch)
    -   Wacht tot de wijziging is doorgevoerd en herhaal de eerste twee
        stappen

-   Klik halverwege de pagina onder “Account & license” op “Upgrade your
    license”

-   Kies “Student & Teacher licenses” in de Enjoy kolom

-   Klik “Upgrade” onder “Edu Enjoy” (de prijs moet \$0 per maand zijn)

[Categorie:Software](/nl/tags/software)
