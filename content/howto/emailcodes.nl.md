---
author: bram
date: '2022-09-23T07:16:08Z'
keywords: []
lang: nl
tags:
- email
title: Email Codes
wiki_id: '926'
---
## Overzicht van email codes

### ALICE-1

Dit is een email waarin wordt uitgenodigd om aan een cursusevaluatie
deel te nemen.

-   Aan wie? Alle studenten die zich voor het tentamen van de
    betreffende cursus hebben ingeschreven in OSIRIS.
-   Wanneer? Normaliter op de dag van het tentamen (ongeveer om 17:00).

### ALICE-2

Dit is een herinneringsmail waarin nogmaals wordt verzocht om aan een
cursusevaluatie deel te nemen.

-   Aan wie? Alle studenten die zich voor het tentamen van de
    betreffende cursus hebben ingeschreven in OSIRIS en de enquête nog
    niet hebben ingevuld.
-   Wanneer? Vijf dagen na de ALICE-1 email, dus normaliter vijf dagen
    na het tentamen.

### ALICE-3

Dit is een email waarin het resultaat van de feedback van een cursus
wordt teruggekoppeld aan studenten.

-   Aan wie? Alle studenten die de enquête hebben ingevuld.
-   Wanneer? De cursussen van een bepaald kwartaal worden aan het eind
    van het erop volgende kwartaal teruggekoppeld.

### ALICE-4

Dit is een email waarin het resultaat van de feedback wordt
teruggekoppeld aan de docent/cursuscoordinator van die cursus.

-   Aan wie? De docent/cursuscoordinator van de cursus.
-   Wanneer? De cursussen van een bepaald kwartaal worden aan het eind
    van het erop volgende kwartaal teruggekoppeld.

### CUDO-1

Dit is een email waarin de docent/cursuscoordinator op de hoogte wordt
gesteld van het feit dat de studentenevaluatie klaar is (en de
resultaten in het cursusdossier staan) en tevens wordt uitgenodigd om de
docentevaluatie te schrijven.

-   Aan wie? De docent/cursuscoordinator van de cursus.
-   Wanneer? Op het moment dat de studentenevaluatie klaar is,
    normaliter twee weken na het tentamen.

Zie voor meer informatie omtrent het cursusdossier
<https://eduview.science.ru.nl/redirect_link/fnwi/cursusdossier/>\
Het docentenevaluatieformulier is te vinden via de pagina
<https://eduview.science.ru.nl/redirect_link/fnwi/formulieren/>

### CUDO-2

Dit is een email waarin de docent/cursuscoordinator eraan wordt
herinnerd om de docentevaluatie te schrijven.

-   Aan wie? De docent/cursuscoordinator van de cursus.
-   Wanneer? Twee weken nadat de studentenevaluatie klaar is, normaliter
    vier weken na het tentamen.
-   Extra voorwaarde? Er staat nog geen docentenevaluatie PDF bestand in
    het cursusdossier.

Zie voor meer informatie omtrent het cursusdossier
<https://eduview.science.ru.nl/redirect_link/fnwi/cursusdossier/>\
Het docentenevaluatieformulier is te vinden via de pagina
<https://eduview.science.ru.nl/redirect_link/fnwi/formulieren/>

### CUDO-3

Dit is een email naar de secretarissen/voorzitters van een OLC waarin
wordt gemeld welke cursussen de week erop in principe door de
betreffende OLC beoordeeld zou moeten kunnen worden.

-   Aan wie? De secretaris/voorzitter van een OLC.
-   Wanneer? Normaliter vijf weken na het tentamen.

### ROUNDCUBE-1

-   Aan wie? Gebruikers van de
    [Roundcube](http://roundcube.science.ru.nl) webmaildienst.
-   Wanneer? Als er bij het instellen van filters in Roundcube bestaande
    [Sieve](/nl/howto/email-sieve/) filters worden aangetroffen.
-   Waarom? Bestaande Sieve filters zijn wel actief, maar worden niet
    weergegeven in Roundcube.
-   Wat te doen? Verwijder de Sieve filters en stel ze opnieuw in met
    Roundcube. Zegt dit u niks, neem dan [contact op met
    C&CZ!](/nl/howto/contact/).

### PRINTBUDGET-1

Deze mail wordt gestuurd om u te attenderen op een te laag printbudget.

-   Aan wie? Gebruikers van wie het persoonlijk printbudget te laag is
    geworden.
-   Wanneer? Binnen een kwartier nadat het printbudget te laag is
    geworden.

### PRINTBUDGET-2

Deze mail wordt gestuurd om u te attenderen op een te laag geworden
groepsprintbudget.

-   Aan wie? De eigenaren van een groepsprintbudget dat onder de gekozen
    ondergrens is gekomen.

Deze ondergrens kan per groepsbudget gezet worden. U kunt de ondergrens
in [DHZ](https://dhz.science.ru.nl) aanpassen.

-   Wanneer? Binnen een kwartier nadat het printbudget onder de
    ondergrens is gekomen.

### PRINTBUDGET-3

Deze mail wordt gestuurd om u te attenderen op een te laag geworden
printbudget.

-   Aan wie? De eigenaren van een printbudget dat onder de -5.00 euro is
    gekomen.
-   Wanneer? Zolang het printbudget te negatief is, elke zondag om
    20:35.

### DIY-1

Deze mail wordt gestuurd om u te informeren over het bereiken van de
controledatum van Science logins.

-   Aan wie? ICT contactpersonen.
-   Wanneer? Elke maandagmorgen om 10 uur. En alleen als de
    controledatum van 1 of meerdere logins is bereikt.

Op de "Mijn accounts"-pagina van [DHZ](http://dhz.science.ru.nl) kunt u zien van
wie u ICT contactpersoon bent.

### DIY-2

Deze mail wordt gestuurd om u te informeren over het bereiken van de
controledatum van uw Science login.

-   Aan wie? Aan eigenaar login.
-   Wanneer? Om 10:00 op de dag dat de controle datum is bereikt.

### LOGIN-1

Deze mail wordt gestuurd om u te informeren over het aanmaken van een
Science login voor u.

-   Aan wie? Degene waarvoor een Science login (account, loginnaam en
    wachtwoord) gemaakt is.
-   Wanneer? Elke werkdag om 19:00 uur wordt een mail gestuurd naar de
    die dag aangemaakte logins.

### PASSWORDRESET-1

Deze mail wordt gestuurd om u een nieuw initiëel wachtwoord voor uw
Science login te geven.

-   Aan wie? Degene die om een wachtwoord reset van zijn science login
    heeft gevraagd.
-   Wanneer? Direct nadat het wachtwoord door een systeembeheerder is
    aangepast.

