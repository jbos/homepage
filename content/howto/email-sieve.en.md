---
author: harcok
date: '2018-06-27T05:42:46Z'
keywords: []
lang: en
tags:
- email
title: Email sieve
wiki_id: '13'
---
## Sieve

If one wants incoming mail to be sorted into folders automatically or to
be filtered in some special way, one can use “Sieve” if one has the
IMAP-server `post.science.ru.nl` as incoming mailserver.\
To put the sieve script on the mail server and activate it, use a mail
program which supports sieve. The filter settings can be managed quite
easily in [Roundcube webmail](http://roundcube.science.ru.nl) with
Settings -\> Filters. It is also possible with e.g. KMail or Thunderbird
with the [sieve addon](http://sieve.mozdev.org/).\
On lilo4 you get with the command `sieve-connect postvak.science.ru.nl`
an interactive commandline where you can use the command *put* to upload
a sieve script.\
The Sieve language is described in
[RFC3028](http://www.ietf.org/rfc/rfc3028.txt?number=3028).

### Using Sieve in KMail

After adding an account via the KMail’s wizard (with settings found on
[:Categorie:Email](/en/tags/email)), KMail must be told that the server
supports Sieve. You can do this via Settings, Configure KMail, Accounts,
Modify, Filtering, Server supports Sieve. After that, Sieve scripts kan
be accessed via Settings, Manage Sieve Scripts.

### Examples of Sieve-scripts

-   If some mail should be sorted automatically into an **existing**
    folder, one can use a script like:

`require [ "fileinto", "include" ];`\
`#`\
`# Messages into folders.`\
`#`\
`if header :contains "X-VirusscanResult" "infected" {`\
`        fileinto "INBOX.Virus";`\
`        stop;`\
`}`\
`if address :is ["From", "To"] [`\
`              "Symposium2008@science.ru.nl",`\
`              "New-symposium2008@science.ru.nl"`\
`        ]`\
`{`\
`        fileinto "INBOX.Symposium2008";`\
`        stop; `\
`}`\
`include "spam";`\
`include "vacation";`

If one wants to redirect all mail that remains after our spamfilter has
done its job, one has to add after the `include "spam";` line:

`redirect "field@example.edu";`
