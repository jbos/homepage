---
author: petervc
date: '2018-01-10T15:14:49Z'
keywords: []
lang: nl
tags:
- software
title: Install share
wiki_id: '811'
---
Veel campuslicentie software die door studenten en medewerkers van FNWI
gebruikt mag worden, is te vinden in de
[install](http://install.science.ru.nl/science/) directory van C&CZ
servers.

Op een Windows PC, kan de [share](/nl/howto/netwerkschijf/) aangekoppeld
worden via de service
`\\install-srv.science.ru.nl\install`. Op een
Mac gebruik command-K in de Finder en voer het pad
`smb://install.science.ru.nl/install>` in. Op
een beheerd Unix/Linux systeem kan men deze schijf terug vinden onder
`/vol/install/science`.

Gewoonlijk zijn netwerkschijven direct beschikbaar vanuit het RU interne
netwerk, bedraad of draadloos. Van buiten de RU is een
[VPN](/nl/howto/vpn/)-tunnel noodzakelijk om een netwerkschijf te kunnen
koppelen.

Meestal kan men deze software direct vanaf de [install
share](/nl/howto/install-share/) installeren. Vaak is een licentiecode
nodig, die via mail naar postmaster verkregen kan worden. Elk
softwarepakket heeft eigen licentievoorwaarden. Soms is gebruik alleen
op RU-computers toegestaan, soms ook thuisgebruik door studenten.

- Navigeer op deze netwerkschijf naar de **science** map en dan naarceen software pakket naar keuze.
- Installeer nu de gekozen software, meestal door ‘setup.exe’ uit te voeren.
- Vaak is een licentiecode nodig, die via mail naar [postmaster](/nl/howto/contact) verkregen kan worden.