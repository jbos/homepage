---
author: bram
date: '2022-09-23T07:16:08Z'
keywords: []
lang: en
tags:
- email
title: EmailCodes
wiki_id: '926'
---
## Email codes overview

### ALICE-1

This is an email in which students are requested to participate in the
digital course evaluation.

-   Recipients? All students that are registered in OSIRIS for the exam
    of the corresponding course.
-   When? Typically on the day of the exam (around 17:00).

### ALICE-2

This is an email reminder in which students are requested once again to
participate in the digital course evaluation.

-   Recipients? All students that are registered in OSIRIS for the exam
    of the corresponding course.
-   When? Five days after the ALICE-1 email, so typically five days
    after the exam.

### ALICE-3

This is an email in which the feedback of the course are sent to the
students.

-   Recipients? All students that have participated in the survey.
-   When? The feedback of courses of a given quarter are sent at the end
    of the following quarter.

### ALICE-4

This is an email in which the feedback of the course is sent to the
lecturer/course coordinator.

-   Recipients? The lecturer/course coordinator of the course.
-   When? The feedback of courses of a given quarter are sent at the end
    of the following quarter.

### CUDO-1

This is an email by which the course coordinator is notified that the
student course evaluation is completed (and can be found in the course
dossier) and that subsequently the lecturer’s course evaluation form can
be submitted to the course dossier.

-   Recipients? The course coordinator.
-   When? When the student course evaluation is completed, typically two
    weeks after the exam.

For more information on the course dossier, please, refer to
<https://eduview.science.ru.nl/redirect_link/science/course-dossier/>\
The lecturer’s course evaluation form can be found at the page
<https://eduview.science.ru.nl/redirect_link/science/forms/>

### CUDO-2

This is an email by which the course coordinator is reminded that the
student course evaluation is completed (and can be found in the course
dossier) and that subsequently the lecturer’s course evaluation form can
be submitted to the course dossier.

-   Recipients? The course coordinator.
-   When? Two weeks after the student course evaluation is completed,
    typically four weeks after the exam.

For more information on the course file, please, refer to
<https://eduview.science.ru.nl/redirect_link/science/course-dossier/>\
The lecturer’s course evaluation form can be found at the page
<https://eduview.science.ru.nl/redirect_link/science/forms/>

### CUDO-3

This is an email sent to the programme committee (“OLC”) members listing
courses that should be ready for evaluation in the week after.

-   Recipients? The secretary/chairman of a Programme Committee (“OLC”).
-   When? Typically five weeks after the exam.

### ROUNDCUBE-1

-   Recipients? Users of the [Roundcube](http://roundcube.science.ru.nl)
    webmail service.
-   When? If pre-existing Sieve filters are present on the mail server
    when changing mail filters in Roundcube.
-   Why? Pre-existing filters will remain active, but are not shown in
    Roundcube.
-   What to do? Remove the pre-existing Sieve filters and configure
    everything with Roundcube. If this makes no sense to you, [contact
    C&CZ!](/en/howto/contact/).

### PRINTBUDGET-1

This is an email sent to make you aware that your printbudget has become
too low.

-   Recipients? Users whose personal printbudget has become too low.
-   When? Within 15 minutes after the printbudget has become too low.

### PRINTBUDGET-2

This is an email sent to make you aware that a group printbudget has
become too low.

-   Recipients? The owners of the group printbudget that has gone below
    the chosen threshold.

This threshold can be chosen individually for each group printbudget.
You can set the threshold in [DIY](https://diy.science.ru.nl).

-   When? Within 15 minutes after the printbudget has become lower than
    the threshold.

### PRINTBUDGET-3

This is an email sent to make you aware that your printbudget has become
too low.

-   Recipients? The owners of the printbudget that has gone below -5.00
    euro.
-   When? As long as the budget is too negative, every Sunday at 20:35.

### DIY-1

This mail will be sent if the check date of a Science login has been
reached.

-   Recipients? IT contact persons.
-   When? Monday 10am. Only in case the check date of a Science login
    has been reached.

On the "My accounts" page of [DIY](http://diy.science.ru.nl), you can see for
which Science logins you are the IT contact person.

### DIY-2

This mail will be sent if the check date of your Science login has been
reached.

-   Recipients? Owner of the account.
-   When? At 10:00 on the day that the check date has been reached.

### LOGIN-1

This mail will be sent if a Science login has been created.

-   Recipient? The person for whom a Science login has been created.
-   When? Each day at 7pm.

### PASSWORDRESET-1

This mail will be sent to give you a new initial password for your
Science login.

-   Recipient? The person who requested a password reset.
-   When? Directly after the password has been reset by a system
    administrator.

