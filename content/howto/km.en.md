---
author: petervc
date: '2017-06-16T16:04:42Z'
keywords: []
lang: en
tags:
- hardware
- scanners
title: KM
wiki_id: '912'
---
## Konica Minolta Multifunctionals (Printers/Copiers/Scanners)

In 2013 the [RU](http://www.ru.nl) issued a European tender for
multifunctionals. Based on an extensive list of demands the tender was
won by [Konica Minolta](http://www.konicaminolta.nl). The machines are
used as [copiers](/en/howto/copiers/), [network
printers](/en/howto/printers-en-printen/) and scanners.

### Documentation

All documentation can be found on the [Konica Minolta
website](http://www.konicaminolta.nl/nl/business-solutions/customer-support/downloads.html).
Quick reference guides are available through this wiki for the models
present: [Bizhub C364e, C554 and
C554e](/download/old/bizhub-c554-c454-c364-c284-c224_quick-reference_en_1-0-0.pdf)
and the higher volume [Bizhub
C654](/download/old/bizhub-c754-c654_poster_en_1-0-0.pdf).

### Placement of the machines

As of July 1, 2013, new [Konica Minolta](http://www.konicaminolta.nl/)
(KM) multifunctional printers/copiers/scanners (MFP’s) will be installed
in [many places](/en/howto/copiers/) in the FNWI buildings. These
machines replace the Ricoh’s, which will be removed a few weeks later.

We have decided to install more MFP’s than the current number of
Ricoh’s. This is in conherence with the RU policy to reduce the walking
distance from any work place to the nearest MFP. As a result departments
will be tempted to discontinue their own MFP-services, including their
own paper and toner supplies and support and maintenance contracts. For
the new machines, KM takes care of all that for a fixed price per job
which is the same for the entire Radboud University. The new machines
also report problems directly to KM (through GSM), so sometimes problems
are resolved without having been reported by an employee or student.
Therefore we expect that this can improve efficiency while cutting
costs.

The placement of individual machines is not definitive: a machine that
is used heavily can be replaced by a bigger/faster machine, whereas
machines that are seldom used can be moved to a different location.

Please [contact C&CZ](/en/howto/contact/) with all questions about the
new machines.

### Peage

The MFP’s will in time be managed by [Peage](/en/howto/peage/). Because
FNWI has shared MFP facilities for both students and employees, Peage
will be activated in our faculty only after it becomes operational for
employees. This is expected to be at the end of 2013. With Peage one can
print sensitive output on shared printers, because then one chooses
which print job to print after authentication at the printer with campus
card and personal pin code.

### Printing

As long as Peage is not available for employees, the MFP’s will be
managed by C&CZ. This means that for the time being the KM’s will be
used and factured as [budgetted
C&CZ-printers](/en/howto/printers-en-printen/). The functionality thus
stays the same initially, one just needs to define the new printers on
their workstation.

### Copying

As long as Peage is not available for employees, every KM gets a
(unique) PIN code. FNWI employees can obtain the PIN code(s) from C&CZ
(*call 56666*), which effectively makes copying free for departments:
the faculty pays for all copy costs. When one is finished with copying,
please logout by hitting the button on the right side (a door with a
lockhole and two arrows), otherwise the machine logs you out after five
minutes of inactivity. In case of presumed misuse, C&CZ will change the
code.

Students can copy on the Peage MFP near the restaurant, or by scanning
first and printing afterwards. The Ricohs with Xafax copy unit will be
removed in the near future. Students can come to C&CZ to have their copy
card budget converted to printbudget.

### Scanning

The pin ‘1111’ frees a machine for scanning. Scanning works in much the
same way as with the Ricohs; scans are mailed to an address specified by
the user. The KM’s show all addresses from the [Science LDAP
addressbook](/en/howto/ldap-adresboek/), which eliminates the need to
type in addresses and will result in less addressing errors.

There are several reasons why scanning to an email address can fail.
Most common is a typo in the mail address. Next is a scan that is too
large for the central RU mailserver/Gmail/…, that often have a limit of
ca. 25MB. In all those cases C&CZ, als sender of the mail
(KMscan\@science.ru.nl) gets notice of this. C&CZ tries to forward these
mails to the correct address or to inform the user that the scan is too
large for the mail provider. Alternatives are to scan to your
@science.ru.nl Inbox or scanning to a USB stick. Log in at the MFP with
the scan pin, put a document in the feeder or on the document glass,
select `Scan` and then plug the USB stick into the USB port on the right
side of the MFP, on the top side. After a few seconds the MFP recognizes
the USB stick and presents the choice “Save a document to external
memory”. Choose `OK` and press `Start`. See also [the RU
manual](http://www.ru.nl/publish/pages/687597/uitrol-mf-scan.pdf) on the
[RU-page about the
MFP’s](http://www.ru.nl/ict-uk/staff/printing-copying/).

### Service

In case of hardware and paper problems the Konica Minolta helpdesk can
be reached by dialing 55955, option 4. You must provide the serial
number or machine id. These can be found on the machine, but also in the
[list of all multifunctionals](/en/howto/copiers/). A Konica Minolta
maintenance engineer is present on campus, so reported problems will in
general be solved relatively quickly.
