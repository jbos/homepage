---
author: petervc
date: '2022-06-24T12:45:46Z'
keywords: []
lang: nl
tags:
- software
title: RU software licenties
wiki_id: '145'
---
## RU software licenties afgesloten door ILS

ILS heeft voor de RU voor [een aantal
software-pakketten](http://www.ru.nl/ict/medewerkers/software/)
licentie-overeenkomsten met de leverancier afgesloten. Daardoor is die
software gratis (campuslicentie) of relatief goedkoop (bulklicentie)
voor studenten en medewerkers te verkrijgen. Meestal betreft het
software waarvoor [SURFdiensten](http://www.surfdiensten.nl/) voor
onderwijs-instellingen een SURFlicentie-overeenkomst afgesloten heeft,
die door [SLIM](http://www.slim.nl/) als “SLIMme ROM” uitgebracht wordt.

Veel licentie software is bij [Surfspot](http://www.surfspot.nl/) te
koop voor medewerkers en studenten met hun personeels- of studentnummer
en hun [RU-wachtwoord](http://www.ru.nl/wachtwoord). C&CZ heeft hiervan
sommige (voornamelijk MS-Windows) [software te
leen](/nl/howto/microsoft-windows/). Op verschillende soorten werkplekken
is deze software ook op een andere manier te vinden waardoor het niet
altijd meer nodig is CD-ROMs te komen lenen.

-   Security: F-secure
    -   voor MS-Windows PC’s die met geld van de RU gekocht zijn, kan
        men een licentie krijgen via
        [Radboudnet](https://www.radboudnet.nl/informatiebeveiliging/secure/download-secure/).
    -   voor Apple computers die met geld van de RU gekocht zijn, kan
        men een licentie krijgen via
        [Radboudnet](https://www.radboudnet.nl/informatiebeveiliging/secure/aanvraagformulier/).
    -   voor individueel/thuisgebruik op 1 eigen computer kan men bij
        [Surfspot](http://www.surfspot.nl) gratis “F-Secure Safe (RU
        Nijmegen)” aanschaffen.

-   Op C&CZ Unix-computers, automatisch
    gemount, in de directory
    [/vol/install/science](https://wiki.cncz.science.ru.nl/Install_share).

-   Op een PC met MS-Windows (of een Linux “Samba client”), kan de
    schijf-verbinding gemaakt worden via de service
    **\\\\install.science.ru.nl\\install**. De
    software is dan te vinden in de map
    [science](https://wiki.cncz.science.ru.nl/Install_share). Neem
    contact op met C&CZ voor evt. noodzakelijke license keys.
