---
author: petervc
date: '2011-08-18T14:34:37Z'
keywords: []
lang: en
tags: []
title: Bezet-info publiceren
wiki_id: '723'
---
### Publish freebusy-information from calendar

To make it possible that others, especially the Faculty Office, have
access to the free/busy information from your online calendar, you can
follow the instructions below. If more people use this service, less
time is spent within the Faculty managing calendars. Note: the free/busy
information only lists when you are occupied, but not where, with what
or whom. This is a temporary solution: when (in 2011?) a new RU
mail/calendar system has been fully deployed, one can share calendars
better and simply.

The workaround consists of URL’s of the form
<https://calendar.science.ru.nl/freebusy/I.Surname.vfb> (note the
capitals I and S, all of course adapted for ones own name), in which the
free/busy information is published in Internet-standard [iCalendar
VFREEBUSY-format](http://en.wikipedia.org/wiki/ICalendar#Free.2Fbusy_time_.28VFREEBUSY.29).
These URL’s can be used in other calendar systems.

# Calendar.science.ru.nl E-Groupware

No user action required. The administrators (C&CZ) publish this info.

# Egw.cs.ru.nl E-Groupware

No user action required. The administrators (C&CZ) publish this info.

# Outlook

1.  Navigate in Outlook to “Tools -\> Options -\> Calendar options”.
2.  Click below “Advanced options” on “Publish at my location” and fill
    in the location:
3.  <https://calendar.science.ru.nl/freebusy/I.Surname.vfb>
4.  Of course with one’s own initial and surname instead of I.Surname.
5.  Change the number of months to be published from 2 (the default) to
    12. 
6.  Click OK and restart Outlook. After about 15 minutes Outlook will
    have published the free/busy information.
7.  Send an e-mail to postmaster of science.ru.nl stating that you have
    published your freebusy information.

# Google Calendar

1.  Log in to <http://www.google.com/calendar>
2.  Click in the left column under “My calendars” on the menu-tick next
    to the calendar that you want to share and click “Calendar
    settings”.
3.  Choose the tab: “Share this calendar”.
4.  Check both “Make this calendar public” and “Share only my free/busy
    information (Hide details)”.
5.  Send a mail to postmaster of science.ru.nl with in it:
    1.  the Google username
    2.  the statement that the free/busy info has been shared.
