---
title: openvpn
author: bram
date: 2022-11-04
title: OpenVPN
keywords: [ openvpn, vpn ]
tags: [ vpn ]
cover:
  image: img/2022/openvpn.png
ShowToc: false
TocOpen: false
---
# Openvpn
If you somehow fail to get our standard [VPN service](../vpn) up and running on your system, use our OpenVPN service.

General settings:
| label        | value                    |
| ------------ | ------------------------ |
| server adres | `openvpn.science.ru.nl ` |
| port         | `443`                    |


{{< notice tip >}}
You can test your VPN-connection by visiting this (great) website:

> <https://ip.science.ru.nl>

This page displays your ip address. If your Science VPN is active, your ip address should start with `131.174.`. The hostname will look like `
{{< /notice >}}

## Ubuntu 22.04 instructions
Download the [Science openvpn configuration file](https://gitlab.science.ru.nl/cncz/openvpn/raw/master/openvpn-ca-science.ovpn):

```
wget https://gitlab.science.ru.nl/cncz/openvpn/raw/master/openvpn-ca-science.ovpn
```

Goto "Systeem settings" > "Network". You should see this screen:

 ![add vpn in ubuntu](/img/2022/newvpnubuntu.png)

Click the `+` button to add a new VPN connection.

In the next dialog choose "Import from file..." and select the just downloaded `ovpn` file.

You should see this screen now:

![openvpn configuration ubuntu](/img/2022/openvpn-ubuntu.png)

Just fill in your [science loginname](../login).

After saving your configuataion, you should be able to activate your OpenVPN connection. Click the icons in the top-right corner of your screen:

![activate vpn ubuntu](/img/2022/ubuntu-enable-vpn.png)