---
author: petervc
date: '2014-11-14T09:31:06Z'
keywords: []
lang: en
tags:
- email
title: Pine
wiki_id: '507'
---
### Pine

Use ‘S’
for SETUP and then’C" for Config and fill in:

      smtp-server    = smtp.science.ru.nl/user=...
      inbox-path     = {post.science.ru.nl/ssl/user=...}INBOX

where … should be replaced by your login
name

To see the other IMAP folders, use ‘S" for SETUP and then ’C’ for Config
and change:

      [ Folder Preferences ]
                [X]  combined-folder-display
                [X]  expanded-view-of-folders
                [X]  quell-empty-directories

Ook nog binnen SETUP: kies ‘L’ voor collectionLists en maak met ‘A’ twee
nieuwe collection lists:

      Nickname  : Personal folders
      Server    : post.science.ru.nl/ssl/user=...
      Path      : INBOX.
      View      :

(close with control-X)

[and]

      Nickname  : Shared folders
      Server    : post.science.ru.nl/ssl/user=...
      Path      : user.
      View      :

(close with control-X)

`If you remove the default Mail/ collection list, then all sent mail will be`

automatically stored in a folder on post.science.ru.nl.
