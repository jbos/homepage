---
author: petervc
date: '2022-06-24T12:45:46Z'
keywords: []
lang: en
tags:
- software
title: RU software licenties
wiki_id: '145'
---
## RU software licences negotiated by ILS

ILS negotiated license agreements for the RU for [a number of software
packages](http://www.ru.nl/ict-uk/staff/software/). This makes that
software relatively cheap (bulk license) or even free (campus license)
for use by students and employees. In most cases it is software for
which [SURFdiensten](http://www.surfdiensten.nl/) negotiated a
SURFlicense-agreement for use by academic users, which is marketed by
[SLIM](http://www.slim.nl/) as “SLIMme ROM”.

A lot of license software can be bought at
[Surfspot](http://www.surfspot.nl/) by employees and students by using
their personnel or student number and their
[RU-password](http://www.ru.nl/wachtwoord). Some of this (primarily
MS-Windows) [software can be borrowed](/en/howto/microsoft-windows/) from
C&CZ.

-   Security: F-Secure
    -   for MS-Windows PC’s purchased with RU funds that are privately
        administered, one can get a license through
        [Radboudnet](https://www.radboudnet.nl/informatiebeveiliging/secure/download-secure/).
    -   for Apple computers purchased with RU funds that are privately
        administered, one can get a license through
        [Radboudnet](https://www.radboudnet.nl/informatiebeveiliging/secure/aanvraagformulier/).
    -   for 1 privately owned device one can get free of charge
        “F-Secure Safe (RU Nijmegen)” at
        [Surfspot](http://www.surfspot.nl).

-   On C&CZ Unix-computers, automatically
    mounted, in the directory
    [/vol/install/science](https://wiki.cncz.science.ru.nl/Install_share).

-   On a PC with MS-Windows (or a Linux “Samba client”), one can connect
    to the service
    **\\\\install.science.ru.nl\\install**. The
    software can then be found in the directory
    [science](https://wiki.cncz.science.ru.nl/Install_share). Please
    contact C&CZ for the necessary license keys.
