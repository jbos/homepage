---
author: caspar
date: '2015-01-28T12:27:45Z'
keywords: []
lang: en
tags:
- studenten
- medewerkers
title: Prezi
wiki_id: '930'
---
# Prezi

[Prezi](https://prezi.com) is a cloud based application to create and
show presentations. It is a functionally attractive alternative for
powerpoint. Prezi (pronounced as prezzi) presentations are stored in the
cloud, i.e. on the Prezi website. This means you must be connected to
the internet to show a presentation, although the more expensive
licenses allow for offline presentations as well (Prezi Desktop). With
the standard public license all of your presentations are visible for
the world

Example presentations, user manuals, and instruction videos are
available online.

## Edu Enjoy license

For teachers and students an extended license is available for free, the
so called “Edu Enjoy” license. Your mail address is used to check if you
belong to an educational organisation, so use an RU address which ends
in “ru.nl” (e.g. “science.ru.nl” or “fnwi.ru.nl”) to qualify for an Edu
license. The registration process itself is straightforward.

Warning: It is possible that the verification email is put into the
“spam” folder.

Advantages of the Edu Enjoy license as compared to Public:

-   PRIVATE presentations (choose who to share with)
-   Use your own logo (Get rid of the Prezi logo)
-   500MB storage space instead of 100MB

Compared to the regular Enjoy license the Edu Enjoy license does not
offer Premium (e-mail helpdesk) support.

## Edu Enjoy upgrade

If you already have a personal (public) Prezi license and you wish to
upgrade to Edu Enjoy, proceed as follows:

-   Logon to the [Prezi](https://prezi.com) website.
-   Click your account name (upper right) and choose “Settings &
    Account”

-   -   If necessary change your mail address (which is also your Prezi
        login name) (a “Save changes” button automatically appears)
    -   Wait for the change to take place, then repeat the first two
        steps

-   Halfway down the page under “Account & license” click “Upgrade your
    license”

-   In the Enjoy column choose “Student & Teacher licenses”

-   Click “Upgrade” under Edu Enjoy (price should be \$0/month)

[Categorie:Software](/en/tags/software)
