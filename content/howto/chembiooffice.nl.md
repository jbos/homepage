---
author: polman
date: '2022-04-05T11:52:47Z'
keywords: []
lang: nl
tags:
- software
title: ChemBioOffice
wiki_id: '690'
---
### There is a problem with the extension of the license, the supplier has been notified.

The license was supposed to be extended till the end of April while the
negotiations for a new license period are still on going. Unfortunately
this turns out to be not true. We are waiting for an update from the
supplier

ChemBioOffice is een veelzijdig software pakket voor onder andere het
opzoeken en tekenen van moleculaire structuren, 3-D visualisatie en
molecular modeling, alsook teken van biologische transformaties en
management van biologische data.

Belangrijkste onderdelen van ChemBioOffice:

* ChemDraw: tekenen van structuren (o.a. name-to-structure) en
biologische componenten, NMR en MS fragmentatie predictie, voorspellen
van fysische en chemische eigenschappen zoals ClogP en polar surface
area, importeren van structuren op basis van naam.

* Chem3D: 3-D visualisatie van moleculen, rotatie, zoomen,
conformatie-analyse, molecular modeling en dynamics, zowel empirisch
(MM2) als ab initio (GAMESS), orbitalen visualisatie.

* ChemFinder: zoeken van moleculaire structuren op het WWW

* ChemACX: database van commercieel verkrijgbare verbindingen

* BioDraw: toepassing voor het tekenen, delen en presenteren van
biologische processen. Algemene biologische elementen (membranen, DNA,
enzymen, receptoren, reactiepijlen, etc) zijn via een template
voorradig, anderen kunnen geimporteerd worden.

* BioAssay: management van data van biologische experimenten.
Ontwikkeld voor chemici en biologn die actief zijn in onderzoek naar
medicijnen of gen-onderzoek doen. Vooral nuttig voor onderzoeker die in
vivo experimenten uitvoeren met complexe modellen door de integratie van
chemische en biologische data.

* BioViz: transformeert de getallen in je database in grafische
weergaves op je scherm. Terughalen of zoeken van een set van
verbindingen, keuze uit gewenste data, zoals relevante biologische data
uit Oracle tabellen, berekende fysische eigenschappen, prijzen in
catalogi. BioViz genereert een interactief scherm met scatterplot,
histogram of andere nuttige grafische data-weergave.

Deze software wordt veelvuldig gebruikt binnen verschillende werkgroepen
van de Faculteit NWI en in toenemende mate ook in het onderwijs. Door de
gebruiksvriendelijkheid van de software is het ook zeer geschikt voor
studenten. **Iedereen met een e-mail adres eindigend op @ru.nl of
@student.ru.nl is gerechtigd de software te downloaden en 3 keer te
installeren.**

Om ChemBioOffice te downloaden moet je je eerst online registeren op de
website van PerkinElmer:

` `<https://informatics.perkinelmer.com/sitesubscription/#R>` (zoek Radboud Universiteit)`

Ga daarna naar

<https://perkinelmer.flexnetoperations.com/control/prkl/login>` `

en download de software. Een serial number is na inloggen te vinden
onder “order History”.
