---
author: polman
date: '2021-10-15T12:33:06Z'
keywords: []
lang: en
tags:
- email
title: Email authsmtp
wiki_id: '11'
---
## Advantages of authenticated SMTP

It is desirable to authenticate to the mail server with your login name
and password when sending mail. When you use authentication, the mail
server will never refuse mail with the error “Relaying denied”. So it is
possible to use our mail servers from outside the Faculty. If you use a
laptop, you can always use the scienc mail servers, both at work and at
home or abroad, providing you authenticate with the SMTP server.

When everyone uses a trusted way to send e-mail, eg when using
authenticated SMTP or our webmailers, we could decide to let “the
Internet” know that when someone sends e-mail with an `@science.ru.nl`
address, this can only be sent using smtp servers from the university.
We can do this using
[SPF/DKIM/DMARC](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail).
Mail sent through external smtp servers will then be rejected if the
receiving mail server performs this check, which is becoming more and
more the standard behavior. This is an important contribution to stop
phishing mails that appear to come from science.ru.nl addresses.

**NB** for `science.ru.nl` you can also read other domains managed by
us, like e.g. `cs.ru.nl`.

## Authenticated SMTP

SMTP (Simple Mail Transfer Protocol) is the protocol with which email is
transferred to another computer on the internet. Your email program uses
it to give outgoing mail to your outgoing SMTP-server. When you want to
send mail with your favourite email program, normally you have to use
the SMTP-server of the internet-provider that you use at that moment. If
you want to send email while travelling or if you have a laptop that you
use on campus and at home, this would mean that you would have to change
the outgoing SMTP-server all the time.

Why would you need a different SMTP-server? If you use your ‘old’
SMTP-server while connected through a different internet-provider, the
SMTP-server sees an incoming connection from a ‘strange’ network. It
will normally only accept mail from this ‘strange’ network for his ‘own’
users, but not for recipients all over the internet. If it would accept
mail from anywhere and delivers that all over the internet, it can be
misused to relay spam, after which the server will appear on blacklists
and mail from this server will not be accepted by many other servers.

The alternative. If you authenticate yourself with your username and
password to the SMTP-server, then the administrators of the SMTP-server
can accept mail with recipients all over the internet. If misused, the
administrators know which of their customers to contact.

Security. Because of the fact that you connect through a ‘strange’
network and that you have to supply username and password, the
connection is encypted with SSL to prevent eavesdropping. All modern
email programs support this.

# Configuration of authenticated SMTP

## General settings

-   Choose as SMTP-server **smtp.science.ru.nl**.
-   The **default tcp port 25** can be used, but the **submission port
    587** also is supported. Smtps (port 465) is also supported.\
    **Some providers block outgoing traffic to port 25.** Futhermore,
    Norton firewall and anti-virus refuse encrypted outgoing traffic to
    port 25 and it is not always clear how to fix that without disabling
    Norton firewall entirely. In this case using the submission port 587
    offers a solution.\
    Other firewalls such as the firewall of McAfee by default seem to
    block outgoing traffic to port 587, but allow it to the smtp port
    25. 
-   If one uses port 587, authentication is *mandatory*.
-   If one uses port 587, outgoing mail is not checked for spam content
    or viruses. This gives a way to send exe-files, which is otherwise
    not allowed.
-   **Authentication** is possible with the PLAIN and LOGIN
    authentication methods. In particular NTLM (a Microsoft protocol)
    and CRAM-MD5 are not supported.\
    As account name one always has to use the login name (i.e. without
    “.” and “@”).
-   Authentication is only allowed if **TLS** is used. Sometimes this is
    called STARTTLS (Thunderbird) or SSL (Outlook, Pine).

## Alternative RU server

-   server: smtp-auth.ru.nl

-   user name: U/S-number

-   password: RU password

## Configuration of Thunderbird

-   Choose **Tools → Account Settings…**. For some versions **Account
    Settings…** is under the menu **Edit**.

-   Click on **Outgoing Server (SMTP)** in the left column and fill in
    or check:

    \* Server Name: **smtp.science.ru.nl**

    -   Port: **25** or **587** (See [\|General settings] for more
        information.)
    -   **Check the box**: Use name and password
    -   User Name: *your login name*
    -   Use secure connection: **TLS, if available**\
        {{< figure src="/img/old/thunderbird-en-smtp.jpg" title="Outgoing Server (SMTP) Settings" >}}

## Configuration of Outlook

-   Choose **Tools → E-mail Accounts…**

-   Check **View or change existing e-mail accounts** and click on
    **Next**.

-   Select the account and click on **Change…**:

    {{< figure src="/img/old/outlook-en-accounts.jpg" title="E-mail Accounts" >}}

-   On the next form, fill in:

    \* Your E-mail Address.

    -   Outgoing mailserver (SMTP): **smtp.science.ru.nl**

    -   User Name: *your login name*

    -   **Do not check**: Log on using Secure Password Authentication
        (SPA).

        {{< figure src="/img/old/outlook-en-server.jpg" title="Settings" >}}

        Now click on **More Settings…**

-   Go to the tab **Outgoing Server** and:

    \* **Check the box** My outgoing server (SMTP) requires
    authentication.

    -   **Do not check**: Log on using Secure Password Authentication
        (SPA).

    -   It seems that Outlook XP won’t use authentication unless you
        check **Log on using** and **Remember password**. Outlook 2003
        doesn’t have this bug.

        {{< figure src="/img/old/outlook-en-auth.jpg" title="Tab Outgoing Server" >}}

-   Now go to the tab **Advanced** and adjust:

    \* Outgoing server (SMTP): **25** of **587** (See [\|General
    settings] for more information.)

    -   Choose TLS for the encryption method (Note: some versions of
        Outlook refer to this as SSL!)

        {{< figure src="/img/old/outlook-en-tls.jpg" title="Tab Advanced" >}}

        This is what it looks like in a modern version of Outlook:

{{< figure src="/img/old/outlook2010-nl-tls.png" title="Tabblad Geavanceerd" >}}

## Configuration of Outlook Express

-   Choose **Tools → Accounts…**

-   Go to the **Mail** tab, select the account and click on
    **Properties**:

    {{< figure src="/img/old/outlookexpress-en-accounts.jpg" title="Internet Accounts" >}}

-   In the next window go to the tab **Servers** and adjust:

    \* Outgoing server (SMTP): **smtp.science.ru.nl**

    -   Account name: *your login name*

    -   **Do not check**: Log on using Secure Password Authentication
        (SPA).

    -   **Check the box**: My server requires authentication.

    -   Optionally click on **Settings…** to change the Logon
        Information.

        {{< figure src="/img/old/outlookexpress-en-servers.jpg" title="Tab Servers" >}}
        {{< figure src="/img/old/outlookexpress-en-auth.jpg" title="Logon Information" >}}

-   Now go to the tab **Advanced** and adjust:

    \* Outgoing server (SMTP): **25** or **587** (See [\|General
    settings] for more information.)

    -   **Check**: This server requires a secure connection (SSL).

        {{< figure src="/img/old/outlookexpress-en-tls.jpg" title="Tab Advanced" >}}

## Configuration of Eudora

` Please us a recent version of Eudora, at least version 6 or higher. The older the version of Eudora, the more trouble you will have with certificates.`

-   Choose **Tools → Options…** and click on **Sending Mail** from the
    left colomn.

-   Fill in or check:

    \* Your Email address.

    -   SMTP server: **smtp.science.ru.nl**

    -   **Check the box**: Allow authentication

    -   If it exists, you might want to check the box: Use submission
        port (587)\
        See [\|General setting] for more information on the submission
        port.

    -   Choose **If Available, STARTTLS** from the menu at the bottom of
        the window.

        {{< figure src="/img/old/eudora-smtp.jpg" title="Sending Mail Options" >}}

-   Now click on the **New Message** icon or use ctrl-N and try to send
    an email message to yourself. Eudora will probably complain about
    certificates:

    {{< figure src="/img/old/eudora-cert-reject.jpg" title="Server SSL Certificate Rejected" >}}

    Click on **Yes**. From now on Eudora should not complain any more
    about certificates when sending email.

-   Older versions of Eudora will give other error messages about
    certificates and will refuse to send messages. If this is the case,
    you have to do the following:

    \* Go the right most tab in the left column of Eudora. This tab is
    called **Personalities**. Click with the right mouse button on the
    **Dominant** personality and choose **Properties**:

    {{< figure src="/img/old/eudora-last-ssl-info.jpg" title="Last SSL Info" >}}

    \* Click on the button **Last SSL Info**.

    -   In the next window, click on **Certificate Information
        Manager**.

    -   In het volgende venster, selekteer het certificaat dat Eudora
        nog niet vertrouwt en klik op **Add To Trusted**:

        {{< figure src="/img/old/eudora-cert-manager.jpg" title="Certificate Information Manager" >}}

-   In even older versions of Eudora it can be necessary to import [the
    Microsoft (DER) version of the C&CZ root
    certificate](http://certificate.science.ru.nl/cacert.der) and add it
    to the trusted Eudora certificates. But it is better to switch to a
    more recent version of Eudora.

## Configuration of Pine

` Use `**`S`**` for SETUP and then `**`C`**` for Config and fill in:`

**``` smtp-server``   ``=``   ``smtp.science.ru.nl/user= ```***`loginname`*\
` If pine is used on a computer which is not a Sun administered by C&CZ, possibly the CA certificate will not be found. In that case the easiest way is to add the option `**`/novalidate-cert`**`, i.e.:`

**``` smtp-server``   ``=``   ``smtp.science.ru.nl/novalidate-cert/user= ```***`loginname`*

## Configuration of KMail

-   In KMail choose **Settings → Configure KMail…**, select **Accounts**
    and go to the tab **Sending** and then click on **Modify…**.

-   On the tab **General** fill in or check:

    {{< figure src="/img/old/kmail-en-smtp.jpg" title="SMTP General" >}}

-   On the tab **Security** check:

    {{< figure src="/img/old/kmail-en-tls.jpg" title="SMTP Security" >}}

## Configuration of OS X Mail

-   Pull down the **Mail** menu, then select **Preferences…**

-   Click on **Accounts** (not for mail v1.0).

-   Select your existing e-mail account in the Accounts list on the left
    side of the window, then click on the **Server Settings…** button:

    {{< figure src="/img/old/macosx-accounts.jpg" title="Mac OS X Accounts" >}}

-   Adjust the following settings:

    \* Outgoing Mail Server: **smtp.science.ru.nl**

    -   Server port: **25** or **587** (See [\|General settings] for
        more information.)

    -   **Check the box**: Use Secure Sockets Layer (SSL)

    -   **Authentication**: Password

    -   Fill in your *login name* and password.

        {{< figure src="/img/old/macosx-smtp.jpg" title="Mac OS X SMTP Options" >}}
