---
author: petervc
date: '2021-12-05T23:34:06Z'
keywords: []
lang: nl
tags:
- software
title: Mathematica
wiki_id: '150'
---
[Mathematica](http://www.wolfram.com/products/mathematica/) is een
wiskundig softwarepakket van [Wolfram Research](http://www.wolfram.com/).

[ned] C&CZ betaalt de netwerk-licentie gekocht voor het kleinschalig
gelijktijdig gebruik van Mathematica binnen FNWI. Bij [C&CZ
systeembeheer](/nl/howto/systeemontwikkeling/) zijn de
installatie- en licentie-informatie te krijgen. [/ned] [eng] C&CZ
pays for the network license for the concurrent use (by a small number
of people) of Mathematica within the Faculty of Science. One can get the
installation and license information from [C&CZ system
administration](/nl/howto/systeemontwikkeling/). [/eng]

Het gebruik van Mathematica via de license server wordt gelogd.

Op de door C&CZ beheerde Linux PCs is Mathematica in /vol/mathematica te
vinden. Op de MS-Windows PCs is Mathematica op de S-schijf (software)
beschikbaar.
