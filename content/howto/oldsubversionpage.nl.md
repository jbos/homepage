---
author: bram
date: '2016-04-26T10:32:53Z'
keywords: []
lang: nl
tags: []
title: OldSubversionPage
wiki_id: '1017'
---
De opvolger van onze Subversion service is [GitLab](/nl/howto/gitlab/)!

------------------------------------------------------------------------

## Introductie

[thumb\|alt=Subversion
logo\|Subversion](/nl/howto/bestand:subversion.png/)

Subversion (SVN) is een revisiebeheersysteem waarmee het mogelijk is om
bestanden met een of meerdere gebruikers te delen.

Hoewel de doelgroep van SVN voornamelijk in de software ontwikkeling
zit, is het ook mogelijk om subversion te gebruiken voor het beheren van
allerhande bestandssoorten.

We hebben voor FNWI een eigen subversionserver ingericht. Hierop staat
ook administratieve software voor het beheer van repositories en
gebruikersaccounts.

## Getting started

Voor het gebruik van de subversionserver heeft u de volgende zaken
nodig:

-   De URL van de repository;
-   een SVN gebruikersnaam en wachtwoord;
-   een SVN client applicatie.

Neem [contact op met C&CZ](mailto:postmaster@science.ru.nl) om een
repository aan te maken en een eigenaar toe te wijzen.

### De repository

Voor netwerktoegang tot subversionserver wordt het HTTPS protocol
gebruikt. Het web adres voor een repository van onze subversion server
is gewoonlijk in de volgende vorm:

  ----------------------------------------------------------
  https://svn.science.ru.nl/repos/{naam van de repository}
  ----------------------------------------------------------

### WebSVN

Op de subversion-server staat ook [WebSVN](http://www.websvn.info/)
geinstalleerd. Met WebSVN kunt online door de repository bladeren,
commit overzichten, fraai gekleurde broncode bestanden en verschillen
tussen bestanden bekijken. WebSVN is te bereiken met de volgende URL:

  ---------------------------------------------------------
  https://svn.science.ru.nl/wsvn/{naam van de repository}
  ---------------------------------------------------------

### Authenticatie en autorisatie

Een subversion gebruiker kan een of meerdere repositories in eigendom
hebben. Repository eigenaren kunnen lees- of lees-en-schrijfrechten aan
gebruikers toekennen. Indien nodig kunnen repository-eigenaren ook
nieuwe svn gebruikersaccounts aanmaken. In de webinterface kunnen zaken
zoals de volledige naam, het email adres en het wachtwoord worden
aangepast:

<https://svn.science.ru.nl>

#### Repository eigenaren

We raden repository-eigenaren aan om de science login namen aan te
houden voor nieuwe subversion accounts. Gelieve accounts voor externe
gebruikers met een laat streepje (\_) te laten beginnen:

  voor science gebruikers    externe gebruikers
  -------------------------- --------------------
  {science gebruikersnaam}   \_{gebruikersnaam}

svn gebruikersnamen

#### Repository gebruikers

Heeft u een science account? Dan heeft de repository eigenaar
waarschijnlijk een svn account met dezelfde loginnaam voor u gemaakt. Op
dit moment is er nog geen koppeling tussen svn en science accounts. Dat
wil zeggen dat u de bijbehorende wachtwoorden onafhankelijk van elkaar
kunt instellen. Dit zal in de toekomst waarschijnlijk gelijk getrokken
worden.

Bij het eerste gebruik van de repository zal de svn client applicatie
vragen naar uw svn gebruikersnaam en wachtwoord. Net als bij email
programma’s zullen de meeste svn clients de inloggegevens bewaren en
hoeven ze niet elke keer ingevoerd te worden.

### SVN client software

Voor de voornaamste besturingsystemen is svn beschikbaar als command
line programma. Onder Linux meestal als onderdeel van de distributie,
voor andere platforms kan het [apart geinstalleerd
worden](http://subversion.apache.org/packages.html). Typ het volgende
commando in om na te gaan of subversion up uw systeem geinstalleerd
staat:

`$ svn --version`

Er zijn ook veel verschillende grafische svn clients beschikbaar. Voor
beginnende gebruikers kan dit makkelijker in gebruik zijn dan de command
line versie.

#### Grafische SVN clients

  -------------------------------------------------------------------------------------------------------------------------------------------------
  Platform   Applicatie                               bij FNWI                                     Beschrijving
  ---------- ---------------------------------------- -------------------------------------------- ------------------------------------------------
  Windows    [TortoiseSVN](http://tortoisesvn.net/)   Optioneel voor [beheerde                     TortoiseSVN integreert naadloos in de Windows
                                                      PC’s](/nl/howto/windows-beheerde-werkplek/)   shell (Verkenner) en voorziet bestandsiconen van
                                                                                                   een stempel die de revisiestatus aan geeft.
                                                                                                   Daarnaast is voegt het items toe aan het context
                                                                                                   menu voor bestanden (rechter-muis-klik-menu).
                                                                                                   Licentie: GPL.

  Linux      [RapidSVN](http://www.rapidsvn.org/)     Ubuntu systemen                              Het is al weer een tijdje geleden dat er een
                                                                                                   nieuwe versie van RapidSVN uit is gekomen.
                                                                                                   Desondanks een prima grafische client,
                                                                                                   uitgegeven onder de GPL licentie.

  Mac OSX    \-                                                                                    
  -------------------------------------------------------------------------------------------------------------------------------------------------

Aanbevolen grafische SVN clients

## SVN gebruiken

O’Reilly heeft “Version control with Subversion” uitgebracht onder de
Creative Commons Attribution License. Het boek is in verschillende
formaten te downloaden van <http://svnbook.red-bean.com/>.

## F.A.Q.

#### Hoe log ik in op de Administratieve Interface?

Ga naar <https://svn.science.ru.nl/> en klik op ‘Authenticate’.

#### Ik ben m’n wachtwoord vergeten. Hoe kan ik het wijzigen?

Als u uw wachtwoord vergeten bent, kunt u het wijzigen met de *“Forgot
your password?”* optie op de administratieve interface. U wordt verzocht
uw login naam en email adres op te geven zoals u in de SVN administratie
bekend staat. U ontvangt dan een email met een nieuw wachtwoord. Mocht
dit niet lukken, neem dan [contact op met C&CZ](/nl/howto/contact/).
