---
author: bram
date: '2022-09-19T16:05:54Z'
keywords: []
lang: en
tags: []
title: Thunderbird Tags
wiki_id: '664'
---
## Thunderbird tags

In Thunderbird you can tag a message with one or more *tags* by clicking
on the message with the right mouse button and choosing **Tag**. There
are five standard tags with descriptions **Important**, **Work**, etc.
If the mail server is an imap server like **post.science.ru.nl**, then
the tags are stored on the server, which means that every Thunderbird
will show the tags.

It is somewhat more complicated for *new tags* that you can create. Thse
are also stored on the imap server, but will initialy not be visible in
other Thunderbird programs. One can make a new tag by clicking with the
right mouse button on a message and choosing **Tag -\> New Tag…** or
using the menu **Tools -\> Options… -\> Display -\> Tags -\> Add**. The
tag name will be used both as internal tag name and as visible
description. If you alter the tag name later on, then the internal tag
name will remain the same. The internal tag name (i.e. the *initial* tag
name) will be used as tag on the imap server. A new tag will only be
visible in another Thunderbird if a tag with the same internal tag name
exists. So it is important the choose the right initial tag name. In
particular one has to agree on a tag name if the mailbox is shared.
After that everyone can change the colour and tag name, without
consequences for the visibility of the tags.

The difference between the *internal tag name* and the *visible tag
name* is clearly visible in the **Config Editor** (**Tools -\> Options…
-\> Advanced -\> General -\> Config Editor…** and then filter on the
word **tag**). There you can also discover that the internal tag name of
the **Important** tag is **\$label1**.
