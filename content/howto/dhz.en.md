---
author: stefan
date: '2022-10-06T14:53:10Z'
keywords: []
lang: en
tags: []
title: Dhz
wiki_id: '672'
---
### DIY: Do-It-Yourself selfservice for Science logins

By logging in on the [Do-It-Yourself website](https://diy.science.ru.nl)
one can view the settings of the Science login and change most settings.

-   On the main page one can view e.g. the personnel number or student
    number, the path to the homedirectory network disk and the personal
    mail addresses.

-   By choosing **password** one can change the password. The new
    password has to contain at least 6 characters, at least 1 needs to
    be a digit and 1 needs to be a character that is neither a letter
    nor a digit. The password is used for services like attaching
    network drives or [network printers](/en/howto/printers-en-printen/),
    for the *Science* [wireless network](/en/howto/netwerk-draadloos/),
    for receiving e-mail from a C&CZ mailserver, for sending mail with
    [authenticated smtp](/en/howto/email-authsmtp/) and for the [editing
    of the wiki](/en/howto/wiki-aanpassen/) through the OpenID service.

-   By choosing **list** one can see of which simple email lists one is
    a member. Membership of [mailman
    mailinglists](http://mailman.science.ru.nl/) cannot be shown. By
    viewing a selected simple email list, one can see which email
    addresses it contains, what the spam block level is and who are the
    *owners* of the list (if there are owners). An owner of a list can
    add or remove mail addresses from the list and even add new owners.

-   By choosing **groups** one can view the Unix groups which contain
    the loginname. By viewing a selected group one can see other members
    and the *owner* (if any). An owner of a group can add or remove
    loginnames to the group and even add new owners.

-   By choosing **profile** one can choose to have a roaming profile for
    the NWI and/or B-FAC domain. With a roaming profile, program
    settings and the desktop are saved when logging out. On login to a
    different computer, these settings are restored.

-   By choosing **antispam** one can view and change the [antispam
    settings](/en/howto/email-spam/).

-   By choosing **printing** one can view the print budget that can be
    used on the C&CZ-budgetted printers. One also can see the 20 most
    recent printjobs from this budget and the currently active printjobs
    on all printers. If one can print from several budgets, one can
    choose the default budget. Owners of a budget group can also
    administer who is allowed to print from this budget and at what
    value of the print budget they want to receive an alarm mail. A
    budget group can have several owners. Each owner can add or remove
    other owners.

-   By choosing **vacation** one can have an [automatic reply (vacation
    message) to incoming mail](/en/howto/email-vakantie/).

-   By choosing **forward** one can choose a mail-address (maximum 2) to
    which mail will be forwarded and whether a local copy should be kept
    too. NOTE: Since October 14, 2008, externally forwarded mail is
    being filtered from spam to try to keep our mailservers off
    blacklists.

-   By choosing **shell** one can choose the login shell one gets when
    login into C&CZ managed Linux machines.
