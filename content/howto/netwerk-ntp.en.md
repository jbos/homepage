---
author: bram
date: '2022-09-19T16:26:21Z'
keywords: []
lang: en
tags:
- netwerk
title: Netwerk ntp
wiki_id: '96'
---
## Network time (ntp)

The system clocks on C&CZ managed Linux machines are being kept up to
date by using the Network Time Protocol (ntp). Each machine checks at
regular intervals its system clock with several designated machines on
the network and adjusts their system clock when needed. Whenever the
difference between the local system clock compared to the system clock
of other servers gets bigger than half a second the clock will be
adjusted. If the difference is smaller, the system clock will be
adjusted in small steps by slowing down or accelerating a little, to
avoid multiple large time adjustments. Because all systems adjust their
system clock to the same ntp servers, all system clocks will eventually
become synchronized. The local servers are ntp1.science.ru.nl and
ntp2.science.ru.nl.

These two servers sychronize their system clock with external *stratum
1* servers:

-   ntp0.nl.uu.net
-   ntp1.nl.uu.net

One of these ntp servers, the *ntp1.science.ru.nl*, is available for
synchronizing the system clock of your machine.

External information:

-   [\<http://www.ntp.org\>](http://www.ntp.org/)
-   <http://ntp.isc.org/bin/view/Main/WebHome>
-   [\<http://www.pool.ntp.org\>](http://www.pool.ntp.org/)
