---
author: wim
date: '2021-09-24T10:55:58Z'
keywords: [ vpn, vpnsec, ipsec, ikev2, manual ]
lang: en
tags: [ vpn, macos, ubuntu, windows ]
title: VPN
wiki_id: '71'
ShowToc: true
aliases:
- vpnsec-linux-install
- vpnsec-macos-x-strongswan-app
- vpn-mac
cover:
  image: img/2022/vpn.png
---
# VPN
C&CZ manages a VPN (Virtual Private Network) server, which makes it possible for all users to
gain secure access to the network with their [Science username and
password](../login).

The computer at home (or anywhere on the Internet) becomes part of the
campus network. In this way users can get access to services that are
normally only accessible from computers on campus. The most common of
such services are [connecting to disk shares](/en/howto/diskruimte/) or
to special servers.

## Alternatives

### EduVPN
A [RU-central VPN service EduVPN](https://www.ru.nl/ict-uk/staff/working-off-campus/vpn-virtual-private-network/)
can be used with [RU-account and RU-password](http://www.ru.nl/idmuk/).

### OpenVPN
We also offer an [OpenVPN](../openvpn) service. OpenVPN can be used for devices that are not compatible
 with our standard [Science VPN service](#science-vpn-server).

### Library
For the use of the [University library](http://www.ru.nl/ub) one does
not need VPN, because the library has a proxy website *UB Off-Campus*, that can be used
from anywhere on the Internet after logging in with your Radboud Login.

# Science VPN service
Our [IPsec](https://wikipedia.org/wiki/IPsec) VPN server address is
 `vpnsec.science.ru.nl` and uses your [Science account](../login) for authentication.

{{< notice tip >}}
You can test your VPN-connection by visiting this (great) website:

> <https://ip.science.ru.nl>

This page displays your ip address. If your Science VPN is active, your ip address should start with `131.174.`.
The hostname will resemble `vpnXXXXXX.science.ru.nl` (where XXX corresponds to the IP address).
{{< /notice >}}

## Windows 10 instructions
{{< notice tip >}}
Add a *new* VPN with server `vpnsec.science.ru.nl`, that’s all.
{{< /notice >}}

### Configuration
From Windows Settings, go to "Networks and Internet" >  "VPN" >  "+ Add a VPN connection"
Fill in and save the settings as displayed here: 

![Windows 10 vpnsec configuration screenshot](/img/2022/vpnsec-windows10-config_en.png)

These are the same settings:

| field                  | setting                     | note              |
| ---------------------- | --------------------------- | ----------------- |
| VPN provider           | Windows (built-in)          | default choice    |
| Connection name        | Science VPN                 | choose something  |
| Server name or address | `vpnsec.science.ru.nl`      | provide as stated |
| VPN type               | Automatic                   | default choice    |
| Type of sign-in info   | User name and password      | default choice    |
| User name              | [Science account](../login) | optional          |
| Password               | your password               | optional          |

Save the settings. 

### Start VPN
To start the VPN connection, click the network or wifi icon next to the clock, click the "Science VPN" label and click "Connect" as shown in this picture:

![screenshot vpn start windows 10](/img/2022/vpnsec-windows10-start_en.png)

Finally, you'll be asked for your [Science login](../login) and password:

![screenshot vpn logging in windows 10](/img/2022/vpnsec-windows10-login_en.png)

### Stop VPN
Stop the VPN connection with the disconnect button:

![screenshot vpn stop windows 10](/img/2022/vpnsec-windows10-stop_en.png)


## macOS 13 instructions
Use the following steps to configure a VPNsec connection on your macOS 13 system:

1. Download the file [vpnsec-ios.mobileconfig](/downloads/2022/vpnsec-ios.mobileconfig) (Save Link As...).

1. Double-click the `vpnsec-ios`-file in Finder and import the settings.

1. Open "System Settings"

1. Open "Network", after which you get the dialog shown in the picture below:

   ![screenshot macOS Network settings](/img/2022/macos-network-settings.png)

1. Click on the button with the three dots `...`, and go to "Add VPN Configuration"  -> "IKEv2...":

   ![screenshot Add IKEv2 VPN macOS](/img/2022/macos-add-vpn-ikev2.png)

1. Fill in the following fields:
| Field               | Value                              |
| ------------------- | ---------------------------------- |
| Server address      | `vpnsec.science.ru.nl`             |
| Remote ID           | `vpnsec.science.ru.nl`             |
| Authentication Type | Username                           |
| Username            | [your-science-loginname](../login) |
| Password            | *your password*                    |


## Ubuntu 20.04 & 22.04 instructions
Start with installing the necessary packages:

``` console
$ sudo apt install strongswan-nm network-manager-strongswan \
libstrongswan-standard-plugins  strongswan-libcharon \
libstrongswan-extra-plugins strongswan-charon libcharon-extra-plugins
```
{{< notice warning >}}
Reboot your system after installing these packages!
{{< /notice >}}

Via "Settings" -> "Network", you can then add
a VPN connection by clicking on the `+`:

![screenshot ubuntu add vpn dialog](/img/2022/newvpnubuntu.png)

Fill in the fields as shown here:

![screenshot vpn settings ubuntu](/img/2022/vpnsec-ubuntu.png)

Basically, fill in the following fields:
| field          | value                           |
| -------------- | ------------------------------- |
| Name           | Science VPN                     |
| Server Address | `vpnsec.science.ru.nl`          |
| Username       | [your-science-login](../login)  |
| Options        | [v] Request an inner IP address |
|                | [v] Enforce UDP encapsulation   |

Apply and then you can switch on the VPN through the NetworkManager
applet in the toolbar:

![screenshot enable vpn on ubuntu](/img/2022/ubuntu-enable-vpn.png)

## iPhone & iPad instructions
For iOS, you should be able to follow the [macOS instructions](#macos-instructions).

## Android instructions
Install the [strongSwan app](https://play.google.com/store/apps/details?id=org.strongswan.android). Use "IKEv2 EAP (username/password).
> NB: some special characters in the password should be escaped using a `\` 

## KDE/Plasma instructions
Pretty much the same as the general [Ubuntu instructions](#ubuntu-2004--2204-instructions), "Server" may be called
"Gateway", you don’t need to specify a key or certificate.

To make an actual VPN connection, go to the system tray and select "VPN"
from the network icon and click connect.
