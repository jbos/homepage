---
author: bram
date: '2020-10-16T09:22:29Z'
keywords: []
lang: en
tags:
- contactpersonen
title: Login aanvragen
wiki_id: '5'
---
## Request for login

Please send your request to **postmaster\@science.ru.nl**. For logins
for new employees or students, only the [department
contact](/en/tags/eerstelijns) may request new logins.

The following data are necessary to create a new login:

-   a suggestion for the new **login name** e.g. *jcove* or *katew* or
    *`<surname>`* or *`<firstname>`*
-   a suggestion for the new **mail address** (typically
    *initial*.*surname*\@science.ru.nl)
-   The **full name** of the new colleague or student
-   The **U- or S-number** if known/applicable, needed for printing
-   **Your login name**, the contactperson for this account, determines
    if the account is still needed.
-   Optionally, a private mail addres to which we can send an initial
    password. This mail also contains instructions how to set a new
    password, but it does not contain the loginname.

Optionally you can specify:

-   The check date (yyyy-mm-dd). After this date, C&CZ will send you an
    email asking you whether the login should be removed or kept for a
    certain period. (One year by default)
-   the printbudget groups the new login must be a member of. Used for
    poster-printing or 3d prints.
-   the Unix group(s) (rights) the new login must be a member of.

Initially the new login will have the same password as the requester.
This way the requester can login into [DHZ](https://dhz.science.ru.nl/)
(Do It Yourself) for the new colleague or student after which he or she
can choose their own password. Usually department contact persons are
also the owner of the printbudget and Unix group(s) of the department so
they can manage these groups via DHZ.

The password for a (new) U-account can be reset by C&CZ (you need to
bring your personel pass or valid ID for identification). To reset the
password of their S-accounts students should turn to the Dienst
Studentenzaken.

The telephone contact person of a department will register the name,
mail address, room, and phone number into RBS and the FNWI online phone
directory via <https://telmut.science.ru.nl/>

Access rights for Oracle and other concern application must be requested
from the RU application owner by the department itself.

If a mailbox on the Exchange server (typically *name*\@ru.nl) is desired
this must be specified in the request as well. C&CZ will coordinate this
request towards the [ISC](http://www.ru.nl/isc).
