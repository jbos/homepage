---
author: postmaster
date: '2007-08-14T15:11:19Z'
keywords: []
lang: en
tags: [ email, faq ]
title: Email FAQ
wiki_id: '492'
---
# Outlook

## Als Outlook lang doet over het opstarten, ongeveer een minuut en je hebt een Beheerde Werkplek (Windows XP beheerd door C&CZ in het NWI domein) of email ophalen duurt extreem lang, meerdere seconden voor een bericht binnenhalen

  ---------------------------------------- -----------------------------------------------------------------------------------------------------------------
  Zet in Outlook onder “Tools -\> Options” {{< figure src="/img/old/startup\_probleem-outlook-1.jpg" title="Instant messaging vinkje" >}}
  -\> Tabblad “Other” het vinkje uit bij   
  “Instant Messaging”, “Enable Instant     
  Messaging in Microsoft Outlook”.         

  Sluit het volgende dialoogvenster en het {{< figure src="/img/old/startup\_probleem-outlook-2.jpg" title="Bevestiging uitschakeling (na herstart)" >}}
  “Options” dialoogvenster af met “OK”. Na 
  een herstart van Outlook zou het         
  probleem zich niet meer voor moeten      
  doen.                                    
  ---------------------------------------- -----------------------------------------------------------------------------------------------------------------

## Als Outlook opgestart en je op een folder klikt (b.v. inbox) dan krijgt je rechtsonder een zandloper voordat je de lijst met mailtjes ziet

  ------------------------------------------------------- -------------------------------------------------------------------------------------------------
  Het “zandlopertje” en eventueel het venster dat je      {{< figure src="/img/old/fetching\_headers.jpg" title="Fetching new (message) headers..." >}}
  ziet, ziet er uit zoals rechts. Dan zitten er in die    
  folder (te)veel berichten. Aangezien Outlook zijn       
  “headers cache” bijwerkt als je op de IMAP-Folder       
  klikt, en deze cache bij het uitloggen leeggegooit      
  wordt, komt dit terug na iedere keer dat je op een      
  computer inlogd. Dit is normaal.                        

  ------------------------------------------------------- -------------------------------------------------------------------------------------------------

## Hoe stel ik een LDAP adresboek in?

Zie [Outlook adresboek](/en/howto/outlook-adresboek/)

## Ik zie bij een nieuwe email als ik op “To…” klik geen contactpersonen

Zie [Outlook adresboek](/en/howto/outlook-adresboek/)

## Ik zie geen contactpersonen als ik in de menubalk bij “Tools -\> Address Book…”

Zie [Outlook adresboek](/en/howto/outlook-adresboek/)

# Thunderbird

# Eudora

## Hoe stel ik mijn Eudora in?

Zie [Eudora](/en/howto/eudora/) voor een beschrijving.
