---
author: remcoa
date: '2022-10-06T15:12:45Z'
keywords: []
lang: en
tags:
- medewerkers
- studenten
- storage
title: Netwerkschijf
wiki_id: '685'
---
### Information on all network disks

-   In general you need to know the server name and share name to
    connect to a network disk.
-   Most network disks are available directly from within the Radboud
    University network. Off campus use a [VPN](/en/howto/vpn/) tunnel to
    connect to a network disk.
-   A number of network disks or shares are available for all Science
    users:
    -   The U-disk: your science home directory - the server and share
        name can be found on
        [dhz.science.ru.nl](https://dhz.science.ru.nl/) after ‘Home disk
        at’. The server name is one of ‘home1.science.ru.nl’ or
        ‘home2.science.ru.nl’. The share name is always the same as your
        science login name.
    -   The [S-disk](/en/howto/s-schijf/) with a lot of standard,
        directly executable software - server:
        software-srv.science.ru.nl, share: software.
    -   The [T-disk](/en/howto/t-schijf/) with course software - server:
        cursus-srv.science.ru.nl, share: cursus.
    -   The [install share](/en/howto/install-share/) with most of the
        installer programs for software with a campus license (contact
        us for license details) - server: install-srv.science.ru.nl,
        share: install.
-   Science departments can also [rent network discs](/en/howto/diskruimte/).

### Network disk on Windows 10

-   Open the file-explorer and go to This Pc
-   In the ribbon select Computer -\> Map network drive -\> Map network
    drive
-   Choose drive letter en fill out the Folder location
-   check both boxes and click Finish
-   Fill out the username as b-fac\\science_account and the password
-   Select Remember my credentials and click ok

The network drive is now connected

For example the S-Disk with a lot of standard software can be mapped as
‘\\\\software-srv.science.ru.nl\\software’.

### Network disk on a Mac

To connect to a network share on a Mac:

-   Activate the Finder for example by clicking somewhere on the
    Desktop.
-   Under the ‘Go’ menu choose ‘Connect to Server…’ (or type Command-K).
-   Specify the share name as *<smb://servername/sharename>* and click
    ‘Connect’.
-   Use a valid science login name and password to authenticate.

For example the S-Disk with a lot of standard software can be mapped as
‘<smb://software-srv.science.ru.nl/software>’.

### Network disk on Android

Attaching a network drive in Android can be achieved by installing an
app with SMB support. Several alternatives exist.

-   [Astro File
    Browser](https://play.google.com/store/apps/details?id=com.metago.astro)
    with the [Astro SMB
    module](https://play.google.com/store/apps/details?id=com.metago.astro.smb).
    Buy the Ad Free version if you do not want to see annoying ads.
-   [FolderSync
    Lite](https://play.google.com/store/apps/developer?id=Tacit%20Dynamics).
    This makes it possible to synchronize a network disk to and/or from
    the local SD card. Buy the non-light version if you do not want to
    see annoying ads if you open this app once in a while.
-   [SyncMe
    Wireless](https://play.google.com/store/apps/details?id=com.bv.wifisync).
    This too makes it possible to synchronize a network disk to and/or
    from the local SD card.
-   [SolidExplorer](https://play.google.com/store/apps/details?id=pl.solidexplorer2).
    The built-in storage manager allows you to make connections to all
    sorts of network disks.

The naming of the shares is the same as in Windows, see the
[diskspace](/en/howto/diskruimte/) page.

### Network disk on iPad/iPhone

See the [page on iPad usage](/en/howto/ipad/).

### Network disk on C&CZ-managed Linux

The [diskruimte pagina](/en/howto/diskruimte/) gives the details about
the network discs, that are automatically available. They can be found
in /home for the home directories, /vol for the data volumes and /www
for the website files.

### Network disk on privately managed Linux

To connect to a network on Linux (it works for xubuntu 10.4):

-   Install smbclient and cifs-utils. *sudo apt-get install smbclient
    cifs-utils*
-   Create a file */etc/samba/user* with the context

` username=USER`\
` password=PASSWORD`

Here USER is your science username and PASSWORD is your science
password. Secure this file with *chmod 400 /etc/samba/user*.

-   Create the directory */media/USER*.
-   Now put at the end of */etc/fstab* the following code

` //FILESERVER.science.ru.nl/USER /media/USER cifs credentials=/etc/samba/user,noexec,users,uid=1000,gid=100 0 0`

Again USER is your science user name FILESERVER is the host listed on
[DHZ](https://dhz.science.ru.nl/) under “Home disk at”.

-   *sudo mount /media/USER* or reboot your linux.
