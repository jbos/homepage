---
author: petervc
date: '2022-06-24T12:17:55Z'
keywords: []
lang: nl
tags: []
title: Adres
wiki_id: '99'
---
## Bezoekadres

De afdeling C&CZ bevindt zich in het Huygens-gebouw, op de begane grond,
net naast de ingang van vleugel 5. Het ruimtenummer is HG00.051, de
[systematiek van de ruimtenummers](http://www.ru.nl/fnwi/ihz/huisvesting/huisvesting/ruimtenummering/)
kan men op de website van [Interne en huisvestingszaken](http://www.ru.nl/fnwi/ihz) vinden.

~~~ txt
C&CZ, FNWI, Radboud Universiteit Nijmegen
HG00.051
Heyendaalseweg 135
6525 AJ Nijmegen
~~~

## Postadres

~~~ txt
Radboud Universiteit Nijmegen
Faculty of Science
C&CZ
Postbus 9010
6500 GL Nijmegen
~~~

Voor vragen of meer information [neem contact op met C&CZ](/nl/howto/contact).
