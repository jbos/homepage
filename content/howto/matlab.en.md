---
author: bram
date: '2022-09-19T16:29:14Z'
keywords: []
lang: en
tags:
- software
title: Matlab
wiki_id: '151'
---
### Matlab

Matlab, a "matrix laboratory" for interactive scientific calculations of  [The Mathworks Inc.](http://www.mathworks.com/) 
is an integrated software package, it combines numeric calculations, data visualization and a programming language.

### License

A number of departments in the university have bought shares in the
network license that C&CZ has arranged for the use of Matlab. When one
has bought a share, one can get the software and installation/licensing
keys of Matlab for either Linux, Mac or MS-Windows, also for older
versions of Matlab, from [C&CZ system
administration](/en/howto/systeemontwikkeling/).

This network license with a lot of toolboxes can only be used by
computers on the Internet. For standalone use, one can try to use a free
alternative like [Scilab](http://www.scilab.org) or
[Octave](http://www.gnu.org/software/octave/). The license purchase
costs for on average 1 concurrent Matlab-user are EUR 241,- excl. VAT.
The average is determined by counting the maximum number of concurrent
users in each week. These maxima are then averaged over the whole year.
Maintenance costs depend on the number of users, but are roughly EUR
50,- per year.

When a department regularly uses Matlab without yet having bought a share in the network license, one should contact [Astrid Linssen](/en/howto/overcncz-wieiswie-secretariaat/).   The use of Matlab is logged on the license server. At the end of each year C&CZ calculates the average peak-use per week of each department, this should correspond to the number of Matlab-licenses bought. During the highest peak-use a department thus uses more licenses than bought, but this is no problem due to the shared network license: not all departments peak at the same time.  `

`Matlab is available on the MS-Windows and Linux machines managed by C&CZ.

### Integrating Matlab in Python

It is possible to use Matlab in python, see
[matlab-engine-for-python](https://nl.mathworks.com/help/matlab/matlab-engine-for-python.html).
On all Linux clients and servers this has been setup for Matlab R2020a
and R2019b. See /opt/matlab\*/Matlab-Python.Readme for details.

### Online Matlab course

If your department has a Matlab license and you want to learn Matlab
interactively with the Matlab Academy (Mathworks Training Services),
visit [the Matlab Academy](https://matlabacademy.mathworks.com/) and
create an account with an email address ending in: @science.ru.nl,
@donders.ru.nl, @pwo.ru.nl, @let.ru.nl, @fm.ru.nl, @ai.ru.nl,
@socsci.ru.nl or @ru.nl, and use the Activation Key that
[C&CZ](/en/howto/contact/) can provide.

## Known problems

### Toolkit problems after upgrading to new version

A toolkit seems to be
missing, the reason might be a Matlab searchpath that has not been
adjusted for the new version. See [Mathworks
help1](http://www.mathworks.com/help/techdoc/matlab_env/br7ppws-1.html#br7poxu-10)
and [Mathworks
help2](http://www.mathworks.com/help/techdoc/matlab_env/br7ppws-1.html#br8p4gj-1).

A fast fix, in Matlab is:

`restoredefaultpath; matlabrc`

### Firewalls

-   When one is separated from the license server by a firewall, one
    needs to open 2 ports: one on which the license server is listening
    and 1 return portnumber. Contact C&CZ for details, the portnumbers
    might change due to an upgrade of Matlab.
