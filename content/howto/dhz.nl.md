---
author: stefan
date: '2022-10-06T14:53:10Z'
keywords: []
lang: nl
tags: []
title: Dhz
wiki_id: '672'
---
### DHZ: Doe-Het-Zelf selfservice voor Science logins

Door in te loggen op de [Doe-Het-Zelf
website](https://dhz.science.ru.nl) kan men de instellingen van de
Science login bekijken en de meeste instellingen ook wijzigen.

-   Op de hoofdpagina staat o.a. het studentnummer of personeelsnummer,
    het pad van de eigen homedirectory netwerkschijf en de persoonlijke
    e-mail adressen.

-   Door **wachtwoord** te kiezen, kan het wachtwoord worden gewijzigd.
    Het nieuwe wachtwoord moet uit minstens 6 tekens bestaan en dient
    tenminste 1 teken te bevatten dat geen letter en geen cijfer is. Het
    wachtwoord wordt o.a. gebruikt voor het aankoppelen van
    netwerkschijven en [netwerkprinters](/nl/howto/printers-en-printen/),
    voor het *Science* [draadloze netwerk](/nl/howto/netwerk-draadloos/),
    voor het ophalen van e-mail van een mailserver van C&CZ, voor het
    versturen van [authenticated smtp](/nl/howto/email-authsmtp/) en voor
    het [aanpassen van de wiki](/nl/howto/wiki-aanpassen/) via de OpenID
    service.

-   Door **lijsten** te kiezen, kan men zien tot welke e-mail lijsten
    men behoort. Alleen lidmaatschap van [mailman
    mailinglists](http://mailman.science.ru.nl/) kan niet worden
    getoond. Door een geselecteerde lijst te bekijken, is te zien welke
    mailadressen er verder in voor komen, wat het spambloknivo van de
    lijst is en wie de *eigenaren* van de lijst zijn (mits er eigenaren
    zijn). Een eigenaar van een lijst kan mailadressen van de lijst
    halen en nieuwe toevoegen en zelfs nieuwe eigenaren toevoegen.

-   Door **groepen** te kiezen, kan men zien tot welke Unix groepen
    (voor toegang tot netwerkschijven, mappen of websites) men behoort.
    Door een geselecteerde groep te bekijken, is te zien wie er verder
    lid is van die groep en wie de *eigenaren* van de groep zijn (mits
    er eigenaren zijn). Een eigenaar van een groep kan loginnamen aan de
    groep toevoegen of van de groep verwijderen en zelfs nieuwe
    eigenaren toevoegen.

-   Door **profiel** te kiezen, is het mogelijk om een zgn. zwervend
    profiel aan en uit te zetten voor het domein NWI en/of B-FAC. Een
    zwervend profiel zorgt ervoor dat allerlei instellingen van
    programma’s en het bureaublad bewaard worden bij uitloggen. Bij
    inloggen op een andere pc worden deze instellingen weer opgehaald.

-   Door **antispam** te kiezen, is [werking en de mogelijke
    gebruikersinstellingen](/nl/howto/email-spam/) van het spamfilter aan
    te passen.

-   Door **printen** te kiezen, ziet men het
    [budget](/nl/howto/hardware-budgetten/) voor afdrukken op de door
    C&CZ gebudgetteerde [printers](/nl/howto/printers-en-printen/). Ook
    ziet men de laatste 20 printopdrachten van het budget en de
    momenteel actieve printopdrachten op alle printers. Als men van
    meerdere printerbudgetten kan afdrukken, kan men het standaardbudget
    instellen. Eigenaren van een budgetgroep kunnen ook bijhouden wie er
    mag afdrukken van het budget en bij welke waarde van het printbudget
    een alarmmail gestuurd moet worden. Een budgetgroep kan meerdere
    eigenaren hebben en een bestaande eigenaar kan zelf eigenaren
    toevoegen en verwijderen.

-   Door **afwezigheid** te kiezen, is het mogelijk om [automatisch
    bericht terug te sturen bij afwezigheid](/nl/howto/email-vakantie/).

-   Door **doorsturen** te kiezen, kan worden ingesteld dat mail naar
    (maximaal twee) mailadressen wordt doorgestuurd en of men dan nog
    een lokale kopie wil houden. N.B.: vanaf 14 oktober 2008 wordt
    extern doorgestuurde mail op spam gefilterd, om te voorkomen dat
    onze mailservers op zwarte lijsten komen.

-   Door **shell** te kiezen, is de loginshell te kiezen die men krijgt
    bij het inloggen op door C&CZ beheerde Linux machines.
