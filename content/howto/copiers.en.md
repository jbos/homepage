---
author: bram
date: '2022-09-19T16:27:50Z'
keywords: []
lang: en
tags: []
title: Copiers
wiki_id: '36'
---
## Copiers

### New system: Peage

All Konica Minolta MFPs have moved to [Peage](/en/howto/peage/): the
RU-wide uniform system for budgeted printing, copying and scanning.
