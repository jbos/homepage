---
author: petervc
date: '2015-12-02T14:57:41Z'
keywords: []
lang: nl
tags:
- email
title: Email spam
wiki_id: '14'
---
## Spamblokkering en -filtering in elektronische post

### Blokkering op basis van het adres van de computer die de mail aanbiedt

Het blokkeer niveau kan per gebruiker aangepast worden via de [Doe Het
Zelf website](http://dhz.science.ru.nl/). Bij binnenkomst op de
**science** mailserver wordt direct gecontroleerd of het geadresseerde
mail-adres wel bestaat en of de geadresseerde gebruiker de afleverende
computer op zijn blokkeerniveau heeft staan. Anders wordt de mail niet
geaccepteerd. Wanneer de mail door een hoger blokkeer niveau
tegengehouden zou worden, wordt de mail wel geaccepteerd, maar
doorgestuurd met een mail-header-regel “X-Would-Be-Blocked-By:”.

Er zijn vier niveau’s van blokkering: ‘none’, ‘light’, ‘medium’ en
‘heavy’, waarbij ‘medium’ de standaard aan te raden waarde is.

-   none: er vindt geen blokkering plaats.
-   light: blokkering met gebruikmaking van de volgende
    lijsten:

1.  whitelist.science.ru.nl: C&CZ’s eigen witte lijst: machines die op
    zwarte lijsten staan of gestaan hebben, maar waarvan veel van onze
    gebruikers toch graag mail willen ontvangen. De lijst bevat nu
    (gelukkig) maar een paar entries: een list-server van Surfnet en de
    spamprovider van het RadboudUMC.
2.  blacklist.science.ru.nl:C&CZ’s eigen zwarte lijst: machines waar
    anderen blijkbaar (nog) geen last van hebben, maar C&CZ wel. De
    lijst is nu (gelukkig) nog zeer kort en bevat een aantal machines
    die de C&CZ mailservers gebombardeerd hebben met spam en/of
    virussen.

-   medium: bovenop de light-lijsten komen:
    -   bl.spamcop.net: Een database van bekende en/of gerapporteerde
        spammers per ‘server IP-addres’.
    -   sbl.spamhaus.org: Een database van geverifieerde spam bronnen
        (inclusief spammers, spam gangs en spam support services),
        beheerd door het 
        [Spamhaus](http://www.spamhaus.org) Project team.

Wanneer men een lager blokkeerniveau heeft dan de standaard ‘medium’
(dus ‘light’ of ‘none’), dan wordt mail die bij ‘medium’ blokkering
geweigerd zou worden, doorgelaten met een waarschuwingsstempel
“X-Would-Be-Blocked-By: Medium”.

-   heavy: Bovenop de ‘medium’ lijsten komt:

1.  dnsbl.sorbs.net: [SORBS](http://www.dnsbl.au.sorbs.net/) Een
    database van diverse soorten spam bronnen.
2.  xbl.spamhaus.org: [Spamhaus](http://www.spamhaus.org) Exploits Block
    List: illegale exploits, incl. open proxies (HTTP, socks,
    AnalogX, wingate, etc), worms/viruses met ingebouwde spam engines [en andere typen van trojan-horse
    misbruik]and other types of ‘trojan horse’ abuse.]

Mail die door het ‘heavy’ blokkering geweigerd zou worden, wordt bij een
‘medium’ blokkeerniveau doorgelaten met een waarschuwingsstempel
“X-Would-Be-Blocked-By: Heavy”.

C&CZ geeft nieuwe logins het ‘medium blokkering’ als standaard niveau.
Dit heeft een klein risico dat er gewenste mail geweigerd wordt, maar
blokkeert voor de meeste gebruikers duidelijk meer spam dan de ‘light’
blokkering. Mail die door een zwaardere blokkering geweigerd zou worden,
wordt weliswaar doorgelaten, maar met het waarschuwingsstempel
(X-Would-Be-Blocked-By:). De gebruiker kan zelf zien aan de mail die met
waarschuwingsstempels doorkomt, hoeveel gewenste post geweigerd zou
worden bij een zwaarder niveau van blokkering (‘valse positieven’). Het
waarschuwingsstempel kan men vaak ook gebruiken om de post in aparte
bakken voor te sorteren, bv. met [Sieve](/nl/howto/email-sieve/).

Wanneer men mail van andere adressen automatisch door laat sturen
(forwarden) naar het **science** adres, kan het blokkeren op deze manier
niet goed werken. Onze mailserver ziet dan namelijk niet de originele
spam-versturende computer, maar de mailserver die het bericht
doorstuurt.

### Filtering op basis van de inhoud van de mail

Ondanks de bovenstaande manier van blokkeren, kan men nog steeds veel
last van ongewenste spam-mail hebben. In zo’n geval kan men niet veel
anders dan op de inhoud van de mail filteren. Wanneer men dit niet op de
eigen computer doet, maar op de mailserver, dan kost dit wel veel meer
capaciteit dan het bovenstaande blokkeren op aanbiedende computer.

Op alle door C&CZ beheerde mailservers in `ru.nl` wordt voor het
filteren op inhoud, zowel voor virussen als spam,
[MIMEDefang](http://www.mimedefang.org/) gebruikt, die voor het bepalen
van het spamniveau gebruik maakt van
[SpamAssassin](http://www.spamassassin.org/) met centraal Bayes-filter.

Door SpamAssassin als spam herkende mail heeft als laatste bijlage een
samenvatting van de redenen waarom deze mail als spam herkend werd.
Hierbij tellen veel verschillende zaken mee: commercieel dan wel sexueel
getinte woorden, eigenschappen van adressen en header-regels,
formattering, gebruik van hoofdletters, etc. Daarnaast wordt er door
C&CZ een statistische (Bayes) lijst bijgehouden van woorden die in
gewone mail en in spam voorkomen. Elk van deze zaken geeft een bijdrage
aan de ‘totale spamscore’ van het mailtje. Wanneer de score meer dan 5.0
is, wordt de mail standaard als spam beschouwd. Voor gebruikers waarbij
de mail afgeleverd wordt op de IMAP mailserver `post.science.ru.nl`
wordt de als spam gemarkeerde mail automatisch in de Spam map op de
mailserver gezet. Uit deze map wordt mail van meer dan 14 dagen oud
automatisch verwijderd.

Via de [Doe Het Zelf website](http://dhz.science.ru.nl/) is het mogelijk
om mail adressen en domeinen op een witte lijst te zetten. Mail van deze
afzenders wordt dan nooit automatisch in de Spam map gezet, ook al is de
mail door SpamAssassin als spam gemarkeerd. Met behulp van
[Sieve](/nl/howto/email-sieve/) kan mail bij binnenkomst op de server op
andere manieren verwerkt worden.

## Overige filtering

Voor complexe filtering, zoals het doorsturen of in mappen opslaan
afhankelijk van de afzender/onderwerp/inhoud van de mail of zelfs het
uitzetten van het spamfilter, zie de [Sieve
pagina](/nl/howto/email-sieve/).

