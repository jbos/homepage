---
author: petervc
date: '2022-06-28T14:57:16Z'
keywords: []
lang: nl
tags:
- hardware
- hardware
title: Huisleverancier PC's
wiki_id: '42'
---
### Huisleverancier: Dustin (voorheen CentralPoint/Scholten Awater)

De huisleverancier voor pc’s voor de Radboud Universiteit is sinds 1
maart 2006 [Dustin (voorheen CentralPoint/Scholten
Awater)](http://www.dustin.nl/) na diverse Europese aanbestedingen.

### Kernassortiment

Het [ISC](http://www.ru.nl/isc) stelt een kernassortiment samen van
geteste producten met vaak een extra gunstige prijs. Deze zijn via BASS
bij Centralpoint te zien in “Mijn assortiment” als RU-laptop, RU-desktop
etc.

===

Via het [inkoopsysteem BASS](https://www.bass.ru.nl) kan men op [de
website van CentralPoint](http://www.centralpoint.nl/) RU-prijzen zien.

### Bestelprocedure

U dient uw bestelling via het [financiële systeem](/nl/howto/bass/) op te
geven aan de [afdeling inkoop](http://www.ru.nl/inkoop). Offertes kunt u
als bijlage meesturen.

### Basis-configuratie

Er zullen altijd enkele basis-configuraties aangeboden worden voor
desktop, monitor en laptop. Deze basis-pc’s zijn getest: ze kunnen met
een standaard procedure, via het netwerk, geïnstalleerd worden. N.B.: De
RU heeft een campuslicenties voor UPGRADE van MS-Windows op desktops. U
kunt dus de goedkoopste versie van MS-Windows te bestellen
(home-edition, deze is in de prijs van de basis-pc inbegrepen). Bij de
netwerkinstallatie wordt de professionele versie geïnstalleerd. Zij die
geen gebruik van de netwerkinstallatie (kunnen) maken, kunnen bij C&CZ
[een Windows CD/DVD](/nl/howto/microsoft-windows/) lenen. Upgrade van de
ene (NL) taal naar een andere (UK) schijnt problematisch te zijn, dan
kan men beter een nieuwe installatie doen. Als u een Linux operating
systeem gebruikt, hoeft u geen MS-Windows licentie te kopen. Dit dient u
bij uw bestelling te vermelden. Er wordt dan Free DOS opgezet door
CentralPoint.

### Garantie

Op de producten uit het kernassortiment zit standaard 3 jaar garantie
met Next Business Day reparatie. Soms kan men een vierde jaar bijkopen.
PAS OP: OP ANDERE PRODUCTEN WORDT DE STANDAARD\_FABRIEKSGARANTIE
GEGEVEN. KIJK DUS GOED NAAR DE VOORWAARDEN IN DE PRIJSLIJST. In geval
van reparaties zal C&CZ ervoor zorgen dat de monteur op de juiste plaats
komt. Kijk voor het reparatieformulier op
[Reparaties](/nl/howto/reparaties/).

### Verpakking

Ieder wordt dringend verzocht om de verpakking bij het logistiek centrum
af te (laten) leveren. Centralpoint neemt deze n.l. kosteloos mee, wat
de faculteit afvoerkosten bespaart.

### Afvoer

De RU heeft een
[afvalstoffenregeling](https://www.radboudnet.nl/publish/pages/803506/afvalstoffenregeling_radboud_universiteit_1_1_augustus_2019.pdf).
Computerapparatuur kan aangeboden worden aan het Logistiek Centrum
telefoon 52156. U bent zelf verantwoordelijk voor het verwijderen van
data of datadragers.
