---
author: petervc
date: '2022-11-24T17:03:10Z'
keywords: []
noage: true
lang: en
tags:
- studenten
title: Login
wiki_id: '163'
---
Every employee and every student of the Faculty of Science is entitled
to get a (free) *personal* **science** login. With this login all
services offered by computers that are managed by
[C&CZ](/en/tags/overcncz) can be used. The loginname is based on one’s
own name, e.g. *johndoe*. Note that the loginname (often less than 16
non-capital characters, without **.** (dot) or **@…**) are not the same
as the email address. There is a [list of
Science-services](/en/howto/nieuwe-studenten/) that can be used with this
Science login.

So-called *functional logins*, which are not coupled to one natural
person, are an unwanted exception. C&CZ tries to make sure that all services
can be used without the need for more than 1 login per person.

Logins for new employees are created [on request from the contact person
of the department](/en/howto/login-aanvragen/). Also [guest
logins](/en/howto/gastlogin/) are possible.

Logins for students are created automatically for all enlisted students that
need a Science login from the information in Osiris and communicated to the
student via e-mail to the registered address.

For special requests, see the contact person of your study or department.
E.g. if you want to keep your Science account a while longer while you
do not work or study at Radboud University anymore, C&CZ will only do
this after a request from the contact person of your study or
department. In most cases, visit the [Education
Center](http://www.ru.nl/fnwi/onderwijs/onderwijscentrum/), more
specificlly the Student Service Desk, 52200. In
the event that the Student Service Desk does not have the latest
information, a temporary login can be created through the [contact
person for the specific
study](http://www.ru.nl/fnwi/onderwijs/studieadviseurs/).
