---
author: sioo
date: '2022-09-13T09:27:00Z'
keywords: []
lang: en
tags:
- email
title: Ldap adresboek
wiki_id: '707'
---
### Faculty of Science address book in Outlook/Thunderbird

Note that this only works within the RU network (or VPN)

## Outlook

1.  In Outlook choose Tools-\>Email Accounts
2.  Next select “Add a new directory or address book” and click on
    “Next”
3.  Select “Internet Directory Service (LDAP)” and click on “Next”
4.  Fill in server name: ldap.science.ru.nl and click on “More Settings”
5.  Select the Tab Search and fill in as search base: o=addressbook
6.  click on “OK”, subsequentlty on “Next” and finally on “Finish”
7.  Close Outlook and start it again.

You can now search in this address book but a more convenient
construction is the following: start a new mail message and fill in part
of the name in the “To:” field and press “Control-K”. A list of names
will popup that match the typed text, choose the name you were looking
for.

Unfortunately Outlook stops searching the address books as soon as it
has a match in one address book. This implies that if LDAP is the second
address book and you have a contact that matches in your personal
address book you will not get results from the LDAP address book. In
that case only explicitly searching in the LDAP address book will work,
you can add the found results to you personal address book so the next
time it will find it there.

## Thunderbird

1.  In Thunderbird choose Tools-\>Address Book
2.  Next choose from the menu in the Address Book window: File -\> New
    -\> Ldap Directory …
3.  Choose a Name: for example Science
4.  Hostname: ldap.science.ru.nl
5.  Base DN: o=addressbook
6.  In the Advanced tab you can change the maximum number of returned
    matches (default is 100)
7.  Close by clicking “OK”
8.  To turn on Autocompletion for the LDAP address book, choose in
    “Preferences” the “Composition” Tab
9.  Then in Addressing check Autocompletion for the Directory Server
    (Choose Science in the drop down menu)

subsequently as soon as you start typing a name in a “To:” field a list
with possible matches will popup from your own address book AND the LDAP
address book.

If you want to search multiple LDAP servers then there is a Thunderbird
plugin [Multi LDAP
Enabler](https://addons.mozilla.org/nl/thunderbird/addon/5684) that
makes autocompletion possible from multiple LDAP servers.

## OsX 10.7

Open addressbook.

1.  In Preferences (Cmd+,) go to Accounts and click on the plus at the
    bottom right.
2.  At Account type, choose: LDAP
3.  Server address: ldap.science.ru.nl
4.  Click Continue.

1.  Description: your choice (e.g. Science)
2.  Search base: o=addressbook
3.  Scope: Subtree, Authentication: None.
4.  Click Create.

At View-Groups (Cmd+3) you will see your new directory.
