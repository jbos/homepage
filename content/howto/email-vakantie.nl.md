---
author: jverdurm
date: '2008-04-09T22:19:36Z'
keywords: []
lang: nl
tags:
- email
title: Email vakantie
wiki_id: '15'
---
## Automatisch bericht terugsturen bij binnenkomende elektronische post

De instellingen voor het automatisch terugsturen van een email bericht
wanneer een nieuw bericht binnenkomt zijn aan te passen op de
[Doe-Het-Zelf website](http://dhz.science.ru.nl).

Wanneer men automatisch een (afwezigheids)bericht terug laat sturen bij
inkomende post, krijgt elke afzender van een mail die direct aan u
geadresseerd is (geen BCC:), eenmalig een mailtje terug met de door u
opgegeven tekst.

Wanneer men weer aanwezig is en het automatisch bericht versturen dus
weer afzet, blijft het (afwezigheids)bericht bewaard voor een volgende
keer.
