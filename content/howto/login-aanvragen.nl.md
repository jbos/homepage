---
author: bram
date: '2020-10-16T09:22:29Z'
keywords: []
lang: nl
tags:
- contactpersonen
title: Login aanvragen
wiki_id: '5'
---
## Login aanvragen

Aanvragen moeten worden gemaild naar **postmaster\@science.ru.nl**. Voor
logins voor medewerkers of studenten worden alleen aanvragen van
[afdelingscontactpersonen](/nl/tags/eerstelijns) gehonoreerd.

Geef in de mail aan:

-   een suggestie voor de nieuwe **loginnaam**, bijv *ajakobsen* of
    *pieterb* of *`<achternaam>`* *`<voornaam>`*
-   een suggestie voor het nieuwe **mail-adres** (typisch
    *initialen*.*achternaam*\@science.ru.nl)
-   de **volledige naam** van de nieuwe medewerker of student
-   het **U- of S-nummer** indien bekend of van toepassing, dit is nodig
    voor bijv. printen.
-   **uw loginnaam** voor de contactpersoon van deze login, die bepaalt
    of de login mag blijven bestaan.
-   optioneel een prive mailadres waar een initieel wachtwoord naar toe
    moet worden gestuurd. In deze mail staan instructies voor het
    instellen van een nieuw wachtwoord, maar geen vermelding van de
    loginnaam.

Optioneel kan worden aangegeven:

-   de controledatum (jjjj-mm-dd). Na het verstrijken van deze datum
    krijg je eem mail met de vraag of het account opgeruimd of verlengd
    moet worden. (Standaard 1 jaar)
-   toegang tot een rekencluster
-   de printbudget-groep(en) waar de nieuwe login toe moet behoren (voor
    posterprinten en 3d printen)
-   unix-groepen (rechten) waartoe de nieuwe login moet behoren

De nieuwe login krijgt hetzelfde wachtwoord als de aanvrager. Zo kan de
aanvragen voor de nieuwe medewerker of student inloggen in
[DHZ](https://dhz.science.ru.nl/) (Doe Het Zelf) zodat deze zelf zijn of
haar wachtwoord kan kiezen. Afdelingscontactpersonen zijn in de regel
eigenaar van de printbudget-groep(en) en Unix-groep(en) van de afdeling
en kunnen in DHZ de groepen inzien en wijzigen.

Het wachtwoord op een (nieuw) U-nummer kan door C&CZ worden gereset
(neem ter identificatie de personeelspas of een geldig ID mee). Voor het
resetten van het wachtwoord bij een S-nummer dient de student zich te
wenden tot de Dienst Studentenzaken.

De telefooncontactpersoon van de afdeling registreert naam, mail-adres,
kamer- en telefoonnummer in RBS en de FNWI online telefoongids via
<https://telmut.science.ru.nl/>

Rechten op Oracle en andere concern-applicaties moeten door de afdeling
zelf worden aangevraagd bij de betreffende beheerder.

Als er een mailbox op de exchange server ([ISC](http://www.ru.nl/isc))
nodig is (typisch van de vorm *naam*\@ru.nl) dan moet dat bij de
aanvraag opgegeven worden. C&CZ coordineert deze aanvraag richting het
[ISC](http://www.ru.nl/isc).
