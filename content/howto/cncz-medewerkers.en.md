---
author: petervc
date: '2022-06-28T15:11:11Z'
keywords: []
lang: en
tags:
- overcncz
title: CNCZ Medewerkers
wiki_id: '116'
---
-   Department head
    -   {{< author "bbellink" >}}
-   Secretary
    -   {{< author "alinssen" >}},
        [secretariaat](/en/howto/secretariaat/)
-   System administration and
    development
    -   {{< author "remcoa" >}}
    -   {{< author "petervc" >}}
    -   {{< author "bram" >}}
    -   {{< author "miek" >}}
    -   {{< author "wim" >}}
    -   {{< author "ericl" >}}
    -   {{< author "sioo" >}}
    -   {{< author "polman" >}}
    -   {{< author "visser" >}}
-   Application development
    -   {{< author "remcoa" >}}
    -   {{< author "alexander" >}}
    -   {{< author "fmelssen" >}}
-   Operations/Helpdesk/User
    support/datacenter
    -   {{< author "bjorn" >}}
    -   {{< author "john" >}}
    -   {{< author "stefan" >}}
    -   {{< author "bertw" >}}
    -   {{< author "dominic" >}}

There also is a list of [former employees](/en/howto/oud-medewerkers/).
