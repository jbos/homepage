---
author: petervc
date: '2019-09-26T15:40:21Z'
keywords: []
lang: nl
tags:
- software
title: Intel compilers
wiki_id: '877'
---
### Intel compilers (2019u5 / 2019u4 / 2019 / 2014)

C&CZ heeft samen met TCM en Theoretische Chemie twee licenties voor
gelijktijdig gebruik van de [Intel Parallel Studio XE voor
Linux](https://software.intel.com/en-us/parallel-studio-xe) aangeschaft.
Verschillende versies zijn geïnstalleerd in `/vol/opt/intelcompilers` en
beschikbaar op o.a. [clusternodes](/nl/howto/hardware-servers/) en
[loginservers](/nl/howto/hardware-servers/). Om de omgevingsvariabelen
goed te zetten, moeten SH/BASH-gebruikers vooraf uitvoeren voor de
nieuwste versie:

    source /vol/opt/intelcompilers/intel-2019u5/composerxe/bin/compilervars.sh intel64

en CSH-gebruikers:

    setenv arch intel64
    source /vol/opt/intelcompilers/intel-2019u5/composerxe/bin/compilervars.csh intel64

Daarna levert `icc -V` het versienummer. Voor de oudere versies moet
“2019u5” veranderd worden in “2019u4”, “2019” resp. “2014”.\
Een erg handige resource is
[intel-mkl-link-line-advisor](https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor/)
waarmee je compiler- en linker-opties kunt opvragen voor het gebruik van
de MKL.

### Documentation for the previous version (2011)

#### Compiling Fortran (/opt/intel/bin/ifort)

-   [Intel Fortran Composer XE 2011 Getting Started
    Tutorials](http://www.theochem.ru.nl/composerxe/Documentation/en_US/tutorials_f/index.htm)
-   [Intel Fortran Compiler XE 12.0 User and Reference
    Guides](http://www.theochem.ru.nl/composerxe/Documentation/en_US/compiler_f/main_for/index.htm)

#### Math Kernel Library (mkl, linking blas, lapack)

-   [Intel Math Kernel Library for Linux OS User’
    Guide](http://www.theochem.ru.nl/composerxe/Documentation/en_US/mkl/mkl_userguide/index.htm)
    -   [Linking with the Intel Math Kernel
        Library](http://www.theochem.ru.nl/composerxe/Documentation/en_US/mkl/mkl_userguide/MKL_UG_linking_your_application/MKL_UG_linking_your_application.htm)
-   [Intel Math Kernel Library Reference
    Manual](http://www.theochem.ru.nl/composerxe/Documentation/en_US/mkl/mkl_manual/index.htm)

#### Intel Cluster Studio 2011

-   [Intel Cluster Studio 2011 for Linux\* OS - index to all local
    documentation](http://www.theochem.ru.nl/ics/2011.0.013/doc/Doc_Index.htm)
-   [Intel® MPI Library for Linux OS Documentation
    Index](http://www.theochem.ru.nl/mpi/Doc_Index.html)

### How to create a standalone MKL version of BLAS and LAPACK shared libraries ?

This is described in detail in [Building Custom Shared
Objects](http://www.theochem.ru.nl/composerxe/Documentation/en_US/mkl/mkl_userguide/MKL_UG_linking_your_application/Building_a_Custom_Shared_ObjectsDLLDynamically_Linked_Shared.htm)

-   Create a new directory (e.g. \~/lib)

` mkdir ~/lib`\
` cd ~/lib`

-   Copy these files:

` cp /opt/intel/composerxe/mkl/tools/builder/{makefile,blas_list,lapack_list} ~/lib`

-   Set the MKLROOT variable (in bash):

` MKLROOT=/opt/intel/mkl`\
` export MKLROOT`

In tcsh use:

` setenv MKLROOT /opt/intel/mkl`

-   Make the shared libraries libblas\_mkl.so and liblapack\_mkl.so

` make libintel64 export=blas_list interface=lp64  threading=parallel name=libblas_mkl`\
` make libintel64 export=lapack_list interface=lp64  threading=parallel name=liblapack_mkl`

The options are described
[here](http://www.theochem.ru.nl/composerxe/Documentation/en_US/mkl/mkl_userguide/MKL_UG_linking_your_application/Specifying_Makefile_Parameters.htm)

The newly created libblas\_mkl.so and liblapack\_mkl.so require

` /opt/intel/lib/intel64/libiomp5.so`\
` `

to work. On the cluster nodes this file is automatically linked when
required.

## Using the MKL BLAS and LAPACK shared libraries (with Scilab)

This should work for any executable that uses a dynamically linked blas
or lapack. We use Scilab as an example.

-   Make sure we have an executable, not just a script that calls the
    executable:

` file scilab-bin`

The output looks something like this:

` scilab-bin: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), for GNU/Linux 2.6.15 ...`

-   Determine the exact name that is used by the executable:

` ldd scilab-bin | grep blas`

The output could be:

` libblas.so.3gf => ~/sciab-5.4.1/lib/thirdparty/libblas.so.3gf`

-   Replace the library with a link to the MKL version

` cd ~/sciab-5.4.1/lib/thirdparty/`\
` rm libblas.so.3gf`\
` ln -s ~/lib/libblas_mkl.so libblas.so.3gf`

Also follow this procedure for lapack.

-   To use more than one thread, i.e., for parallel computation, set:

` MKL_NUM_THREADS=4`\
` export MKL_NUM_THREADS`

This example will use 4 cores.

-   To check the number of cores available, use:

` cat /proc/cpuinfo | grep processor | wc`
