---
author: caspar
date: 2013-04-12 18:19:00
tags:
- studenten
- medewerkers
- docenten
title: CMS functional manager Wim Janssen stationed at C&CZ
---
On April 15 Wim G.H.M. Janssen (IMAPP and facultary CMS functional
manager) will “move in” with C&CZ. Wim will remain employed by IMAPP but
will be able to perform his work more effectively from his new location
at C&CZ because of our frequent collaboration. Moreover C&CZ will take
over some of his tasks thus creating the possibility for new
developments. There may be some confusion: for many years C&CZ also has
an employee Wim P.J. Janssen, so more often a Wim-Wim-situation will
arise.
