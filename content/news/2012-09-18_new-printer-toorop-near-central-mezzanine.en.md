---
author: petervc
date: 2012-09-18 16:07:00
tags:
- studenten
- medewerkers
title: New printer 'toorop' near central mezzanine
---
A new [b/w laser printer](/en/howto/printers-en-printen/) ‘toorop’, an HP
LaserJet 9050 PCL 6, has been installed near HG01.038, between the two
central stairs on the first floor of the Huygens building. Soon the
mezzanine next to it will be opened, a study landscape without desktop
pc’s, suited for laptop usage. There are more than 60 network and power
outlets. The network outlets will be opened up when the test with
[802.1x network
authentication](http://en.wikipedia.org/wiki/IEEE_802.1X) has been done.
Until then, only the [wireless network](/en/howto/netwerk-draadloos/) can
be used. Two new
[wireless-N](http://nl.wikipedia.org/wiki/IEEE_802.11#802.11n) access
points ought to give enough connectivity.
