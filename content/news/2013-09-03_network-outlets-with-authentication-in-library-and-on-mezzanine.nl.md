---
author: mkup
date: 2013-09-03 15:17:00
tags:
- studenten
- medewerkers
- docenten
title: Inprikpunten met authenticatie in Bibliotheek en op insteekverdieping
---
Sinds vandaag zijn in de leeszaal van de bibliotheek en op de
insteekverdieping respectievelijk 32 en 64 geauthenticeerde
netwerk-inprikpunten beschikbaar met een snelheid van 100 Mb/s. Sluit
een computer aan met een UTP-kabel en start een browser. Dan komt men
terecht op een website waar men zich kan registreren met het
personeelsnummer (`U123456`) of studentnummer (`s123456`). Daarna heeft
men toegang tot Internet. Binnenkort zal het ook mogelijk zijn om te
registreren met `scienceloginnaam@science.ru.nl`. De bedoeling is om in
de toekomst alle openbaar toegankelijke actieve inprikpunten van
authenticatie te voorzien. De techniek hierachter is Qmanage van
[Quarantainenet](http://www.quarantainenet.com/?language=nl;page=main-home).
