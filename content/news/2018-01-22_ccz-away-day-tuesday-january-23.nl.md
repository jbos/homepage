---
author: bbellink
date: 2018-01-22 10:00:00
tags:
- medewerkers
- studenten
title: 'C&CZ heidag: dinsdag 23 januari'
---
C&CZ heeft een heidag gepland voor dinsdag 23 januari. Voor
bereikbaarheid in geval van ernstige storingen wordt gezorgd.
