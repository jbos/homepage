---
author: polman
date: 2014-07-02 14:25:00
tags:
- medewerkers
- studenten
title: Limiet op aantal groepen drastisch verhoogd
---
De limiet op het aantal groepen waarmee men bestanden op
[netwerkschijven](/nl/howto/diskruimte/) kan delen, is verhoogd van 16
naar 100. Dit historische probleem in het ontwerp van
[NFS](http://nl.wikipedia.org/wiki/Network_File_System) is [opgelost in
moderne Linux
versies](http://www.xkyle.com/solving-the-nfs-16-group-limit-problem).
