---
author: bram
date: 2012-02-28 16:53:00
tags:
- studenten
title: Welke TK heeft vrije PC's?
---
Sinds kort is op [welke.tk](http://welke.tk) te zien welke terminalkamer
vrije pc’s heeft. Voordelen boven de al langer bestaande [actuele
bezettingspagina](http://www.ru.nl/studenten/voorzieningen/pc-voorzieningen_0/faculteiten/actuele_bezetting/)
zijn dat het realtime is en dat men in één oogopslag kan zien of een
ruimte voor onderwijs gereserveerd is of zal worden. De website
[welke.tk](http://welke.tk) is overigens ook prima op een klein scherm
van een smartphone te raadplegen. De website is gemaakt door Bas
Westerbaan met medewerking van C&CZ, het ontwerp is van Judith van
Stegeren, Ruben Nijveld heeft geholpen met de mobiele ondersteuning. De
bron van de roosters is [Ruuster.nl](http://beta.ruuster.nl).
