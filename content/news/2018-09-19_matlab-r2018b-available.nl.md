---
author: petervc
date: 2018-09-19 17:49:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2018b beschikbaar
---
De nieuwste versie van [Matlab](/nl/howto/matlab/), R2018b, is
beschikbaar voor afdelingen die licenties voor het gebruik van Matlab
hebben. De software en de licentiecodes zijn via een mail naar
postmaster te krijgen voor wie daar recht op heeft. De software staat
overigens ook op de [install](/nl/tags/software)-schijf. Op alle door
C&CZ beheerde Linux machines is R2018b binnenkort beschikbaar, een
oudere versie (/opt/matlab-R2018a/bin/matlab) is nog tijdelijk te
gebruiken. Op de door C&CZ beheerde Windows-machines zal Matlab tijdens
het semester niet van versie veranderen om versieafhankelijkheden bij
lopende colleges te voorkomen.
