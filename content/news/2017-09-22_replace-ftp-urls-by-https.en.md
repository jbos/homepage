---
author: bram
date: 2017-09-22 16:46:00
tags:
- studenten
- medewerkers
- docenten
title: Replace FTP URL's by HTTPS
---
Because [Chrome will label all FTP URL’s as “not
secure”](https://groups.google.com/a/chromium.org/forum/m/#!msg/security-dev/HknIAQwMoWo/xYyezYV5AAAJ)
starting December 2017, it is wise to change those URL’s, if you offer
files through <ftp://ftp.science.ru.nl/pub>. The same files can be found
with the [more secure URL
\<https://ftp.science.ru.nl/\>](https://ftp.science.ru.nl/).
