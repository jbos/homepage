---
author: wim
date: 2011-08-29 17:59:00
tags:
- studenten
- medewerkers
- docenten
title: 'PC''s onderwijszalen: meer, nieuw en Ubuntu'
---
Op de pagina over de [Terminalkamers](/nl/howto/terminalkamers/) is te
lezen hoeveel en welke PC’s beschikbaar zijn in de onderwijszalen. De
belangrijkste veranderingen zijn:

-   Alle PC’s zijn in de zomervakantie opnieuw geïnstalleerd.
-   Alle PC’s zijn nu dual-boot Ubuntu Linux en Windows XP. Hiermee is
    Fedora Linux in deze zalen uitgefaseerd.
-   In TK075 zijn in de zomervakantie 66 nieuwe PC’s geïnstalleerd.
-   De nieuwe zaal TK137 is gevuld met 49 PC’s uit andere zalen.
