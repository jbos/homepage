---
author: john
date: 2016-09-01 15:47:00
tags:
- studenten
- medewerkers
title: New laptops Library of Science
---
23 laptops of the [Library of Science laptop
pool](/en/howto/laptop-pool/) have been replaced by a new and faster
model: HP EliteBook 850 G3. In particular, the SSD (solid state disk)
and the full HD screen are a significant improvement over the old model.
The other (46) old laptops will be in use for the time being.
