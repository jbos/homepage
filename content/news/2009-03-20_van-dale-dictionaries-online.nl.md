---
author: petervc
date: 2009-03-20 12:21:00
title: Van Dale woordenboeken online
---
De UB heeft een licentie voor de Van Dale woordenboeken
[online](http://www.ru.nl/ubn/op_bezoek/sociaal-culturele/nieuws/van_dale/).
Deze zijn ook thuis te gebruiken.
