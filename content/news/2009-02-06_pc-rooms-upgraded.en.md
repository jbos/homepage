---
author: theon
date: 2009-02-06 11:06:00
title: PC rooms upgraded
---
The last weeks almost all older [PC rooms](/en/howto/terminalkamers/)
have had an upgrade. These PCs now have at least 1 GB of memory and a
[better MS Windows (WDS)
installation](/en/howto/windows-beheerde-werkplek/). Exceptions are the
too old PCs in TK702 (TK-GIS, HG02.702) and the “self study rooms”
(HG00.2xx).
