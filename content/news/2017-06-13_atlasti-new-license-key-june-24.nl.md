---
author: petervc
date: 2017-06-13 12:40:00
tags:
- studenten
- medewerkers
title: ATLAS.ti nieuwe licentiesleutel op 24 juni
---
Licentiebeheer van het [ISC](http://www.ru.nl/isc) heeft meegedeeld dat
de nieuwe licentiesleutel van ATLAS.ti pas op 24 juni 2017 [beschikbaar
zal
zijn](http://www.radboudnet.nl/ictservicecentrum/codes/licentiecode-atlas/).
