---
author: polman
date: 2018-07-17 13:57:00
tags:
- studenten
- medewerkers
- docenten
title: New version of Mailman maillist software
---
The [Mailman maillist software](https://mailman.science.ru.nl/) has had
een upgrade to a new version and has been moved to a new server. Because
of subscription spam, we had to temporarily disable subscription. The
new version uses [reCAPTCHA](https://www.google.com/recaptcha/) as
countermeasure for subscription spam. Therefore we have enabled
web-subscription again.
