---
author: theon
date: 2009-06-17 14:26:00
title: Free RSI prevention software
---
The free RSI prevention software
[Workrave](http://www.workrave.org/welcome/) is available on all C&CZ
managed Windows pc’s. If Workrave doesn’t start automatically, one can
start it with `Start -> Programs -> Workrave`. When one doesn’t want
Workrave to start automatically, one can go to
`Start->Programs->Startup` and delete `Workrave` there.
