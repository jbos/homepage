---
author: john
date: 2014-01-09 16:34:00
tags:
- medewerkers
- docenten
title: Laptops zonder wifi voor gebruik bij tentamens zonder Internet-toegang
---
Het is nu mogelijk om de laptops uit de [FNWI laptop
pool](/nl/howto/laptop-pool/) te lenen waarin [draadloze
netwerk](http://nl.wikipedia.org/wiki/Wifi) is uitgeschakeld. Zulke
laptops kunnen gebruikt worden voor tentamens om studenten de
mogelijkheid te geven een computer te gebruiken zonder gemakkelijke
Internet-toegang. Op twee laptops in de pool is Wifi permanent
uitgeschakeld op verzoek van het
[Onderwijscentrum](http://www.ru.nl/fnwi/onderwijs/onderwijscentrum/),
dit t.b.v. gebruik bij tentamens door functiebeperkte studenten. Voor
grootschaliger gebruik dient men op tijd laptops te
[reserveren](/nl/howto/laptop-pool/), zonodig met vermelding dat het gaat
om laptops met uitgeschakeld draadloos netwerk.
