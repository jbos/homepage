---
author: polman
date: 2007-11-30 14:37:00
title: Notepad++ installed on all managed Windows computers
---
Notepad++ is a free source code editor and Notepad replacement, which
supports several programming languages, running under the MS Windows
environment. For detailed information see
[Notepad++](http://notepad-plus.sourceforge.net/uk/site.htm)
