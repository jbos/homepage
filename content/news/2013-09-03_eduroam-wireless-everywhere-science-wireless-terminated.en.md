---
author: mkup
date: 2013-09-03 14:31:00
tags:
- studenten
- medewerkers
- docenten
title: Eduroam wireless everywhere, Science wireless terminated
---
In all buildings of the Faculty of Science the [wireless network
Eduroam](/en/howto/netwerk-draadloos/) is now available. See [the
website](http://www.ru.nl/draadloos) for all information. This marks the
end of the Science wireless network. The ru-wlan network will also be
terminated in the near future, so please switch to Eduroam as soon as
possible.
