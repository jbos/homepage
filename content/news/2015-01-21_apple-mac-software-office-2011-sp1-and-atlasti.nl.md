---
author: petervc
date: 2015-01-21 13:12:00
tags:
- studenten
- medewerkers
title: 'Apple Mac-software: Office 2011 SP1 en ATLAS.ti'
---
Voor Apple Macs is een nieuwe versie (SP1) van de Microsoft Office 2011
software en een eerste versie van de ATLAS.ti kwalitatieve
data-analysesoftware beschikbaar op de
[Install](/nl/howto/install-share/)-netwerkschijf. Volgens de
site-license mag deze software op alle computers in eigendom van de RU
worden geïnstalleerd zonder bijkomende kosten. Licentiecodes zijn bij
C&CZ helpdesk of postmaster te verkrijgen. Voor thuisgebruik van deze
software kunnen medewerkers en studenten terecht op
[Surfspot](http://www.surfspot.nl).
