---
author: wim
date: 2014-11-14 12:51:00
tags:
- studenten
- medewerkers
- docenten
title: Disks xpsoftware and xpcursus renaming to software and cursus
---
The [network shares](/en/howto/netwerkschijf/) `xpsoftware` and
`xpcursus`, that many PC’s use, have been renamed to `software` and
`cursus`. The old names still work until Monday, January 5th 2015. These
disks are also mounted as software share [S-drive](/en/howto/s-schijf/)
and course software share [T-drive](/en/howto/t-schijf/) on PC’s managed
by C&CZ. Lecturers can manage their course software on the
[T-drive](/en/howto/t-schijf/) themselves.
