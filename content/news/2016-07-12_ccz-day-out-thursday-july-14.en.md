---
author: petervc
date: 2016-07-12 17:11:00
tags:
- medewerkers
- studenten
title: 'C&CZ day out: Thursday July 14'
---
The C&CZ yearly day out is scheduled for Thursday July 14. C&CZ can be
reached in case of serious disruptions of services.
