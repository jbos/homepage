---
author: wim
date: 2010-08-09 09:52:00
tags:
- studenten
title: Microsoft Office 2007 geinstalleerd in onderwijsruimtes
---
Op de [PC’s in de onderwijsruimtes](/nl/howto/terminalkamers/) is
Microsoft Office 2007 geïnstalleerd ter vervanging van Microsoft Office
2003. Er is hulp beschikbaar voor de overstap naar Office2007:

-   Een [Learning
    Guide](http://sito.science.ru.nl/LearningGuide/Publications/start/default.htm)
    voor alle onderdelen van Office2007 (Word, Excel, PowerPoint,
    Outlook, Access) en ook voor Windows7, die vanaf de campus te
    raadplegen is.
-   De [informatiesite van het project Uitrol Office
    2007](http://www.ru.nl/gdi/projecten/office_2007/) van de
    [GebruikersDienst ICT](http://www.ru.nl/gdi).
