---
author: petervc
date: 2016-08-23 15:09:00
tags:
- studenten
- medewerkers
title: Caspar Terheggen vertrekt als hoofd C&CZ
---
Caspar Terheggen stopt per 1 september als afdelingshoofd van C&CZ. Hij
gaat vanaf dan de nieuwe functie van enterprise- en informatiearchitect
van de Radboud Universiteit invullen. Tot er een nieuw
C&CZ-afdelingshoofd is, zal Peter van Campen waarnemend afdelingshoofd
zijn.
