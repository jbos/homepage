---
author: petervc
date: 2013-07-22 16:23:00
tags:
- studenten
- medewerkers
- docenten
title: Xafax copier units removed
---
As [announced before](/en/news/), the Xafax copy units will be
removed, starting July 23, 2013. Shortly therafter, the Ricoh MFP’s will
be removed. The new [Konica Minolta MFP’s](/en/howto/km/) have been
available the last few weeks. FNWI-emplyees can get PIN codes for
copying [from C&CZ](/en/howto/contact/). Therefore we expect no problems
from the removal of the Xafax units. Students can make copies by using
the [Peage
MFP](http://www.ru.nl/ictservicecentrum/studenten/peage-%28-english%29/)
near the restaurant, or make copies by scanning and printing afterwards.
