---
author: petervc
date: 2021-03-30 18:01:00
tags:
- studenten
- medewerkers
- docenten
title: 'Nieuwe versie Maple: Maple2021'
---
De nieuwste versie van [Maple](/nl/howto/maple/), Maple2021, is te vinden
op de [Install](/nl/howto/install-share/)-netwerkschijf en is op door
C&CZ beheerde Linux-computers geïnstalleerd. Licentiecodes zijn bij C&CZ
helpdesk of postmaster te verkrijgen voor de deelnemende afdelingen.
