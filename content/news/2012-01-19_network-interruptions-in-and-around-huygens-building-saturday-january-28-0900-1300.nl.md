---
author: mkup
date: 2012-01-19 15:12:00
tags:
- studenten
- medewerkers
title: Huygens e.o. netwerkonderbrekingen zaterdag 28 januari 09:00-13:00
---
De router voor het Huygensgebouw en aangrenzende gebouwen zal enkele
onderbrekingen ondervinden, omdat het [UCI](http://www.ru.nl/uci) nieuwe
aansluitingen en nieuwe software gaat installeren. Allerlei
netwerkdiensten waarbij clients en of servers via die router aangesloten
zijn, kunnen gedurende die tijd storingen ondervinden.
