---
author: mkup
date: 2014-05-08 12:12:00
tags:
- medewerkers
- studenten
title: Onderhoud wireless infrastructuur op 19 mei a.s.
---
Op maandagavond 19 mei a.s. vindt er door het
[ISC](http://www.ru.nl/isc) een major upgrade plaats van de wireless
infrastructuur en het bijbehorende beheersysteem (Prime geheten). Vanaf
20:00 uur zal daardoor de wireless-service (lees: Eduroam) enkele malen
onderbroken worden. Bestaande wireless verbindingen worden verbroken en
nieuwe verbindingen opzetten is dan tijdelijk niet mogelijk. Vanwege de
complexe voorbereidingen is er een kans dat deze werkzaamheden niet of
niet volledig op 19 mei kunnen worden afgerond. In dat geval zullen de
werkzaamheden worden voortgezet op maandag 26 mei a.s., eveneens met
onderbrekingen vanaf 20 uur.
