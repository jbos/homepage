---
author: wim
date: 2009-02-02 21:09:00
title: PC-zaalbezetting weer beschikbaar
---
Het gebruik van de [PC-zalen](/nl/howto/terminalkamers/) wordt weer
[gemonitord](http://graphs.science.ru.nl/graph_view.php?action=tree&tree_id=7).
Hierbij wordt gebruik gemaakt van [Cacti](http://www.cacti.net/) voor
het weergeven van de met SNMP verkregen data.
