---
author: petervc
date: 2007-11-09 18:10:00
title: Installed StarOffice 8 for Solaris
---
The newest version of the office software
[StarOffice](http://en.wikipedia.org/wiki/StarOffice) (8) has been
installed for Solaris. Type `soffice` to use it.
