---
author: petervc
date: 2018-12-07 11:29:00
tags:
- medewerkers
- docenten
title: Nieuwe prijzen MS Visio en Project
---
Vanaf [januari
2016](https://wiki.cncz.science.ru.nl/Nieuws_archief#.5BMS_Visio_en_MS_Project_verwijderd_van_PC.27s.5D.5BMS_Visio_and_MS_Project_removed_from_pcs.5D)
moet voor het gebruik van Microsoft Visio Pro en Microsoft Project per
pc per jaar betaald worden. De meeste andere Microsoft producten
waarvoor de RU licenties heeft, mogen zonder bijbetaling op
RU-apparatuur gebruikt worden. Per 2019 worden de prijzen van Visio en
Project erg aangepast, zie onder. Neem s.v.p. [contact op met
C&CZ](/nl/howto/contact/) als u Visio of Project gebruikt of wil gaan
gebruiken.

  ---------------------------------------------------------------------------------
  *Product*                                                       *2018*   *2019*
  --------------------------------------------------------------- -------- --------
  Microsoft Visio Pro (Medewerkers,                             € 3,55   € 29,45
  groepslicentie)                            

  Microsoft Project Professional (with 1 Project Server CAL)      € 3,97   € 34,08
  (Medewerkers, groepslicentie)            

  Microsoft Project Server, Server licentie                       € 396,98 € 0,00
  (Groepslicentie)                                    
  ---------------------------------------------------------------------------------
