---
author: petervc
date: 2022-07-15 10:43:00
tags:
- studenten
- medewerkers
- docenten
title: CCZ verhuist terug naar HG03.055
---
Omdat er te veel herrie was in de nieuwe ruimte, zal C&CZ tijdelijk
terug naar de derde verdieping van het Huygensgebouw verhuizen. De
verwachting is dat eind 2022 C&CZ definitief weer naar de begane grond
zal verhuizen.
