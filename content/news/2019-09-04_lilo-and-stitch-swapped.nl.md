---
author: sioo
date: 2019-09-04 16:33:00
tags:
- medewerkers
- studenten
title: Lilo en Stitch omgewisseld
---
Zoals eerder aangekondigd zijn de linux login (ssh) servers
(lilo5.science.ru.nl en lilo6.science.ru.nl) omgewisseld van bijnaam,
waardoor men standaard Ubuntu 18.04 krijgt in plaats van Ubuntu 16.04.
De naam lilo.science.ru.nl verwijst nu naar lilo6.science.ru.nl en
stitch.science.ru.nl verwijst naar lilo5.science.ru.nl. Mocht je
meldingen krijgen over veranderde fingerprints, weet je waar het door
komt. Zie ook [de pagina over servers](/nl/howto/hardware-servers/).
