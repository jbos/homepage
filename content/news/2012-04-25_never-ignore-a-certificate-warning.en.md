---
author: petervc
date: 2012-04-25 13:47:00
tags:
- studenten
- medewerkers
- docenten
title: Never ignore a certificate warning\!
---
If a program complains about an incorrect certificate of e.g. a website,
then never ignore this, but [report this to C&CZ
a.s.a.p.](/en/howto/contact/). The reason to give this warning is a
recent complaint of a browser, that pointed to a contaminated laptop,
that launched an [ARP spoofing
attack](http://en.wikipedia.org/wiki/ARP_spoofing). The idea behind such
an attack is to listen to network traffic in order to e.g. obtain
passwords.
