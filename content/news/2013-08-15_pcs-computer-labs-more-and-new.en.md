---
author: john
date: 2013-08-15 16:48:00
tags:
- studenten
- medewerkers
- docenten
title: 'PC''s computer labs: more and new'
---
The page about the [computer labs](/en/howto/terminalkamers/) lists all
details about the PC’s in the computer labs. The most important changes
are:

-   The old PC’s in TK029, TK206, Study area, Library of Science,
    Project rooms and Flex workplaces have been replaced by new [HP
    Compaq Elite
    8300](http://www.pcmag.com/article2/0,2817,2406799,00.asp) PC’s,
    which are [All-in-one
    PC’s](http://en.wikipedia.org/wiki/All-in-One_PC#All-in-one).
-   In the study area extra PC’s have been installed.
-   A new lab TK702 has been created with 17 PC’s.
