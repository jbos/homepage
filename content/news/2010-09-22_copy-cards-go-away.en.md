---
author: petervc
date: 2010-09-22 11:06:00
title: Copy cards go away
---
The copy cards with a magnet strip, that can be used with the
[multifunctional copiers](/en/howto/copiers/), will go away, starting in
a few months. To print and copy in the future
[Peage-systeem](http://www.ru.nl/studenten/voorzieningen/peage-%28proef%29/),
one needs a chip card and a network budget. In the near future the first
multifunctional in this new system will be placed in the central hall of
the Huygens building. The first few months this system can only be used
by students, so the other Science-multifunctionals will be switched to
this new system later. Departments are advised to keep less copy cards
in stock.
