---
title: Manage science accounts via DIY
author: bram
date: 2022-11-15
tags: [ contactpersonen ]
cover:
  image: img/2022/dhz.png
---
From now on it is possible to manage [Science accounts](../../howto/login) under your control in [DIY](https://diy.science.ru.nl). The overview of Science accounts for which you are the contact can be found in the menu "My accounts".

![my accounts screenshot](/img/2022/my-accounts.png)

Each of these accounts has a link to a form where you can perform one of the following actions:

- Postpone the check date. This date is used to send reminder emails.
- Block the account. Blocked accounts cannot be used to login into systems (like mail, login server, gitlab).
- Delete the account. If you delete an account, the account will be blocked immediately and will be deleted a month later.

If the check date of a Science account has passed, a [reminder email](../../howto/emailcodes/#diy-1) will be sent inviting you to indicate in DIY what to do with the account. In most cases it is therefore no longer necessary to send a reply to this e-mail.
