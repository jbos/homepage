---
author: sioo
date: 2017-02-16 15:11:00
tags:
- medewerkers
- studenten
title: 'Eduweb: etalage van vrije en open source onderwijssoftware'
---
In samenwerking met [StITPro](http://stitpro.nl/) hebben we
[Eduweb](http://eduweb.science.ru.nl) gemaakt, een overzichtspagina van
vrije en open source software, ontwikkeld ter ondersteuning van het
onderwijs. Een deel hiervan is in gebruik en/of ontwikkeld binnen FNWI,
andere bij andere onderwijsinstellingen.
