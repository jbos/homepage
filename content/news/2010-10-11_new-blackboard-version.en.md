---
author: pberens
date: 2010-10-11 13:57:00
tags:
- docenten
title: New Blackboard version
---
There is a [new version of
Blackboard](/en/howto/blackboard#.5bblackboard-9.1:-nieuwe-versie.5d.5bblackboard-9.1:-new-version.5d/).
Furthermore the [Blackboard](/en/howto/blackboard/) page in the C&CZ wiki
was renewed and there is a new Blackboard maintainer for the Faculty.
