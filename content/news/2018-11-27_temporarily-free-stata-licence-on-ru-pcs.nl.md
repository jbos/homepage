---
author: petervc
date: 2018-11-27 14:26:00
tags:
- medewerkers
- studenten
- docenten
title: Tijdelijk gratis Stata-licentie op RU-werkplekken
---
::: {#.5BTijdelijk_gratis_Stata-licentie_op_RU-werkplekken.5D.5BTemprarily_free_Stata_licence_on_RU_pc.27s.5D}
:::

De Radboud Universiteit heeft samen met Radboudumc een campuslicentie
voor [Stata](https://www.stata.com/) afgesloten. Stata is statistische
software die nu op een aantal faculteiten – met name bij FSW en FM –in
gebruik is. De kosten voor het gebruik van Stata waren tot nog toe
relatief hoog. Doordat er nu een campuslicentie is, is het voor nieuwe
gebruikers een stuk voordeliger geworden: tot november 2019 kan het
gratis gebruikt worden op pc’s die eigendom van de RU zijn. Daarna zal
het, afhankelijk van het aantal gebruikers, maximaal € 150,- per
gebruiker per jaar gaan kosten.
