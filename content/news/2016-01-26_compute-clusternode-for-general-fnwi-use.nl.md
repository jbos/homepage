---
author: petervc
date: 2016-01-26 16:29:00
tags:
- medewerkers
title: Rekenclusternode voor algemeen gebruik FNWI
---
Om afdelingen van FNWI te helpen, die geen eigen clusternode hebben,
maar incidenteel wel rekenbehoeften, heeft C&CZ een
[cn-clusternode](/nl/howto/hardware-servers/) aangeschaft. De
oorspronkelijke reden voor C&CZ om deze rekenserver aan te schaffen was
eigenlijk om afdelingen te helpen afscheid te nemen van oude
clusternodes die veel minder energie-efficiënt zijn (performance per
Watt). De C&CZ clusternode is een Dell PowerEdge R730 met 2 processoren
(10-core Xeon E5-2660V3 op 2.6 GHz) met 256 GB geheugen.
