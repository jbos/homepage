---
author: polman
date: 2017-07-31 17:36:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2017
---
The most recent version of the [TeX
Collection](http://www.tug.org/texcollection), 2017, is available for
MS-Windows, Mac OS X and Linux. It can be found on the
[Install](/en/howto/install-share/) network share.
