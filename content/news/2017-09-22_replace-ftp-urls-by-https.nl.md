---
author: bram
date: 2017-09-22 16:46:00
tags:
- studenten
- medewerkers
- docenten
title: Vervang FTP URL's door HTTPS
---
Omdat in December 2017 [Chrome alle FTP URL’s als “niet
veilig”](https://groups.google.com/a/chromium.org/forum/m/#!msg/security-dev/HknIAQwMoWo/xYyezYV5AAAJ)
gaat labelen, is het verstandig om die URL’s aan te passen, indien u
bestanden aanbiedt via <ftp://ftp.science.ru.nl/pub>. Dezelfde bestanden
zijn te vinden onder de [veiligere URL
\<https://ftp.science.ru.nl/\>](https://ftp.science.ru.nl/).
