---
author: petervc
date: 2015-08-27 18:12:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2015
---
De nieuwste versie van de [TeX
Collection](http://www.tug.org/texcollection), juni 2015, geschikt voor
MS-Windows, Mac OS X en Linux, is beschikbaar. De DVD is te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf, maar is ook [te
leen](/nl/howto/microsoft-windows/).
