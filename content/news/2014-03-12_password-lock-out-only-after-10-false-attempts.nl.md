---
author: petervc
date: 2014-03-12 12:55:00
tags:
- studenten
- medewerkers
- docenten
title: Wachtwoordblokkade pas na 10 foute pogingen
---
Tot vandaag werd een Science-account na 3 foute inlogpogingen 15 minuten
geblokkeerd. Dit is nu aangepast: pas na 10 foute pogingen wordt het
account 30 minuten geblokkeerd. Dit maakt de instellingen identiek aan
die voor het RU-wachtwoord.

Merk op dat zowel het Science-wachtwoord als het RU-wachtwoord niet
verplicht regelmatig gewijzigd hoeven te worden. Wij adviseren om dit
wel te doen, het af en toe wijzigen van wachtwoorden draagt bij aan de
beveiliging. Maar denk hierbij aan alle apparaten waar dit wachtwoord op
opgeslagen is (laptop, smartphone, thuis-PC, enz.).
