---
author: sioo
date: 2019-02-26 10:43:00
tags:
- medewerkers
- docenten
- studenten
title: 'eLabFTW: open source electronisch lab notebook en database'
---
Sinds kort is voor afdelingen van FNWI
[eLabFTW](https://elabftw.science.ru.nl) beschikbaar gemaakt. Dit is een
gratis, veilig, modern en compliant open source electronisch lab
notebook en database, waarmee verschillende teams (onderzoeksgroepen)
hun experimenten efficiënt kunnen volgen en waarmee zij ook met een
krachtige en flexibele database hun laboratorium kunnen beheren.
Medewerkers van afdelingen die al gebruik maken van eLabFTW (in en om
Moleculaire Biologie) kunnen zich zelf aanmelden op
[eLabFTW](https://elabftw.science.ru.nl), voor nieuw toetredende
afdelingen moet eerst in overleg met [Postmaster](/nl/howto/contact/) een
nieuw team gemaakt worden. Zie
[www.eLabFTW.net](https://www.elabftw.net/) voor meer info.
