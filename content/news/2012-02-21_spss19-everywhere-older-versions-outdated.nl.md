---
author: wim
date: 2012-02-21 11:29:00
tags:
- studenten
- medewerkers
- docenten
title: SPSS19 overal, oudere versies vervallen
---
Op alle door C&CZ beheerde Windows en Linux computers is
[SPSS19](http://www-01.ibm.com/software/analytics/spss/products/statistics/)
geïnstalleerd. Het gebruiksrecht voor oudere versies vervalt per juli
2012. De software, ook voor Mac, is ook te vinden op de
[Install](/nl/howto/install-share/) netwerkschijf om zelf te installeren
door medewerkers en studenten van FNWI.
