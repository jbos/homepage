---
author: john
date: 2013-12-05 12:39:00
tags:
- medewerkers
- docenten
title: Filemaker Pro Advanced on Install network share
---
A new version of [Filemaker Pro
Advanced](http://www.filemaker.com/products/filemaker-pro/), 12, is
available on the [Install](/en/howto/install-share/) network share. The
license permits the use on university computers. License codes can be
obtained from C&CZ helpdesk or postmaster. For home PCs the software is
available through [Surfspot](http://www.surfspot.nl).
