---
author: wim
date: 2017-10-04 10:10:00
tags:
- studenten
- medewerkers
- docenten
title: Printer Escher will be removed
---
The last remaining general C&CZ managed printer, the black/white printer
Escher in the Library of Science will be shutdown and removed as of
November 1. This marks the end of the C&CZ budgetted print system. C&CZ
printbudgets are then only used for the not directly accessible [poster
and 3D printers](/en/howto/printers-en-printen/). Printing on general
printers has been moved to [Peage](/en/howto/peage/) more than a year
ago.

Remaining C&CZ printbudget can be used for [poster or 3D
printing](/en/howto/printers-en-printen/) or reclaimed at C&CZ
(HG03.055).
