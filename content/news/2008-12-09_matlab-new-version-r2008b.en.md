---
author: polman
date: 2008-12-09 08:29:00
title: Matlab new version (R2008b)
---
Starting December 2008 a new version (R2008b) of
[Matlab](/en/howto/matlab/) is available. The license server has already
been upgraded. Very soon it will be available on all C&CZ managed PCs
with Linux or Windows. For more information about this release, see [the
Mathworks
website](http://www.mathworks.com/products/new_products/latest_features.html)
