---
author: petervc
date: 2019-04-12 14:38:00
tags:
- medewerkers
- docenten
- studenten
title: Oude MFP's worden 25 april verwijderd
---
De [zes jaar oude Konica Minolta
MFP’s](https://www.ru.nl/facilitairbedrijf/printen/printen-campus/)
worden vanaf 25 april bij FNWI verwijderd. Gebruikers hebben enkele
weken de tijd gehad om over te stappen naar het nieuwe systeem, dat veel
sneller en gebruiksvriendelijker is. Voor studenten: probeer voor 25
april je oude tegoed op te maken en waardeer niet meer op. Krijg je je
tegoed niet op? Dan kun je dit tot 1 juni 2019 bij de Library of Science
terug laten storten. Voor alle gebruikers: ook
[EveryOnePrint](https://eop.ru.nl) verdwijnt, die functionaliteit zit in
de [SafeQ web-upload](https://peage.ru.nl/).
