---
author: petervc
date: 2017-05-24 19:32:00
tags:
- medewerkers
- studenten
title: Eduroam within RU firewall again
---
The [ISC](http://www.ru.nl/isc) moved the [wireless Eduroam
network](http://www.ru.nl/ict-uk/staff/wifi/) back within the RU
firewall on the evening of May 22. In December 2015 it had been moved
outside of the RU firewall because of firewall performance issues. The
new firewall that is in place since December 2016 can handle much more
traffic, therefore this move could be planned.
