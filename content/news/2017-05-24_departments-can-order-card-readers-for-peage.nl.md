---
author: petervc
date: 2017-05-24 19:50:00
tags:
- medewerkers
title: Afdelingen kunnen Peage-paslezers bestellen
---
Het [Facilitair
Bedrijf](http://www.ru.nl/facilitairbedrijf/printen/printen-campus/)
heeft laten weten dat als een afdeling het gebruik van een Konica
Minolta multifunctional sneller wil maken, men voor € 300,- een
campuskaartlezer kan laten plaatsen. Hierna hoeft een medewerker alleen
maar de pincode (4 cijfers) in te toetsen en daarna de campuskaart tegen
de multifunctional te houden om in te loggen, net zoals studenten doen
op de begane grond van het Huygensgebouw. Omdat een nieuwe Europese
aanbesteding naar verwachting januari 2018 gereed zal zijn, zal dit
alleen voor veelgebruikte multifunctionals interessant zijn.
