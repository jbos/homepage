---
author: petervc
date: 2011-09-16 13:11:00
tags:
- studenten
- medewerkers
- docenten
title: Kun.nl mail-adressen verwijderd
---
Iedereen die nog een `@*.kun.nl` mail-adres had, meestal als
doorstuur-adres naar een `@*.ru.nl` mail-adres, heeft een kleine twee
maanden geleden een mail gekregen dat per vandaag alle `@*.kun.nl`
mail-adressen opgeheven zouden worden. Dat is vandaag ook gebeurd. Een
enkele tijdelijke uitzondering is gemaakt voor afdelingen die een
probleem hebben om een extern geregistreerd mail-adres gewijzigd te
krijgen. Het verwijderen van alle `@*.kun.nl` adressen is gecoördineerd
tussen de beheerders van de diverse mailsystemen van de RU.
