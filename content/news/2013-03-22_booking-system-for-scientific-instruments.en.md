---
author: fmelssen
date: 2013-03-22 14:15:00
tags:
- medewerkers
- docenten
title: Booking system for scientific instruments
---
In a collaboration between the Science faculty and the UMCN, C&CZ has
built a web application for the booking of scientific instruments of the
[Microscopic Imaging
Centre](http://www.umcn.nl/Research/Departments/cellbiology/Pages/Microscopic%20Image%20Center.aspx)
(MIC), the [Gemeenschappelijk
Instrumentarium](http://www.ru.nl/fnwi/gi/) (GI) and [Organic
Chemistry](http://www.ru.nl/imm/research-facilities/research-groups/).
The web application can be found at <https://bookings.science.ru.nl>.
Because the application was written as a generic solution, it will soon
also be used for the lending system of [laptops](/en/howto/laptop-pool/)
by the Library of Science. If you have a booking/reservation question
which may be solved using this application, please [contact
C&CZ](/en/howto/contact/).
