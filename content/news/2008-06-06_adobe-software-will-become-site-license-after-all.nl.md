---
author: petervc
date: 2008-06-06 15:32:00
title: Adobe software wordt toch site-licentie\!
---
Vanmiddag meldde [SURFdiensten](http://www.surfdiensten.nl) dat de
site-license overeenkomst tussen SURFdiensten en
[Adobe](http://www.adobe.com) voor het Nederlandse onderwijs een
definitieve status had gekregen. De producten en codes voor de
licentieproducten zullen volgens planning in de loop van volgende week
beschikbaar komen.
