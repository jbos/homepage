---
author: wim
date: 2017-11-23 10:42:00
tags:
- medewerkers
- studenten
title: Linux-printen naar Peage nu eenvoudig
---
Voorlopig als test is er nu een veel simpeler alternatief voor het
[SMB-printen vanaf Linux](/nl/howto/peage/). Op de door C&CZ beheerde
Linux-werkplekken en -servers bestaat nu de printer “Peage”. De
C&CZ-printserver vertaalt de Science-gebruikersnaam naar het U- of
S-nummer en stuurt de printjob door naar een RU payprint-server. Ook
command-line printen met het commando `lpr` is hierdoor mogelijk
(`lpoptions -pPeage -o SelectColor=Color` om in kleur af te drukken).
