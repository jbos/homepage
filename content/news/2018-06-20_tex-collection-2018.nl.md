---
author: polman
date: 2018-06-20 16:15:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2018
---
De nieuwste versie van de [TeX
Collection](http://www.tug.org/texcollection), 2018, voor MS-Windows,
Mac OS X en Linux, is beschikbaar. De DVD is te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf.
