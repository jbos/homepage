---
author: wim
date: 2008-03-03 14:01:00
title: PC's in terminalkamers tonen locktijden
---
PC’s in terminalkamers zijn voorzien van een timer die aangeeft hoe lang
de PC gelockt is. Aan de hand van de locktijd kan men besluiten om de PC
te herstarten. De software is gemaakt door een van onze studenten: J.
Groenewegen.
