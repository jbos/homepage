---
author: mkup
date: 2014-03-27 14:15:00
tags:
- medewerkers
- studenten
title: Eduroam adjustments March 27
---
The [ISC](http://www.ru.nl/isc) let us know that Thursday evening March
27, they will change settings of the controllers of the [Eduroam
wireless network](/en/howto/netwerk-draadloos/), in order to resolve
problems. Please [report to netmaster\@science.ru.nl](/en/howto/contact/)
if you notice improvements due to these changes or if they seem to cause
problems.

This is separate of the [earlier announced](/en/news/) adding and
moving of wireless access points in order to improve the wireless
coverage in the Huygens building before the end of April.

C&CZ invites everyone to report problems with the wireless and wired
network to netmaster\@science.ru.nl.
