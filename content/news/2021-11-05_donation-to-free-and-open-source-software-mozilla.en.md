---
author: petervc
date: 2021-11-05 16:38:00
tags:
- studenten
- medewerkers
- docenten
title: 'Donation to free and open source software: Mozilla'
---
For the majority of the services C&CZ uses [free
software](https://en.wikipedia.org/wiki/Free_software) and [open source
software](https://en.wikipedia.org/wiki/Open-source_software).
Therefore, some time ago the idea emerged that the C&CZ employees would
vote each year which projects would receive a donation from C&CZ. This
year the [Mozilla Foundation](https://foundation.mozilla.org/) was
chosen. Many people will know [the Mozilla
Firefox](https://en.wikipedia.org/wiki/Firefox) webbrowser and the
mailclient [Mozilla
Thunderbird](https://en.wikipedia.org/wiki/Mozilla_Thunderbird) and
maybe use it hours a day. But [the Mozilla Foundation does a lot
more](https://foundation.mozilla.org/en/what-we-do/) to try to ensure
the internet remains a public resource that is open and accessible to us
all.
