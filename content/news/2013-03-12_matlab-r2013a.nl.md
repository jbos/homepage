---
author: petervc
date: 2013-03-12 15:30:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2013a
---
De nieuwste versie van [Matlab](/nl/howto/matlab/), R2013a, is
beschikbaar voor afdelingen die licenties voor het gebruik van Matlab
aangeschaft hebben. De software en de licentiecodes zijn via een mail
naar postmaster te krijgen voor wie daar recht op heeft. De software
staat overigens ook op de [install](/nl/tags/software)-schijf. Op de
door C&CZ beheerde 64-bit Linux machines is R2013a binnenkort
beschikbaar, een oudere versie (/opt/matlab-R2012b/bin/matlab) is nog
tijdelijk te gebruiken. Op de door C&CZ beheerde Windows-machines zal
R2013a ook beschikbaar komen.
