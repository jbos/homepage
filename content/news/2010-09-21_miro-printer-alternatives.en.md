---
author: mbouwens
date: 2010-09-21 18:29:00
title: Miro printer alternatives
---
The old [printer
miro](/en/howto/printers-en-printen#.5bpostscript-kleurenprinters.5d.5bpostscript-color-printers.5d?setlang=en/)
has been out of order for a few weeks, but has been repaired now. There
is no longer a service contract for miro, we try to keep it up and
running, if only to finish the remaining toner cartridges. If you really
preferred the output of this Xerox Phaser 7300DX over the [other (Ricoh,
HP) color
printers](/en/howto/printers-en-printen#.5bpostscript-kleurenprinters.5d.5bpostscript-color-printers.5d/)
and also over the printers of the [RU
copyshop](http://www.ru.nl/copyshop/), please report to C&CZ. C&CZ then
can decide about the purchase of a new printer.
