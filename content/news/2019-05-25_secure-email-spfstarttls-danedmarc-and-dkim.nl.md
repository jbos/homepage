---
author: visser
date: 2019-05-25 23:46:00
tags:
- studenten
- medewerkers
- docenten
title: 'Veilige Email: SPF/STARTTLS, DANE/DMARC en DKIM'
---
De afgelopen tijd heeft C&CZ gewerkt aan DNS-gebaseerde technieken die
te gebruiken zijn om spam, phishing en het afluisteren van e-mail tegen
te gaan. Van de
[<https://www.forumstandaardisatie.nl/open-standaarden/lijst/verplicht?f%5B0%5D=field_keywords%3A68>
Pas-Toe-of-Leg-Uit] lijst van verplichte open standaarden m.b.t. email
had C&CZ al een tijd geleden
[SPF](https://nl.wikipedia.org/wiki/Sender_Policy_Framework) ingevoerd
voor de maildomeinen die C&CZ beheert onder ru.nl (science.ru.nl,
cs.ru.nl, astro.ru.nl, math.ru.nl, …). Omdat strikte invoering van SPF
het
[<https://en.wikipedia.org/wiki/Sender_Policy_Framework#FAIL_and_forwarding>
eenvoudig/automatisch doorsturen van mail onmogelijk maakt], is SPF
niet strikt geïmplementeerd. Ook
[STARTTLS](https://nl.wikipedia.org/wiki/STARTTLS) voor het versleutelen
van mailconnecties is al een tijd geleden ingevoerd. Sinds kort is ook
[DANE](https://archief.dnssec.nl/wat-is-dnssec/faq/index.html#dane_wat)
en [DMARC](https://en.wikipedia.org/wiki/DMARC) ingevoerd, wat het
mogelijk maakt om
[DKIM](https://nl.wikipedia.org/wiki/DomainKeys_Identified_Mail) te gaan
implementeren.
