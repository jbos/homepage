---
author: wim
date: 2010-08-05 15:42:00
tags:
- studenten
title: Education PC's usable by all RU-students
---
All RU students can now use the PC’s in the [educational PC rooms of the
Faculty of Science](/en/howto/terminalkamers/). Choose the RU-domain and
then login with your RU-account (S-number and RU-password).

RU employees will be able to login with their RU-account when the
“provisioning” of the RU-accounts (U-number and RU-password) to the RU
Active Directory domain has been realized.

Employees and students of the Faculty of Science can of course login to
the B-FAC domain with their Science account. Other employees or guests
can request a temporary Science account through their departmental
contact.
