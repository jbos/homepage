---
author: petervc
date: 2017-09-22 17:34:00
tags:
- studenten
- medewerkers
- docenten
title: Old serial numbers Adobe Creative Cloud stop working
---
Through ISC license management we received the message from SURFmarket
that the old serial numbers of the Adobe Creative Cloud software will
stop working in mid-November. All software that has been installed with
these serial numbers will stop working. The old serial numbers are
connected to the contract that ended May 31, 2017, the new ones to the
contract 2017-2020.
