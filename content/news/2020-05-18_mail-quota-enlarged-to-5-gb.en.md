---
author: visser
date: 2020-05-18 13:51:00
tags:
- studenten
- medewerkers
- docenten
title: Mail quota enlarged to 5 GB
---
The [quota](/en/howto/quota-bekijken/) for the Science mailboxes has been
enlarged to 5 GB for users that had less quota. Please mail postmaster
if you need more quota. The mailboxes are being [backed
up](/en/howto/backup/) every evening.
