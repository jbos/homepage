---
author: petervc
date: 2012-09-18 16:07:00
tags:
- studenten
- medewerkers
title: Nieuwe printer 'toorop' bij centrale insteekverdieping
---
Een nieuwe [zwart-wit laserprinter](/nl/howto/printers-en-printen/)
‘toorop’, een HP LaserJet 9050 PCL 6, is geplaatst bij HG01.038, tussen
de twee centrale trappen op de eerste verdieping van het Huygensgebouw.
Binnenkort wordt de “insteekverdieping” daarnaast geopend, een
studielandschap zonder vaste werkplekken, geschikt voor laptop-gebruik.
Er zijn meer dan 60 aansluitingen voor stroom en netwerk. De
netwerk-aansluitingen zullen vrijgegeven worden als de test met [802.1x
netwerk-authenticatie](http://en.wikipedia.org/wiki/IEEE_802.1X)
voltooid is. Tot die tijd kan alleen het [draadloze
netwerk](/nl/howto/netwerk-draadloos/) gebruikt worden. Twee nieuwe
[wireless-N](http://nl.wikipedia.org/wiki/IEEE_802.11#802.11n) access
points bieden naar verwachting ruim voldoende connectiviteit.
