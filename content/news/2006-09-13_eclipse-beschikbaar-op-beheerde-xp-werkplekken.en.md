---
author: polman
date: 2006-09-13 10:29:00
title: Eclipse beschikbaar op beheerde XP werkplekken
---
Op alle beheerde windows XP werkplekken is op de T-disk
(\\\\cursus-srv\\cursus) [www.eclipse.org
Eclipse](/en/howto/www.eclipse.org-eclipse/) geïnstalleerd.
