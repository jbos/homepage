---
author: mkup
date: 2012-02-28 14:00:00
tags:
- studenten
- medewerkers
title: Netwerkproblemen na wijziging 28 januari opgelost
---
Op zaterdag 28 januari heeft [netwerkbeheer UCI](http://www.ru.nl/uci)
de FNWI-router van nieuwe software en configuratie voorzien, o.a. als
voorbereiding op IP-telefonie. De week erna werden er onduidelijke
netwerkproblemen gemeld, die achteraf getraceerd zijn naar een [te hoge
cpu-load](http://cricket.science.ru.nl/?target=%2Fswitches%2Fdr-huyg.heyendaal.net%2Fperformance;ranges=m;view=cpu)
van de router. Nadat eerst een aantal tijdelijke maatregelen genomen
zijn, is afgelopen maandag 20 februari 18:00-18:45 uur een nieuwe
configuratie ingevoerd, waarna de cpu-load niet meer hoog geweest is en
de netwerkproblemen stabieler verholpen lijken.
