---
author: caspar
date: 2013-05-29 15:50:00
tags:
- medewerkers
title: BASS and RU password the same
---
On Saturday June 1st, the password for BASS (Oracle) becomes the same as
for the other concern applications, i.e. to logon to BASS after June 1
the U number and RU password are used. The separate password for BASS
will disappear.

At the same time the web address (URL) of BASS changes to:
<https://bass.ru.nl/> Those who logon to BASS directly may want to
change their existing bookmark or desktop shortcut. Those who use the
port forwarder to login to BASS will have to change one or more settings
as well. More information about this can be found on the
[BASS](/en/howto/bass/) page.
