---
author: alinssen
date: 2010-03-12 17:25:00
title: Web application for employee attendance
---
Within various service departments of the Faculty of Science, a web
application for employee attendance (time registration) is used. This
application is based on the open source system
[TimeTrex](http://www.timetrex.com/). Other departments that are
interested in using the system for one or more employees, should contact
C&CZ. Registration is done with passes or on the web. It can be helpful
for the administration of variable working hours. It is a somewhat
flexible system, that e.g. can also be switched to automatically
register attendance, which makes it into a (sick) leave registration
system. There is no interface (yet) with the RU BASS-HRM registration of
(sick) leave, which has to be updated at least quarterly.
