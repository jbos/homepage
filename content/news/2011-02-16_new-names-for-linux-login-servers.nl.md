---
author: wim
date: 2011-02-16 10:53:00
tags:
- studenten
- medewerkers
title: Nieuwe namen voor Linux login servers
---
Op dit moment zijn de login servers
**lilo.science.ru.nl**
(**LI**nux **LO**gin server)
en **stitch.science.ru.nl** beschikbaar. Echter de
echte machines hebben een andere naam, respectivelijk
**porthos.science.ru.nl** en
**lijfwacht.science.ru.nl**. Dit geeft aanleiding
tot verwarring bij sommige gebruikers. Om dit probleem aan te pakken
worden machines uitgefaseerd en hernoemd.

De oudste server **lilo.science.ru.nl** zal
uitgefaseerd worden op het moment dat de
**lilo1.science.ru.nl** de taken kan overnemen. De
huidige Fedora login server
**stitch.science.ru.nl** wordt hernoemd naar
**lilo2.science.ru.nl**.

De naam **lilo** wordt tzt een alias voor de
snelste login server en de naam **stitch** wordt
voorlopig een alias voor **lilo2**. Bij vervanging
van login servers zullen we vervolgens doornummeren, dus
**lilo3**, **lilo4**, etc.
