---
author: visser
date: 2021-12-14 14:57:00
tags:
- backup
cover:
  image: img/2021/tape.png
title: Yearly backup during Christmas break
---
Just like every year during the Christmas break, in the next weeks a
[full backup of many servers/filesystems onto
tape](/en/howto/backup#monthly-and-yearly-backup-to-tape)
will be made, that is preserved a long time. This will have the effect
that servers will sometimes react slower during this period.
