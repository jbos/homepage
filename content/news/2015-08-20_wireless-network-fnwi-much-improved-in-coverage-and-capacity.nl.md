---
author: mkup
date: 2015-08-20 16:32:00
tags:
- medewerkers
- docenten
- studenten
title: Wireless netwerk FNWI erg verbeterd qua dekking en capaciteit
---
Gedurende de afgelopen maanden is het aantal wireless access-points in
het Huygensgebouw, Mercator 1 en HFML flink uitgebreid, van 97 naar 219.
Vrijwel al deze access-points ondersteunen ook de nieuwe
[802.11ac](http://www.allesoverdraadloosinternet.nl/wifi/wifi-80211ac/)
standaard. Daardoor zijn zowel dekking als capaciteit van het wireless
‘eduroam’ netwerk in deze gebouwen sterk verbeterd. Om zeker te weten
dat op belangrijke locaties zowel dekking als capaciteit optimaal zijn,
verricht het [ISC](http://www.ru.nl/isc) in de komende weken metingen.
Ook vragen wij gebruikers binnen FNWI die toch nog problemen ondervinden
met wireless, dit te melden bij de C&CZ helpdesk
(helpdesk\@science.ru.nl). Waar nodig zullen vervolgens aanpassingen
plaatsvinden. Nadere informatie over wireless is te vinden op [de ISC
website](http://www.ru.nl/ict/medewerkers/wifi/). De overige
FNWI-gebouwen krijgen een upgrade in 2016, indien er budget beschikbaar
is.
