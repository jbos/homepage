---
author: remcoa
date: 2022-09-23 13:05:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van DHZ.science.ru.nl
cover:
  image: img/2022/dhz.png
---
Vandaag is de nieuwe versie van de C&CZ-selfservice site
[DHZ](https://dhz.science.ru.nl) live gegaan. Deze versie heeft ook een
volledig herschreven back-end, waardoor nieuwe functionaliteiten de
komende tijd toegevoegd kunnen worden. De voornaamste nieuwe
functionaliteit die nu direct bruikbaar is, is de mogelijkheid om in te
loggen met het Radboud ID (personeelsnummer of studentnummer), om een
vergeten Science wachtwoord te zetten. Bij vragen, benader
[C&CZ](/nl/howto/contact/).
