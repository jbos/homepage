---
author: bram
date: 2016-04-26 17:00:00
tags:
- studenten
- medewerkers
title: 'Uitfasering svn-service: gebruik GitLab'
---
Met ingang van 1 juni zullen we onze
[Subversion](/nl/howto/subversion/)-server uitzetten. Op onze
Subversion-server is nog maar een klein aantal gebruikers actief. Als
alternatief wordt [GitLab](/nl/howto/gitlab/) aangeboden. Gebruikers van
svn-repositories op svn.science.ru.nl en svn.cs.ru.nl worden verzocht
hun repositories te verhuizen. Verhuisinstructies en mogelijke
alternatieven zijn op onze [Subversion](/nl/howto/subversion/)-pagina te
vinden.
