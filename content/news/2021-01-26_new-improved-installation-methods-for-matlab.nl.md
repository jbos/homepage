---
author: petervc
date: 2021-01-26 17:28:00
tags:
- medewerkers
- studenten
title: Nieuwe verbeterde installatiemethoden voor Matlab
---
De oude installatiemethode die men gebruikte voor Matlab R2020b
veroorzaakte problemen voor sommige gebruikers. De oplossing is gevonden
in een tweede installatiemethode. Sommige gebruikers zullen de voorkeur
geven aan de nieuwe, anderen aan de oude. Beide hebben voor- en nadelen.
De nieuwe maakt Matlab eenvoudig te installeren, ook voor oudere versies
en als men maar een paar toolboxen wil is de download klein. Maar men
moet zich registreren bij The MathWorks en de workflow is daarmee
complexer. Bij de oude installatiemethode is registratie niet nodig en
is de workflow eenvoudig, maar er is altijd een grote download en
niet-expert macOS-gebruikers zullen het lastig vinden omdat de nieuwste
macOS-versie de download in quarantaine zet.
