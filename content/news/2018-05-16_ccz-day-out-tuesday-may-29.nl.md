---
author: bbellink
date: 2018-05-16 14:09:00
tags:
- medewerkers
- studenten
title: 'C&CZ dagje uit: dinsdag 29 mei'
---
Het jaarlijkse dagje uit van C&CZ is gepland voor dinsdag 29 mei. Voor
bereikbaarheid in geval van ernstige storingen wordt gezorgd.
