---
author: petervc
date: 2010-04-29 15:12:00
title: Study landscape attendant for problems in PC-rooms
---
C&CZ and the Library of Science start a test on May 3, with using the
study landscape attendants for problems in PC-rooms on the ground floor
of the Huygens building. During this test,
[problems](/en/tags/storingen) with PC’s in these rooms should
preferably be reported to the study landscape attendant. The attendant
will also regularly check for violations of the rules of behaviour (no
eating, drinking or causing inconvenience).
