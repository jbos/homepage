---
author: polman
date: 2017-09-22 16:02:00
tags:
- studenten
- medewerkers
- docenten
title: 'Maple new version: Maple2017'
---
The latest version of [Maple](/en/howto/maple/), Maple2017, can be found
on the [Install](/en/howto/install-share/) network share and has been
installed on C&CZ managed Linux computers. License codes can be
requested from C&CZ helpdesk or postmaster by departments that take part
in the license.
