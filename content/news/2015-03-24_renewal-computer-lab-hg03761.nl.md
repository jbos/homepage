---
author: wim
date: 2015-03-24 15:14:00
tags:
- medewerkers
- studenten
title: Vernieuwing terminalkamer HG03.761
---
Inmiddels zijn in [terminalkamer](/nl/howto/terminalkamers/) HG03.761
alle PC’s vernieuwd. Er zijn 18 student-pcs, van het type [HP EliteOne
800
G1](http://store.hp.com/NetherlandsStore/Merch/Product.aspx?id=H5U26ET&opt=ABH&sel=PBDT)
(All-in-One, Core i5-4570S, 2.9GHz, 8GB, 23-inch widescreen LCD monitor,
sound), dual boot met Windows and Linux (Ubuntu). Deze zaal is per
25-03-2015 weer beschikbaar.
