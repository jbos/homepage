---
author: petervc
date: 2016-06-22 14:48:00
tags:
- medewerkers
- docenten
- studenten
title: "Gratis printbudget voor testen Péage"
---
De komende maanden worden [de door C&CZ beheerde Konica Minolta
MFP’s](/nl/howto/copiers&setlang=nl/) overgezet naar [het RU-brede
print/copy/scan-systeem Péage](http://www.ru.nl/peage). FNWI-studenten
en -medewerkers worden verzocht om dit te testen, om op die manier
mogelijke problemen vroeg te signaleren. Om dit aan te moedigen is
gratis Péage-budget beschikbaar voor de eerste tientallen FNWI-studenten
en -medewerkers die zich aanmelden bij de Science Postmaster als ze hun
ervaringen met Péage na het testen delen. Een Péage MFP is al jaren
beschikbaar bij de cafetaria in het Huygensgebouw, een andere staat
tijdens de testperiode in de straat bij vleugel 5 op de tweede
verdieping. Na de test zullen de KM’s op de begane grond en de
insteekverdieping als eerste naar Péage omgezet worden. C&CZ zal
FNWI-specifieke informatie bijhouden op de [C&CZ Péage
pagina](/nl/howto/peage/).
