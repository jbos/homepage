---
author: petervc
date: 2008-12-08 23:46:00
title: C&CZ (mid)dagje uit
---
Woensdagmiddag 17 december is C&CZ vanaf ca 14:00 een (mid)dagje uit.
Voor noodgevallen blijft C&CZ ook dan [telefonisch](/nl/howto/helpdesk/)
bereikbaar.
