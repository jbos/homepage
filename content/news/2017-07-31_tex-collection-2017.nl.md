---
author: polman
date: 2017-07-31 17:36:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2017
---
De nieuwste versie van de [TeX
Collection](http://www.tug.org/texcollection), 2017, voor MS-Windows,
Mac OS X en Linux, is beschikbaar. De DVD is te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf.
