---
author: bertw
date: 2013-09-24 16:40:00
tags:
- medewerkers
- docenten
title: 'New phone number C&CZ Helpdesk: 20000'
---
From now on the C&CZ Helpdesk can be reached through telephone number
20000. The current number 56666 will be phased out. Until November 1
2013, the helpdesk can still be reached through the old number. After
that you’ll hear a message to inform you about the change after which
you will be transferred to the new number. From the first week of 2014,
the old number will permanently go out of service.
