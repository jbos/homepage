---
author: petervc
date: 2015-01-15 00:04:00
tags:
- medewerkers
- docenten
title: 'SURFdrive: veiliger dan Dropbox'
---
Het [ISC](http://www.ru.nl/isc) heeft
[bekendgemaakt](http://www.ru.nl/informatiebeveiliging/nieuws-0/nieuws/@977838/surfdrive-opslaan/)
dat [SURFdrive](https://surfdrive.surf.nl/) beschikbaar is voor
RU-medewerkers. Er is tot 100 GB beschikbaar voor de Dropbox-achtige
opslag van werkgerelateerde bestanden, waarvan de
beveiligingsclassificatie niet kritisch of gevoelig hoort te zijn. Data
zijn binnen de Nederlandse grenzen opgeslagen, gebruikers blijven
eigenaar van hun eigen data en SURF zelf verstrekt geen informatie aan
derden, waardoor SURFdrive als veiliger dan Dropbox beschouwd kan
worden. [Eerder](/nl/news/) maakte SURF het al
mogelijk om met [SURFfilesender](https://www.surffilesender.nl/) grotere
bestanden veiliger te versturen dan met alternatieven als
[WeTransfer](https://www.wetransfer.com).
