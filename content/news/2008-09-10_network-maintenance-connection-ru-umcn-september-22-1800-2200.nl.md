---
author: petervc
date: 2008-09-10 15:49:00
title: Netwerkonderhoud verbinding RU-UMCN 22 september 18:00-22:00
---
Op 22 september tussen 18:00-22:00 uur krijgt de netwerkverbinding
tussen RU en UMCN St. Radboud een upgrade naar 1 Gigabit per seconde,
waardoor er gedurende die tijd problemen met die verbinding kunnen zijn.
