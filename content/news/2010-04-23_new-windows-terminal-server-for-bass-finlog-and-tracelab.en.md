---
author: wim
date: 2010-04-23 15:25:00
title: New Windows Terminal Server for BASS-Finlog and Tracelab
---
Because the current Windows terminal Server
**ts1.nwi.ru.nl** shows some hardware problems, we
decided to replace the machine. The new machine is a virtual Windows
2008 Terminal Server. This implies the Windows user interface has
changed, it looks more like Windows Vista or Windows 7. Connecting to
this machine should work the same as with the previous server, just
change the name **ts1.nwi.ru.nl** to
**ts2.nwi.ru.nl**. This machine is primarily
intended for using [BASS-Finlog](/en/howto/bass/) and
[Tracelab](/en/howto/tracelab/).
