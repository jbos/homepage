---
author: polman
date: 2021-12-21 15:15:00
tags:
- medewerkers
- studenten
title: New version of Mathematica (13.0.0)
cover:
  image: img/2021/mathematica-13.png
---
A new version of [Mathematica](/en/howto/mathematica/) (13.0.0) has been
installed on all C&CZ managed Linux systems, older versions can still be
found in `/vol/mathematica`. The installation files for Windows, Linux
and macOS can be found on the [Install](/en/howto/install-share/)-disk,
also for older versions of Mathematica. Installation and license info
can be [requested from C&CZ](/en/howto/contact/).
