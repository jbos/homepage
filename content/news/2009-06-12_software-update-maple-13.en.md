---
author: petervc
date: 2009-06-12 17:55:00
title: 'Software update: Maple 13'
---
A new version (13) of [Maple](/en/howto/maple/) has been installed on
Linux/Solaris. Early next week it will also be available on the
[Windows-XP Managed PCs](/en/howto/windows-beheerde-werkplek/). It can be
installed on Windows from the
[install](http://www.cncz.science.ru.nl/software/installscience) network
disk, but the DVDs can also be borrowed by employees and students, for
home use too.
