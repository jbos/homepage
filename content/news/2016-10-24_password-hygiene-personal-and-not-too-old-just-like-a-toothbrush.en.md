---
author: petervc
date: 2016-10-24 22:34:00
tags:
- studenten
- medewerkers
- docenten
title: 'Password hygiene: personal and not too old, just like a toothbrush'
---
The RU has recently held a [Treat your password like your
toothbrush](http://www.ru.nl/privacy/english/news/news/@1052919/treat-your-password-like-your-toothbrush-campaign/)
campaign. C&CZ too will act on old passwords: IT contacts will get a
list of their users that have not changed their Science password in
years. Old Science passwords do not have the most recent encryptions,
therefore we would like to see them changed to remedy this. Even
choosing a new Science password that is the same as the old one, helps
to get rid of old password encryptions but please do choose a new
password. Science passwords can be changed at the [Do-It-Yourself
website](https://diy.science.ru.nl). When changing a password, please
consider beforehand which (also private) devices will automatically keep
on trying the old password, in order to prevent any inconvenience.
