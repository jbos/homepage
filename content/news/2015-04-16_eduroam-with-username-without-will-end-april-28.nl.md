---
author: mkup
date: 2015-04-16 16:55:00
tags:
- medewerkers
- docenten
- studenten
title: Eduroam met gebruikersnaam zonder @ stopt 28 april
---
Vanaf 28 april is het niet meer mogelijk om met alleen een U- of
S-nummer, zonder daarachter het domein @ru.nl, gebruik te maken van het
[draadloze netwerk Eduroam](http://www.ru.nl/ict/studenten/eduroam/).
Tot die tijd werkt dat wel, maar alleen op de RU-campus, omdat de RU
[RADIUS-server](http://nl.wikipedia.org/wiki/RADIUS) in zo’n geval zelf
@ru.nl aanneemt als RADIUS domein. Wanneer men een gebruikersnaam
gebruikt met een RADIUS-domein, zoals @ru.nl of @science.ru.nl, dan kan
men Eduroam gebruiken bij [alle op Eduroam aangesloten
instellingen](https://www.eduroam.nl/organisations.php). Voor het
toevoegen van het domein aan de Eduroam-instellingen, zie [de ISC
aankondiging](http://www.radboudnet.nl/actueel/nieuws/@991416/wifi-campus-vanaf-28/).
