---
author: petervc
date: 2016-11-22 10:40:00
tags:
- medewerkers
- docenten
title: Eduroam wifi codes voor gasten
---
RU-medewerkers kunnen op [de RU Portal](https://portal.ru.nl) een
dagcode en telefoonnummer vinden, zodat hun gasten
[wifi](/nl/howto/netwerk-draadloos/) kunnen gebruiken. Gasten komen
daarmee op een afgescheiden deel van het Eduroam draadloze netwerk. Voor
meer info zie de [ISC
website](http://www.ru.nl/ict/medewerkers/wifi/wifi-gasten/).
