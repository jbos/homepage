---
author: wim
date: 2017-10-04 10:10:00
tags:
- studenten
- medewerkers
- docenten
title: Printer Escher wordt opgeruimd
---
De laatste algemene C&CZ-beheerde printer, de zwart-wit printer Escher
in de Library of SWcience wordt per 1 november uitgezet en verwijderd.
Hiermee komt er een einde aan het C&CZ gebudgetteerde printsysteem. De
C&CZ printbudgetten worden alleen nog gebruikt voor de niet direct
toegankelijke [poster- en 3D-printers](/nl/howto/printers-en-printen/).
Printen op openbare printers gebeurt al meer dan een jaar via
[Peage](/nl/howto/peage/).

Resterend C&CZ printbudget kan gebruikt worden voor [poster of 3D
printen](/nl/howto/printers-en-printen/), dan wel geclaimd worden bij
C&CZ (HG03.055).
