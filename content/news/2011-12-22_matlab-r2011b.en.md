---
author: petervc
date: 2011-12-22 16:42:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2011b
---
The latest version of [Matlab](/en/howto/matlab/), R2011b, is available
for departments that have bought licenses. The software and license
codes can be acquired through a mail to postmaster for those entitled to
it. The software can also be found on the
[install](/en/tags/software)-disc. The C&CZ-managed 64-bit Linux
machines have this version installed, an older version
(/opt/matlab-R2011a/bin/matlab) is still available temporarily. The
C&CZ-managed Windows machines will receive this version too.
