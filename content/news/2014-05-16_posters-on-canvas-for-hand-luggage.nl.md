---
author: stefan
date: 2014-05-16 12:15:00
tags:
- medewerkers
- studenten
title: Posters op canvas (voor in handbagage)
---
Printen op canvas is nu mogelijk op de posterprinter
“[Kamerbreed](/nl/howto/kamerbreed/)”. Een poster op canvas kan gevouwen
worden en daardoor meegenomen worden in de handbagage. Dat kan een
voordeel zijn t.o.v. een poster op fotopapier, die in een grote koker
vervoerd wordt. De prijs voor printen op canvas is 36 € per A0.
