---
author: pberens
date: 2011-06-16 11:17:00
tags:
- docenten
title: Blackboard FAQ
---
Blackboard Helpdesk binnenkomen zijn op een rijtje gezet. Zit uw
probleem er niet tussen, of komt u er niet uit mail dan gerust naar
bb-nwi\@science.ru.nl [Blackboard FAQ](/nl/howto/blackboard/)
