---
author: petervc
date: 2013-05-22 11:13:00
tags:
- medewerkers
- docenten
title: MS Windows 8 on Install network share
---
The most recent version of [Microsoft
Windows](http://office.microsoft.com), 8, is available on the
[Install](/en/howto/install-share/) network share. The license permits
use on university computers. License codes can be requested from C&CZ
helpdesk or postmaster. One can also order the DVD’s on
[Surfspot](http://www.surfspot.nl).
