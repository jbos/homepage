---
author: sioo
date: 2017-05-01 10:30:00
tags:
- medewerkers
- docenten
- studenten
title: 'Linux Login servers: default Ubuntu 16.04'
---
Met het aflopen van de ondersteuning van Ubuntu 12.04 LTS per 25 april
2017 wordt het tijd om te stoppen met ubuntu 12.04 op de linux login
server (lilo3). Lilo3 gaat daarom per 1 mei uit en de default login
server (lilo.science.ru.nl) wordt doorverwezen naar lilo5, die sinds
najaar 2016 al beschikbaar is met ubuntu 16.04 LTS. De huidige default
met Ubuntu 14.04 LTS (lilo4) zal nog wel beschikbaar blijven onder de
naam lilo4.science.ru.nl, tot de ondersteuning daarvan afloopt in 2019.

Signatures lilo5:

2048 SHA256:VK8+B+OuT+n8JSjoy9uWYw5COb+cZ+01S/pWiAteEtA lilo5 (RSA) 256
SHA256:NeO+rdPufkFYEgLs6YMWPvMJzOBK9pNlxdjMJzK3OBY lilo5 (ECDSA) 256
SHA256:zViYANQchkRs6QNPSSoxOkVyA8Ixe7aZCvzcSQEZK7s lilo5 (ED25519)
