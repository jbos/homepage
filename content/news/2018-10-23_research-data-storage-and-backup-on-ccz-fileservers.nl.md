---
author: petervc
date: 2018-10-23 13:40:00
tags:
- medewerkers
title: 'Research data: opslag en backup op C&CZ fileservers'
---
Met de onlangs [verlaagde prijzen voor netwerkschijven en
backup](https://wiki.cncz.science.ru.nl/index.php?title=Nieuws&setlang=nl#.5BLagere_prijzen_voor_netwerkschijven_en_backup.5D.5BLower_prices_for_network_discs_and_backup.5D),
zijn al diverse afdelingen de overstap gaan maken van lokale schijven in
eigen beheer, naar gebackupte schijfruimte onder beheer van C&CZ. Daar
zitten i.h.a. [grote
voordelen](https://wiki.cncz.science.ru.nl/index.php?title=Diskruimte&setlang=nl#.5BFunctionaliteit_en_prijs_van_netwerkschijven.5D.5BFunctionality_and_costs_of_network_shares.5D)
aan qua betrouwbaarheid, backup en door de afdeling zelfbeheerde
toegangsrechten. Voornaamste nadeel is dat schijfruimte die gebackupt
moet worden, opgezet moet worden in projecten of andere onderdelen van
max. ca. 400 GB. Afdelingen die hun data willen verhuizen naar C&CZ
fileservers, kunnen [contact opnemen met postmaster](/nl/howto/contact/).
De huidige opzet is gebaseerd op individuele
[NAS-fileservers](https://nl.wikipedia.org/wiki/Network-attached_storage),
C&CZ onderzoekt een nieuwe opzet gebaseerd op
[Ceph](https://en.wikipedia.org/wiki/Ceph_(software)).
