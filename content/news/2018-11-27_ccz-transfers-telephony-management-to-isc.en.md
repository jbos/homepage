---
author: bbellink
date: 2018-11-27 11:22:00
tags:
- medewerkers
title: C&CZ transfers telephony management to ISC
---
With the move to
[IPT](https://wiki.cncz.science.ru.nl/Categorie:Telefonie#.5BIP_Telefonie_.28IPT.29.5D.5BIP_Telephony_.28IPT.29.5D),
telephony has become a network application. Telephony management has
become much less time consuming. Therefore, as of January 1, 2019, C&CZ
will transfer the management of FNWI telephony to the [RU central
telephony management at the
ISC](https://www.ru.nl/ict-uk/staff/telephones/). For networking at
FNWI, you can still [contact C&CZ](/en/howto/contact/).
