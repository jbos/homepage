---
author: keesk
date: 2008-06-25 15:24:00
title: ChemBioOffice
---
Floris van Delft (department Organic Chemistry) made a description of
the functionality of the software package
[ChemBioOffice](/en/howto/chembiooffice/). The package may be downloaded,
free of charge, by everyone who has a science mail account (staff and
students). Floris (F.vanDelft\@science.ru.nl) is willing to answer
questions about the usage of the package.
