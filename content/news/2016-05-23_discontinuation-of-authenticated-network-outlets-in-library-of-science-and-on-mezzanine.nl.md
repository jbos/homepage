---
author: mkup
date: 2016-05-23 16:39:00
tags:
- medewerkers
- studenten
title: Opheffen inprikpunten met authenticatie in Library of Science en op insteekverdieping
---
Sinds eind 2013 zijn in de leeszaal van de [Library of
Science](http://www.ru.nl/library/library/library-locations/library-science/)
en op de insteekverdieping boven de cafetaria een aantal
geauthenticeerde netwerk-inprikpunten beschikbaar. Nadat het draadloze
netwerk ([eduroam](http://www.ru.nl/ict/medewerkers/wifi/)) in Huygens
in 2015 sterk is verbeterd, blijken deze inprikpunten vrijwel niet meer
te worden gebruikt. Daarom is door C&CZ en het ISC besloten om ze
inactief te maken per maandag 6 juni 2016, waarna de bijbehorende
[Qmanage](http://www.quarantainenet.nl/oplossingen/detectie/)) server
wordt uitgezet.
