---
author: bram
date: 2018-03-12 14:44:00
tags:
- medewerkers
- docenten
- studenten
title: Mattermost team communication platform available
---
team communication platform Mattermost is available for anyone with a
Science-login. Mattermost is an open source alternative to the
proprietary Slack messaging service. Mattermost teams can setup
integrations with GitLab projects and other (custom) services. Visit
Mattermost at

<https://mattermost.science.ru.nl>`.`
