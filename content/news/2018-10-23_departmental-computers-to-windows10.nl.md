---
author: stefan
date: 2018-10-23 11:12:00
tags:
- medewerkers
title: Afdelingscomputers naar Windows10
---
Nieuwe afdelingscomputers kunnen door C&CZ met Windows10 geïnstalleerd
worden. Ook bestaande pc’s kunnen een upgrade van Windows7 naar
Windows10 krijgen. Maak hiervoor een afspraak met de [C&CZ
helpdesk](/nl/howto/contact/). Via het Software Center kunnen gebruikers
daarna zelf software installeren uit de catalogus van het Software
Center. N.B.: de Outlook2016 die bij Windows10 geïnstalleerd wordt,
werkt niet goed samen met de Science IMAP-mailserver. Alternatieven:
mailclients als Roundcube en Thunderbird of een andere mailserver als de
RU Exchange mail- en agendaservice.
