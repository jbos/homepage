---
author: caspar
date: 2015-07-07 13:49:00
tags:
- medewerkers
title: 'Nieuwe medewerker systeemontwikkeling en -beheer: Simon Oosthoek'
---
Vanaf 1 juli jl. is {{< author "simon" >}} bij FNWI in dienst getreden
als C&CZ-medewerker [systeemontwikkeling en
-beheer](/nl/howto/systeemontwikkeling/).
