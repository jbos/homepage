---
author: petervc
date: 2012-04-25 13:47:00
tags:
- studenten
- medewerkers
- docenten
title: Negeer nooit een certificaat-waarschuwing
---
Wanneer een programma klaagt over een fout certificaat van bv. een
website, negeer dat dan nooit, maar [meld dit z.s.m. aan
C&CZ](/nl/howto/contact/). De directe aanleiding voor deze waarschuwing
is een recente klacht van een browser, die wees op de aanwezigheid van
een besmette laptop, die een zogenaamde [ARP spoofing
aanval](http://en.wikipedia.org/wiki/ARP_spoofing) lanceerde. De
bedoeling van zo’n aanval is om netwerkverkeer af te luisteren om o.a.
wachtwoorden te onderscheppen.
