---
author: petervc
date: 2017-08-11 15:16:00
tags:
- medewerkers
- docenten
title: Nieuwe versie Filemaker Pro, ook Advanced en Server, op Install-schijf
---
Een nieuwe versie van [Filemaker
Pro](http://www.filemaker.com/nl/products/filemaker-pro/), [Filemaker
Pro
Advanced](http://www.filemaker.com/nl/products/filemaker-pro-advanced/)
en van [Filemaker
Server](http://www.filemaker.com/nl/products/filemaker-server/), 16,
voor Windows (32- en 64-bit) en Mac is op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. De
licentievoorwaarden staan gebruik op RU-computers toe. Licentiecodes
zijn bij C&CZ helpdesk of postmaster te verkrijgen. Voor thuisgebruik
kan de software op [Surfspot](http://www.surfspot.nl) besteld worden.
