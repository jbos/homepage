---
author: petervc
date: 2017-02-21 17:14:00
tags:
- studenten
title: MobaXterm in all RU study areas
---
The [ISC](http://www.ru.nl/isc) installed [MobaXterm](/en/howto/ssh/) in
all their managed RU study areas. With MobaXterm, one can connect to
[Linux servers](/en/howto/hardware-servers/) and use X11 graphics
software. This was a request of students of the Faculty of Science,
because the [central library](http://www.ru.nl/library/) has more
extended opening hours than the [Library of
Science](http://www.ru.nl/library/library/library-locations/library-of-science/).
