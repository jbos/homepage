---
author: petervc
date: 2015-02-13 14:02:00
tags:
- studenten
- medewerkers
title: Save your SURFspot product safe before March 1\!
---
SURFspot [announced](https://www.surfspot.nl/productkluis) that the
product safe will disappear in the transition to a new web shop
platform. The product safe contains installation codes and download
links of your download order(s). You need these data when you for
example: - Still have credit facilities (for example you have purchased
an antivirus for 5 PCs and only installed on 2 PCs yet). - Your computer
crashes, you can reinstall the software with the information from your
product safe.

Di you buy and download software from [SurfSpot](http://www.surfspot.nl)
whose license has not expired? Follow [the recommended steps (in
Dutch)](https://www.surfspot.nl/productkluis).
