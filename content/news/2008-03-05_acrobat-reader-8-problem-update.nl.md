---
author: petervc
date: 2008-03-05 17:03:00
title: Acrobat Reader 8 probleem update
---
Omdat Acrobat Reader 8 voor MS-Windows PostScript produceert waar de HP
LaserJet 4250/4350 [printers](/nl/howto/printers-en-printen/) niet mee
overweg kunnen, is C&CZ overgegaan tot het gebruik van de (langzamere)
PCL-drivers voor de meeste van deze printers. Daarnaast is
[FoxIt\_PDF\_Reader](/nl/howto/foxit-pdf-reader/) neergezet op de
[S-schijf](/nl/howto/s-schijf/) als alternatief voor Acrobat Reader.
