---
author: josal
date: 2009-02-09 12:31:00
title: Again power failure server room HG04.070
---
Saturday Jan 31 around 03:00h the power supply fire protection unit in
the server room HG04.070 was activated again, which caused the immediate
and complete power down of all servers in that room. This time as well
no sensor had detected a temperature high enough to activate the
protection unit.

There was no extra information available to determine a more definitive
cause for the immediate ‘Emergency Stop’ of the UPS unit.

To prevent a new faulty fire alarm event, only one (1) temporary,
mechanical temperature switch, can activate the UPS ‘Emergency Stop’.

The complete fire alarm power switching unit with 4 temperature senors
has been disconnected from the UPS ‘Emergency Stop’. This system is
still working and is still connected to and monitored by the building
alarm system.

It has been discovered that every door entrance has a fire-key-switch,
directly connected to the UPS ‘Emergency Stop’. These switches were not
connected to the building alarm system. The switches are now
disconnected from the UPS ‘Emergency Stop’ and are now connected to and
monitored by the building alarm system.

This setup ensures that:

-   there still is a working fire power protection unit, be it with a
    only one slow sensor at one place (near the hot spot).
-   a failure in the old circuit of the door switches will be detected,
    but cannot cause a UPS ‘Emergency Stop’.
-   it will be clear when an UPS internal problem is the cause of an UPS
    ‘Emergency Stop’.

Depending on future alarms, new information will come available to
explain power failures and their causes.

It is agreed that the current situation will remain in place for at
least two months unless an alarm occurs.
