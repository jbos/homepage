---
author: petervc
date: 2010-05-21 12:37:00
title: Verminderen mail van \*-studenten-\* mailing lists
---
Studentenvertegenwoordigers meldden in het reguliere overleg met C&CZ
dat sommige studenten de hoeveelheid mails die ze ontvangen via de
[Mailman mailinglists](/nl/tags/email) graag zouden zien verminderen. De
mailing lists voor studenten zijn de paraplu-lijst
[fnwi-studenten](http://mailman.science.ru.nl/mailman/listinfo/fnwi-studenten)
en de lijsten voor studierichting en jaargang waaruit de paraplu-lijst
bestaat. Daarom heeft C&CZ de configuratie van de -studenten- mailing
lists aangepast, zodat digests (samenvattingen) voor de -studenten-
lijsten nog maar eens per week worden verstuurd. Dit heeft natuurlijk
alleen effect voor studenten die aangegeven hebben zo’n digest te willen
ontvangen in plaats van elke mail afzonderlijk. Voor alle andere C&CZ
[Mailman mailinglists](http://mailman.science.ru.nl/) blijft de digest
periode op de standaard 1 dag staan. De digest optie kan per gebruiker
per mailing list aangezet worden, b.v. op de website van de
studierichting/jaargang-lijst, via “change your subscription options”.
