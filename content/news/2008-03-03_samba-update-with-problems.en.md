---
author: wim
date: 2008-03-03 14:47:00
title: Samba update with problems?
---
C&CZ will upgrade Samba on several servers in the weeks to follow. These
upgrades could cause problems when connecting network drives on Windows
PC’s. At [Upgrade Samba 3.0.28](/en/howto/upgrade-samba-3.0.28/) you can
read how to solve these problems.
