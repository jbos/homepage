---
author: petervc
date: 2014-01-27 16:48:00
tags:
- medewerkers
- studenten
title: New network disks bigger and less expensive
---
Because C&CZ has a new file server, we can lower the pricing of [network
disks](/en/howto/diskruimte/) by 50%. Due to the recently announced [new
backup service](/en/news/) larger disks (400GB) are also
possible. A new option is an un-backupped disk, which is four times less
expensive. Such a disk could be used for copies of a local disk.
Contracts for hiring a network disk are still entered into for a period
of three years. Existing contracts are not changed.
