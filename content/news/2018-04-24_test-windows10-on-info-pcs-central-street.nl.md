---
author: wim
date: 2018-04-24 16:04:00
tags:
- medewerkers
- studenten
title: Test Windows10 op infopc's centrale straat
---
In de zomervakantie krijgen alle [pc’s in de pc-cursuszalen
(terminalkamers)](/nl/howto/terminalkamers/) een upgrade van Windows7
naar Windows10. Dit kan nu al getest worden op de 4 infozuil PC’s in de
centrale straat bij de Library of Science (HG00.011). Alle [vragen en
opmerkingen zijn welkom](/nl/howto/contact/). N.B.1: het Windows7-profiel
wordt niet overgenomen, iedereen begint met een schoon profiel. N.B.2:
omdat de infozuil pc’s steeds minder gebruikt worden, zullen ze in de
zomervakantie verwijderd worden.
