---
author: wim
date: 2015-02-20 14:12:00
tags:
- medewerkers
- studenten
title: PC labs upgrade to Ubuntu 14.04 during summer break
---
During the summer break the operating system of all PCs in the [PC
labs](/en/howto/terminalkamers/) will be upgraded from Ubuntu 12.04 to
Ubuntu 14.04. This might have an effect on software used in courses. You
can test your software on the login server lilo4, which is already
running Ubuntu 14.04.
