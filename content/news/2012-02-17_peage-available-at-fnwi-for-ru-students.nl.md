---
author: caspar
date: 2012-02-17 19:03:00
tags:
- studenten
title: Peage beschikbaar bij FNWI voor studenten RU
---
Vanaf 13 februari 2012 kunnen studenten van FNWI gebruik maken van de
[Peage](/nl/howto/print-project-peage/) print- en kopieervoorzieningen
van de Radboud Universiteit. FNWI medewerkers kunnen er nog geen gebruik
van maken. Totdat iedereen met Peage kan printen worden binnen FNWI
zowel het [bestaande systeem](/nl/howto/printers-en-printen/) als
[Peage](/nl/howto/peage/) aangeboden.
