---
author: caspar
date: 2013-03-12 15:36:00
tags:
- studenten
- docenten
- medewerkers
title: Vervanging van Ricoh multifunctionals door Konica Minolta
---
De Europese aanbesteding van multifunctionals (MFP’s:
printer/copier/scanner) voor de universiteit is gewonnen door [Konica
Minolta](/nl/howto/km/). Dat betekent dat de huidige
[Ricoh](/nl/howto/ricoh/)-MFP’s vervangen zullen worden. Er loopt een
project om het MFP-systeem [Peage](http://www.ru.nl/peage) geschikt te
maken voor medewerkers. In dit systeem zullen de print- en kopieerkosten
voor medewerkers worden doorbelast naar de aanstellingskostenplaats, de
huidige budgetgroepen zullen dus verdwijnen. Tot dit project succesvol
afgerond is, zal het [C&CZ
printbudget-systeem](/nl/howto/printers-en-printen/) in gebruik blijven.

-   Omdat het nieuwe Peage systeem geen [Xafax
    copycards](/nl/howto/ricoh/) ondersteunt, is het verstandig de eigen
    voorraad copycards zo klein mogelijk te houden.
-   De eerste MFP’s van Konica Minolta in FNWI worden half maart
    geplaatst voor de uitbreiding Huygens 2.0 (Mercator 1).
-   De eerste vervanging van Ricoh-machines in FNWI zal half april de
    [Peage](http://www.ru.nl/peage) printer Watt bij het restaurant
    zijn.
