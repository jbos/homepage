---
author: visser
date: 2021-12-14 14:57:00
tags:
- backup
title: Jaarlijkse backup in kerstvakantie
cover:
  image: img/2021/tape.png
---
Zoals elk jaar zal [tijdens de kerstvakantie een volledige backup op
tape](/nl/howto/backup#monthly-and-yearly-backup-to-tape) van heel veel
servers/bestandssystemen gemaakt worden, die lang bewaard wordt. Dit zal ervoor
zorgen dat servers in deze periode soms trager reageren.
