---
author: petervc
date: 2014-01-09 15:39:00
tags:
- medewerkers
- studenten
title: Backupvoorziening geheel vernieuwd
---
Van alle belangrijke [netwerkschijven](/nl/howto/diskruimte/) worden door
C&CZ [backups](/nl/howto/backup/) gemaakt. In de afgelopen maanden is de
[backup service](/nl/howto/backup/) volledig vernieuwd: sneller en meer
capaciteit om in de groeiende diskruimte-behoefte te voorzien. Neem bij
verlies van bestanden van de netwerkschijven z.s.m. [contact op met
C&CZ](/nl/howto/contact/) en geef zo compleet mogelijke informatie over
de naam van de netwerkschijf en de bestanden en een zo lang mogelijke
tijdperiode waarin de bestanden nog aanwezig waren.
