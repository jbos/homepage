---
author: petervc
date: 2013-11-21 16:25:00
tags:
- medewerkers
- studenten
title: F-Secure beveiligingssoftware update beschikbaar / thuisgebruik niet meer gratis
---
F-Secure is niet langer gratis voor thuisgebruik, maar kan wel nog
steeds relatief goedkoop aangeschaft worden door medewerkers en
studenten via [Surfspot](http://www.surfspot.nl). Surfspot moet de
prijsverlaging naar ca € 11.50 nog verwerken, het advies is om daarop te
wachten. Voor systemen die eigendom van de RU zijn, is het gebruiksrecht
voor de F-Secure beveiligingssoftware door de RU afgekocht. De
programmatuur, ook voor de nieuwste versie die Windows 8 ondersteunt, is
te vinden op de [Install](/nl/howto/install/) netwerkschijf. Deze
informatie zal binnenkort ook verschijnen op de F-Secure pagina op
[Radboudnet](http://www.radboudnet.nl/fsecure).
