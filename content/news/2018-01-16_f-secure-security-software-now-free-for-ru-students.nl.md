---
author: petervc
date: 2018-01-16 10:33:00
tags:
- studenten
title: F-Secure beveiligingssoftware nu gratis voor RU-studenten
---
Vanaf nu kun je als student aan de Radboud Universiteit het hele jaar
door gratis F-Secure SAFE downloaden op je
pc/laptop/smartphone/tablet/iMac/Macbook. Zie voor alle info de
[SURFspot download
pagina](https://www.surfspot.nl/f-secure-safe-1-apparaat-radboud-universiteit-nijmegen-en-fontys.html).
