---
author: petervc
date: 2019-05-28 16:42:00
tags:
- medewerkers
- studenten
title: Intel compilers update
---
Update 4 of the [Intel Parallel Studio XE Cluster Edition for Linux
2019](https://software.intel.com/en-us/parallel-studio-xe) has been
installed in `/vol/opt/intelcompilers` and is available on a.o.
[clusternodes](/en/howto/hardware-servers/) en
[loginservers](/en/howto/hardware-servers/). The older (2019 and 2014)
versions can also be found there. To set the environment variables
correctly, BASH-users must first run:

source
/vol/opt/intelcompilers/intel-2019u4/composerxe/bin/compilervars.sh
intel64

After that, `icc -V` gives the new version number as output. For more
info see [the page on Intel
compilers](https://wiki.cncz.science.ru.nl/index.php?title=Intel_compilers&setlang=en)
