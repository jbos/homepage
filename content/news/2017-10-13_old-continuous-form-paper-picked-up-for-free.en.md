---
author: petervc
date: 2017-10-13 12:33:00
tags:
- medewerkers
- docenten
title: Old continuous form paper picked up for free
---
At [the entrance of C&CZ](/en/howto/contact/) we have put a stack of
boxes with [continuous form
paper](https://en.wikipedia.org/wiki/Continuous_stationery), that can be
picked up for free. Years ago that paper was used in the C&CZ managed
matrix printers, that were always free of charge as well.
