---
author: caspar
date: 2011-09-28 13:38:00
tags:
- medewerkers
title: Klantenmiddag GDI, CIM en UCI
---
De [Gebruikersdient ICT (GDI)](http://www.ru.nl/gdi), de afdeling
[Concern Informatie Management (CIM)](http://www.ru.nl/cim) en het
[Universitair Centrum Informatievoorziening (UCI)](http://www.ru.nl/uci)
houden een gezamenlijke klantenmiddag op donderdag 20 oktober van
14.00-17.00 uur in de Mohrmanzaal (Aula, Comeniuslaan 2). Projectleiders
en andere RU-deskundigen spreken die middag over recente ontwikkelingen
rondom de digitalisering van documenten op de campus, het opnemen van
persoonsgegevens in concernsystemen, het e-mail- en agendapakket Share
en het facilitair managementinformatiesysteem Planon. U kunt zich
aanmelden (voor 1 oktober) door een mail te sturen naar het
UCI-secretariaat of even te bellen (17999).
