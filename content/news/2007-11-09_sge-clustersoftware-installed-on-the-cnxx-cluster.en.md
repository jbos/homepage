---
author: petervc
date: 2007-11-09 17:52:00
title: SGE Clustersoftware installed on the cnXX-cluster
---
On the cnXX-cluster of machines of different departments the
clustersoftware [Sun GridEngine](http://gridengine.sunsource.net) has
been installed. Before using it, please read the
[clustersoftware](/en/howto/software-cluster/) page.
