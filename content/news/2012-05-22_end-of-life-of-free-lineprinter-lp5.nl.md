---
author: petervc
date: 2012-05-22 15:42:00
tags:
- studenten
- medewerkers
- docenten
title: Einde aan gratis regelprinter lp5
---
Sinds ca. 1995 was het mogelijk om gratis op kettingformulieren te
printen op de [printer](/nl/howto/printers-en-printen/) lp5. Omdat deze
printer sinds vorige week een serieus hardwareprobleem vertoont, waarvan
de dure reparatie niet opweegt tegen het incidentele gebruik door een
tiental studenten, is besloten de printer niet meer te proberen te laten
repareren.
