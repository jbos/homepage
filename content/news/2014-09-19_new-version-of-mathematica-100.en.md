---
author: polman
date: 2014-09-19 17:57:00
tags:
- medewerkers
- studenten
title: New version of Mathematica (10.0)
---
A new version of [Mathematica](/en/howto/mathematica/) (10.0) has been
installed on all C&CZ managed Linux systems. The installation files for
Windows, Linux and Apple Mac can be found on the
[Install](/en/howto/install-share/)-disk. Departments that take part in
the license can request installation and license info from C&CZ.
