---
author: petervc
date: 2008-06-13 15:39:00
title: 'Adobe: employee home-use site-license software available on surfspot.nl'
---
On [Surfspot](http://www.surfspot.nl) the site-license products of
[Adobe](http://www.adobe.com) for home-use by employees are available
for 35 euro per bundle. One can choose between the English or Dutch
version of the ‘web’ or ‘design’ bundle. Both bundles contain a.o.
Photoshop, Illustrator, Acrobat Professional, Dreamweaver. Only the
‘design’ bundle contains InDesign. The site license products for
installation on campus have been urgently ordered.
