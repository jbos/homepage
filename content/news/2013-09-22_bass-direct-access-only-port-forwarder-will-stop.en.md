---
author: petervc
date: 2013-09-22 13:51:00
tags:
- medewerkers
- studenten
- docenten
title: 'BASS: direct access only, Port Forwarder will stop'
---
As of September 25, direct access to BASS is possible from almost all
FNWI desktops. Those who cannot go directly to BASS can use
[VPN](/en/howto/vpn/) or the [Windows Terminal
Server](/en/howto/windows-terminal-server/). The indirect access to BASS
through the Port Forwarder will be terminated. Port Forwarder users who
manage their own desktop/laptop system must delete the port forwarder
lines from their *hosts* file. For more information, see the
[BASS](/en/howto/bass/) page.
