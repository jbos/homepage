---
author: petervc
date: 2012-02-28 14:29:00
tags:
- studenten
title: Resetknop voor Info-PC's
---
Gebruikers konden de Info-PC’s in de centrale straat voor de [Library of
Science](http://www.ru.nl/fnwi/bibliotheek/library_of_science/) nooit
aanzetten of resetten, omdat ze in een meubel achter het scherm verstopt
zitten. Dat is nu opgelost door reset-knoppen aan te brengen aan de
zijkanten van het meubel.
