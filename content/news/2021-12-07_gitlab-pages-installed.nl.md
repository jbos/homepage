---
author: bram
date: 2021-12-07 15:29:00
tags:
- gitlab
- hosting
title: "GitLab Pages geïnstalleerd"
cover:
  image: img/2021/ssgs_pages.png
---
Recent heeft C&CZ een GitLab Pages server geinstalleerd. Met GitLab
Pages kun je statische websites publiceren rechtstreeks vanuit een
repository in [GitLab](/nl/howto/gitlab/). Hiervoor kun je een statische
site generator gebruiken, zoals Hugo, Harp, MkDocs of Hexo. Maar het is
ook mogelijk om rechtstreeks een site in HTML, CSS en JavaScript te
publiceren. Meer informatie en documentatie is te vinden in de [GitLab
Pages handleiding
(EN)](https://gitlab.science.ru.nl/help/user/project/pages/index.md).
