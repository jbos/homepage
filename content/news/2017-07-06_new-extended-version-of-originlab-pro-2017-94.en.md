---
author: petervc
date: 2017-07-06 16:23:00
tags:
- medewerkers
- studenten
title: New extended version of OriginLab (Pro, 2017, 9.4)
---
A new version (2017, 9.4) of [OriginLab](/en/howto/originlab/), software
for scientific graphing and data analysis, can be found on the
[Install](/en/howto/install-share/)-disk. See [the website of
OriginLab](http://www.originlab.com/index.aspx?go=PRODUCTS&PID=1834) for
the extras in the Pro version.The license server has been upgraded to
this version. Departments that take part in the license can request
installation and license info from C&CZ, also for standalone use.
Installation on C&CZ managed PCs still is being planned. Departments
that want to start using OriginLab should contact
[C&CZ](/en/howto/contact/).
