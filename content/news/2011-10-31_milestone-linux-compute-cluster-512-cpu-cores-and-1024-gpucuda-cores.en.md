---
author: petervc
date: 2011-10-31 15:05:00
tags:
- studenten
- medewerkers
title: 'Milestone Linux Compute Cluster: 512 CPU-cores and 1024 GPU/CUDA-cores'
---
Early 2006 a [faculty Linux compute cluster](/en/howto/hardware-servers/)
was started by the purchase of 4-core rackmountable servers by several
departments. The latest additions to the cluster, a machine with 48
CPU-cores and a machine with 12 CPU-cores and 1024 CUDA GPU-cores in 2
[NVIDIA Tesla M2090](http://en.wikipedia.org/wiki/Nvidia_Tesla) cards,
brought the size of the cluster to these milestone figures. Source code
has to be rewritten to make use of CUDA-cores. The latest version of
[Matlab](/en/howto/matlab/) has [support
for](http://www.mathworks.nl/company/newsletters/articles/gpu-programming-in-matlab.html?s_v1=32543176_1-AL7T9)
the use of CUDA-cores. For workstations and servers, including these
compute servers, Fedora Linux is gradually being replaced by [Ubuntu LTS
Linux](http://en.wikipedia.org/wiki/Ubuntu_%28operating_system%29).
