---
author: polman
date: 2014-10-10 13:08:00
tags:
- medewerkers
- studenten
title: New version of OriginLab (9.1)
---
A new version (9.1) of [OriginLab](/en/howto/originlab/), software for
scientific graphing and data analysis, can be found on the
[Install](/en/howto/install-share/)-disk. The license server has been
upgraded to this version. Departments that take part in the license can
request installation and license info from C&CZ, also for standalone
use. Installation on C&CZ managed PCs still has to be planned. A new
maintenance contract untill August 2017 has been signed with the
supplier.
