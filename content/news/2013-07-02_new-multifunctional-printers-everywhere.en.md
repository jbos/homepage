---
author: petervc
date: 2013-07-02 00:06:00
tags:
- studenten
- medewerkers
- docenten
title: New multifunctional printers everywhere
---
Starting July 1, 2013, new Konica Minolta multifunctional
printers/copiers/scanners are being placed all over the FNWI buildings.
They will replace the Ricoh machines. Read all about the change on the
[KM page](/en/howto/km/).
