---
author: wim
date: 2019-03-21 13:02:00
tags:
- medewerkers
- docenten
- studenten
title: New MFPs and printing with Science account
---
The [new Konica Minolta
MFPs](https://www.ru.nl/fb/english/print/printing-campus/) that offer
the RU-wide FollowMe printqueue, can also be [used with the Science
account](https://wiki.cncz.science.ru.nl/Peage). This only works if the
U/S number is known at C&CZ, which can be controlled on the
[Do-It-Yourself website](https://diy.science.ru.nl). On C&CZ managed
Windows PCs one can then print to the queue “FollowMeScience”, that
translates the Science-account to the U/S number and forwards the job to
the FollowMe queue. On C&CZ managed Linux hosts, this queue is called
“FollowMe”. The six year old MFPs are replaced RU-wide 1-for-1 by new
ones, including options like the bookletmaker at HG00.002.
