---
author: wim
date: 2010-08-09 09:52:00
tags:
- studenten
title: Microsoft Office 2007 installed in educational PC rooms
---
Microsoft Office 2007 has been installed on all [PC’s in the educational
PC rooms](/en/howto/terminalkamers/), replacing the previous version,
Office 2003. Help is available for the switch to Office2007:

-   A [Learning
    Guide](http://sito.science.ru.nl/LearningGuide/Publications/start/default.htm)
    for all parts of Office2007 (Word, Excel, PowerPoint, Outlook,
    Access) and also for Windows7, that can only be used on campus and
    is only available in Dutch.
-   The (Dutch) [information site of the Office 2007
    project](http://www.ru.nl/gdi/projecten/office_2007/) of the
    [[GebruikersDienst ICT](http://www.ru.nl/gdi).
