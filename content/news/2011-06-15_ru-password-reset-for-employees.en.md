---
author: pberens
date: 2011-06-15 14:19:00
tags:
- medewerkers
- docenten
title: RU password reset for employees
---
With your U-number (personnel number) and RU-password you can login to
RU webapplications. Probably you know you can ask for a password reset
via C&CZ in case you loose your RU-password. Please bring your peronnel
pass or other id in that case. Are you also aware of the possibility to
get an activationcode by sms? To enable this functionality, please once
register two questions at idm.ru.nl. More info on
[www.ru.nl/idm](http://www.ru.nl/idm/) and
[idm.ru.nl](http://idm.ru.nl). We strongly recommend this do-it-yourself
method.
