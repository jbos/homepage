---
author: petervc
date: 2008-04-04 15:42:00
title: New disk server with 90 GB partitions
---
New [disk-servers](/en/howto/hardware-servers#disk.5b-.5d.5b-.5dservers/)
are available with partitions of 90 GB that can be rented by departments
of the Faculty of Science. The older RAID-arrays were sold out, one of
them had run out of maintenance. The new servers have internal disks and
controller. The partitions of the out-of-maintenance RAID-array will be
moved to a new server.
