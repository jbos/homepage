---
author: sommel
date: 2008-04-14 14:32:00
title: Printbudget commando voor Unix
---
Op [Linux/Solaris werkplekken en login
servers](/nl/howto/hardware-servers/) bestaat het commando
**printbudget**. Hierbij zijn de budgetgroepen en het bedrag dat erop
staat op te vragen, is de standaard budgetgroep aan te passen en kan een
overzicht van de laatste printopdrachten worden getoond, net zoals via
de [Doe-Het-Zelf](https://dhz.science.ru.nl) website.
