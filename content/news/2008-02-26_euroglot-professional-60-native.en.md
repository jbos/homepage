---
author: wim
date: 2008-02-26 13:09:00
title: EuroGlot Professional 6.0 Native
---
A new version of the [EuroGlot](http://www.euroglot.nl) software is
available to be [borrowed](/en/howto/microsoft-windows/).
