---
author: polman
date: 2007-11-29 10:28:00
title: PhpMyadmin, new version because of security fixes
---
[PhpMyadmin](http://www.phpmyadmin.net) is a webapplication to maintain
mysql databases, all mysql databases hosted by C&CZ can be maintained
through <http://phpmyadmin.science.ru.nl> . The new version (11.2.2)
fixes a security leak
[1](http://www.phpmyadmin.net/home_page/security.php?issue=PMASA-2007-8).
