---
author: petervc
date: 2017-07-26 17:30:00
tags:
- medewerkers
title: Exchange account/mail-adres Voornaam.Achternaam at ru.nl voor FNWI-medewerkers
  per 1 augustus
---
Met ingang van 1 augustus krijgen alle FNWI-medewerkers die nog geen
account hebben in [Exchange (de RU-centrale mail- en
agendavoorziening)](http://www.ru.nl/ict/medewerkers/mail-agenda/) zo’n
account en daarmee een mail-adres erbij, van de vorm:
Voornaam.Achternaam\@ru.nl. Voor wie al een Exchange-account heeft, met
een mail-adres eindigend op bv @ru.nl @donders.ru.nl @fnwi.ru.nl,
verandert er niets.

In Exchange kan men agenda’s met elkaar delen, zodat het maken van
afspraken gemakkelijker wordt. Op elk van deze nieuwe @ru.nl-adressen
wordt een “redirect” gezet naar het bestaande Science-adres. Hierdoor
mist u geen mail, u krijgt er alleen een adres bij, dat alle nieuwe mail
doorstuurt naar het bestaande adres.

Wilt u gebruik maken van Exchange? De “redirect” kunt u uitzetten door
in te loggen op <https://mail.ru.nl> met het U-nummer voorafgegaan door
“RU\\” en uw RU-wachtwoord. Vervolgens vinkt u bij “Options -\> Create
an Inbox Rule” de “redirect all incoming” uit. Het doorsturen van de
Science-mail naar Exchange kan aangezet worden door in de [Doe-Het-Zelf
site](https://dhz.science.ru.nl) bij “doorsturen” het @ru.nl mail-adres
in te stellen.

N.B.1: Thunderbird-gebruikers moeten de add-on “Lightning” installeren
om vergaderverzoeken uit Exchange te kunnen zien en erop te reageren.

N.B.2: als er een redirect-regel in Exchange is, zal Exchange de mails
niet doorsturen die afkomstig zijn van het doorstuur-adres, om te
voorkomen dat mail steeds maar doorgestuurd wordt.

Heeft u hierover vragen, neem dan [contact op met
C&CZ](/nl/howto/contact/).
