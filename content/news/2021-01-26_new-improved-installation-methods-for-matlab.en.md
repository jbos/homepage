---
author: petervc
date: 2021-01-26 17:28:00
tags:
- medewerkers
- studenten
title: New improved installation methods for Matlab
---
The old installation method, used with Matlab R2020b, highlighted
several problems for some users. The solution was found in a different
installation method. Some users will prefer the new method, others the
old one. Each has its pros and cons. The new installation method makes
Matlab easy to install, also for older versions and can be done with a
small download if you only want a few toolboxes. But it requires signing
in at The MathWorks and thus has a more complex workflow. The old
installation method does not require registration and has an easy
workflow, but it always requires a large download and non-expert macOS
users will find it troublesome because the newest macOS version will
quarantine the download.
