---
author: wim
date: 2021-09-15 16:42:00
tags:
- medewerkers
- docenten
- studenten
title: Major RU network maintenance October 9 08:30-15:00
---
ISC network [announcement.](https://www.ru.nl/systeem-meldingen/)

Saturday October 9, between 08:00 and 15:00, major RU network
maintenance will be carried out. During the maintenance window there
will be interruptions of maximum 15 minutes. For the FNWI buildings, the
maintenance will end 15:00 hours, for the other RU buildings, it might
continue until 19:00 hours.
