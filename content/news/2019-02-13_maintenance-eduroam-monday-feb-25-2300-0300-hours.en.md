---
author: mkup
date: 2019-02-13 12:23:00
tags:
- medewerkers
- docenten
- studenten
title: 'Maintenance eduroam: Monday Feb 25, 23:00-03:00 hours'
---
Monday evening February 25 between 23:00-03:00 hours, there will be
maintenance on the wireless network of the RU. Eduroam will be
temporarily unavailable. Please think which processes depend on wireless
connectivity and prepare for this outage if necessary.
