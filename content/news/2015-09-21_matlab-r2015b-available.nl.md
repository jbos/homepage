---
author: petervc
date: 2015-09-21 17:07:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2015b beschikbaar
---
De nieuwste versie van [Matlab](/nl/howto/matlab/), R2015b, is
beschikbaar voor afdelingen die licenties voor het gebruik van Matlab
hebben. De software en de licentiecodes zijn via een mail naar
postmaster te krijgen voor wie daar recht op heeft. De software staat
overigens ook op de [install](/nl/tags/software)-schijf. Op alle door
C&CZ beheerde Linux machines is R2015b binnenkort beschikbaar, een
oudere versie (/opt/matlab-R2015a/bin/matlab) is nog tijdelijk te
gebruiken. Op de door C&CZ beheerde Windows-machines zal Matlab tijdens
het semester niet van versie veranderen om versieafhankelijkheden bij
lopende colleges te voorkomen. R2015b is de allerlaatste versie voor
32-bit Windows. R2016a zal wel 64-bit Windows ondersteunen, maar niet
32-bit Windows.
