---
author: petervc
date: 2008-01-28 18:14:00
title: Surfgroepen to cooperate with others
---
To cooperate (share files etc.) with other employees and students of
other universities [Surfgroepen](http://www.surfgroepen.nl) can be used
for free, even external persons can join.
