---
author: petervc
date: 2021-08-17 16:47:00
title: PCs in Study Area and Library of Science
---
Since the summer of 2021, the computers in the Faculty of Science Study
Area (HG00.201) and the Library of Science (HG00.011) are no longer
being managed by C&CZ, but by the [ILS](/en/howto/contact/#ils-helpdesk).
makes all Windows computers in all RU Study Areas and Library locations
uniform. General information about the Library of Science (such as opening hours
and reservation of a study place), can be found at [the Library of Science website](https://www.ru.nl/library/library/library-locations/library-science/).
In addition to these Windows-only computers, eight Linux computers,
that are managed by C&CZ, are available in the front of the Study Area
(HG00.201).
