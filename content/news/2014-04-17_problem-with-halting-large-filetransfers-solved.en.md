---
author: mkup
date: 2014-04-17 17:15:00
tags:
- studenten
- medewerkers
title: Problem with halting large filetransfers solved
---
Next Thursday morning, April 24 the [ISC network
admins](http://www.ru.nl/isc) will change a setting in the RU-firewall,
as a result of a few reported problems with halting transfers of large
data files. The firewall does [TCP Sequence Number
Randomization](http://www.cisco.com/c/en/us/td/docs/security/fwsm/fwsm31/configuration/guide/fwsm_cfg/protct_f.html)
for security reasons. Therefore [TCP
SACK](http://en.wikipedia.org/wiki/Transmission_Control_Protocol#Selective_acknowledgments)
will be disabled in the firewall, which makes modern hosts that use SACK
no longer having halted connections through the firewall.
