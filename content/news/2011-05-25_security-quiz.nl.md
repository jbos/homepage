---
author: petervc
date: 2011-05-25 18:07:00
tags:
- studenten
- medewerkers
title: Security Quiz
---
Voor wie wil weten, hoe slim men is met betrekking tot ICT-beveiliging,
is de Engelstalige [Security Quiz](http://securityquiz.wlu.ca/) van de
Canadese Wilfrid Laurier University een aanrader. Lukt het om daar een
100% score te halen? Een (eveneens Engelstalige) [quiz met directe
feedback](http://security.gmu.edu/quiz/question1.html) is te vinden bij
George Mason University] uit de VS.
