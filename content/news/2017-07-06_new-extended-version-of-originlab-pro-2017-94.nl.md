---
author: petervc
date: 2017-07-06 16:23:00
tags:
- medewerkers
- studenten
title: Nieuwe uitgebreidere versie van OriginLab (Pro, 2017, 9.4)
---
Er is een nieuwe en uitgebreidere versie (Pro, 2017, 9.4) van
[OriginLab](/nl/howto/originlab/), software voor wetenschappelijke
grafieken en data-analyse,beschikbaar op de
[Install](/nl/howto/install-share/)-schijf. Zie [de website van
OriginLab](http://www.originlab.com/index.aspx?go=PRODUCTS&PID=1834)
voor de extra’s in de Pro-versie. De licentieserver heeft een update
naar deze versie gehad. Afdelingen die meebetalen aan de licentie kunnen
bij C&CZ de licentie- en installatie-informatie krijgen, ook voor
standalone gebruik. Installatie op door C&CZ beheerde PC’s wordt
gepland. Afdelingen die OriginLab willen gaan gebruiken, kunnen zich
melden bij [C&CZ](/nl/howto/contact/).
