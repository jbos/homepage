---
author: polman
date: 2014-09-19 11:22:00
tags:
- medewerkers
- studenten
title: Compute clusters today
---
With the purchase of a.o. a server with 3072 GB RAM by
[ICIS](http://www.ru.nl/icis/) with funds from the
[RRF](http://www.radboudresearchfacilities.nl), the C&CZ-managed cn
compute cluster has grown to 1324 cores and 7914 GB RAM. The
Astrophysics coma cluster, with 320 cores, 240 TB fileserver storage,
GPU’s and fast [Infiniband](http://en.wikipedia.org/wiki/InfiniBand)
interconnect is a separate cluster. For more info, see the [cn compute
cluster](/en/howto/hardware-servers/) page.
