---
author: petervc
date: 2014-03-21 14:10:00
tags:
- medewerkers
- docenten
title: XP support ends on April 8
---
As [announced before](/en/news/), from April 8 [Microsoft will no
longer support Windows
XP](http://www.microsoft.com/en-us/windows/enterprise/endofsupport.aspx).
Therefore C&CZ urges everyone to change to a different operating system
before that date, if possible. XP computers that cannot be upgraded,
e.g. because they are attached to a measuring device which requires XP,
can be moved to a shielded part of the network. PC’s can be upgraded to
a C&CZ managed [Windows 7](/en/howto/windows-beheerde-werkplek/) and/or
Ubuntu 12.04 PC. On a Windows7 managed PC, a department can have local
administrator (localadmin) rights. One can also ask C&CZ to just
[install Windows 7 and/or Ubuntu 12.04 on a
PC](/en/howto/windows-beheerde-werkplek/), after which the user can
manage the PC her/himself. Please [contact the C&CZ
helpdesk](/en/howto/contact/) if you want to make use of this
possibility. Please note that if a PC doesn’t have the [required
hardware](/en/howto/windows-beheerde-werkplek/), one will have to buy new
hardware, e.g. a memory upgrade or a new PC entirely.
