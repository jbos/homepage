---
author: petervc
date: 2021-12-15 16:13:00
tags:
- studenten
- medewerkers
- docenten
title: Malicious network traffic will be blocked
---
In the process of handling the
[Log4Shell](https://en.wikipedia.org/wiki/Log4Shell) vulnerability, it
was decided to use the [Intrusion
Protection](https://en.wikipedia.org/wiki/Intrusion_detection_system) of
the FortiNet firewall for all RU network traffic. Don’t hesitate to
[contatc](/en/howto/contact/) us if you experience drawbacks.
