---
author: petervc
date: 2015-08-28 11:01:00
tags:
- studenten
- medewerkers
- docenten
title: Online Matlab course
---
If you want to learn Matlab interactively, at your own pace, visit [the
Matlab Academy](https://matlabacademy.mathworks.com/) and create an
account with an email address ending in: @science.ru.nl,
@student.science.ru.nl, @donders.ru.nl, @pwo.ru.nl, @let.ru.nl,
@fm.ru.nl, @ai.ru.nl, @socsci.ru.nl, @ru.nl or @student.ru.nl and use
the Activation Key that you can get from [C&CZ](/en/howto/contact/) if
your department takes part in the campus [Matlab](/en/howto/matlab/)
license.
