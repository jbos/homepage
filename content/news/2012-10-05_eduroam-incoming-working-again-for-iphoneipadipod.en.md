---
author: mkup
date: 2012-10-05 17:48:00
title: Eduroam incoming working again for iPhone/iPad/iPod
---
The [UCI network management](http://www.ru.nl/uci) reports that the
[problem](/en/cpk/) with the
[incoming](http://www.ru.nl/gdi/voorzieningen/campusbrede-systemen/eduroam/)
version of [Eduroam](http://www.eduroam.nl) for iPhone/iPad/iPod users
has been resolved. Eduroam incoming means that one uses the wireless
network of a remote institute, with authentication (login/password)
being checked by RU or Science.
