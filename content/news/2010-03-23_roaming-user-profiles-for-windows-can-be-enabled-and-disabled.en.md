---
author: sommel
date: 2010-03-23 15:48:00
title: Roaming user profiles for Windows can be enabled and disabled
---
On the [Do-It-Yourself](http://diy.science.ru.nl)-website it is possible
to enable and disable [roaming user
profiles](http://en.wikipedia.org/wiki/Roaming_user_profile) for the
domains NWI and B-FAC. The default is to use roaming profiles for the
NWI domain and not for the B-FAC domain. In general logging on and off
will be a little bit faster without using a roaming user profile, but a
disadvantage is that user settings will not be saved. Examples of user
settings that wil not be saved are the Desktop backgound image,
documents stored on the Desktop, Email settings, ‘Favorites’, ‘Recent
Documents’, etc. Without a roaming profile documents that are placed on
the desktop will not be saved!
