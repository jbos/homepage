---
author: petervc
date: 2008-06-16 16:59:00
title: 'Matlab: TIMEOUTALL introduced, installation procedure changed'
---
After an idea from [FC Donders](http://www.ru.nl/fcdonders), C&CZ
changed the options for the [Matlab](/en/howto/matlab/) license server to
include a ‘TIMEOUTALL 3600’. This has the advantage that an inactive
user can hold a license for Matlab or a toolbox for 1 hour at maximum,
but could cause problems sometimes for the many tens of users within
Radboud Unicversity and Hospital, because it can happen that a
long-running Matlab job stops because there is currently no license
available.

With Matlab version R2008a the [Matlab installation
procedure](/en/howto/matlab/) has been changed.
