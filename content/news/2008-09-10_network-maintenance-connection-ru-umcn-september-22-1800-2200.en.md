---
author: petervc
date: 2008-09-10 15:49:00
title: Network maintenance connection RU-UMCN September 22 18:00-22:00
---
September 22 between 18:00-22:00 hours the network connection between RU
and UMCN St. Radboud will be upgraded to 1 Gigabit per second, which
will cause problems withthat connection during that time period.
