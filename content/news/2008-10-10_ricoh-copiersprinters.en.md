---
author: theon
date: 2008-10-10 15:35:00
title: Ricoh copiers/printers
---
Because the [Ricoh copiers/printers](/en/howto/copiers/) have been used
for half a year now and soon a new machine will be bought, C&CZ likes to
hear wishes/problems from within the Faculty of Science. Please address
these to the [C&CZ helpdesk](/en/howto/helpdesk/).
