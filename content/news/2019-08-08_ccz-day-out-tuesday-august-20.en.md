---
author: petervc
date: 2019-08-08 12:42:00
tags:
- medewerkers
- studenten
title: 'C&CZ day out: Tuesday August 20'
---
The C&CZ yearly day out is scheduled for Tuesday August 20. C&CZ can be
reached in case of serious disruptions of services.
