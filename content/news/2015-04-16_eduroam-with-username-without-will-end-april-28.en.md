---
author: mkup
date: 2015-04-16 16:55:00
tags:
- medewerkers
- docenten
- studenten
title: Eduroam with username without @ will end April 28
---
As of April 28, it is no longer possible to use the [wireless Eduroam
network](http://www.ru.nl/ict-uk/students/wifi/) with only a U- or
S-number as username, without appending the domain @ru.nl. Until then,
this works, but only on RU campus, because the RU [RADIUS
server](http://en.wikipedia.org/wiki/RADIUS) then assumed the RADIUS
realm @ru.nl. When one uses a username with a RADIUS realm like @ru.nl
or @science.ru.nl, one can use Eduroam at [all Eduroam connected
institutions](https://www.eduroam.nl/organisations.php). How to add the
RADIUS realm @ru.nl to your Eduroam settings can be found in the [ISC
announcement](http://www.radboudnet.nl/english/radboud_university/news-agenda/news/@991420/wi-fi-on-campus-only/).
