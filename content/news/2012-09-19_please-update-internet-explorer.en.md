---
author: petervc
date: 2012-09-19 12:11:00
tags:
- studenten
- medewerkers
title: Please update Internet Explorer\!
---
Update September 24: Microsoft released an [update for Internet
Explorer](https://technet.microsoft.com/en-us/security/bulletin/ms12-063).
We urge you to install this update before using Internet Explorer to
browse the Internet.

September 19: Due to a [new vulnerability in Internet Explorer (versions
6 through 9](http://www.f-secure.com/weblog/archives/00002429.html),it
is advised not to use Internet Explorer to browse the Internet. Visiting
not-contaminated websites within ru.nl of course is still possible. Use
an alternative like Google Chrome or Mozilla Firefox. Without
installation one can use [Firefox
Portable](http://portableapps.com/apps/internet/firefox_portable).
