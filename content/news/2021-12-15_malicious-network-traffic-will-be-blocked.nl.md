---
author: petervc
date: 2021-12-15 16:13:00
tags:
- studenten
- medewerkers
- docenten
title: Kwaadaardig netwerkverkeer wordt geblokkeerd
---
Met als directe aanleiding de
[Log4Shell](https://nl.wikipedia.org/wiki/Log4Shell) kwetsbaarheid is
besloten om voor de hele RU de [Intrusion
Protection](https://nl.wikipedia.org/wiki/Intrusion_detection_system)
van [FortiGuard](https://www.fortiguard.com/) op de FortiNet firewall in
te schakelen. Dit betekent een extra filter op alle netwerkverkeer.
Mocht u daar nadelen van ondervinden, aarzel dan niet om
[contact](/nl/howto/contact/) op te nemen.
