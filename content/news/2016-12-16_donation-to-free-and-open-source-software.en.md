---
author: petervc
date: 2016-12-16 16:30:00
tags:
- studenten
- medewerkers
- docenten
title: Donation to free and open source software
---
For the majority of the services C&CZ uses [free
software](https://en.wikipedia.org/wiki/Free_software) and [open source
software](https://en.wikipedia.org/wiki/Open-source_software).
Therefore, some time ago the idea emerged that the C&CZ employees would
vote on which projects would receive a donation of C&CZ. This year the
[Free Software Foundation
(FSF)](https://en.wikipedia.org/wiki/Free_Software_Foundation) and
[Limesurvey](https://www.limesurvey.org/) were chosen. The FSF supports
GNU/Linux, which is used on almost all servers maintained by C&CZ and on
many hundreds of workstations, including the [computer
labs](/en/howto/terminalkamers/). LimeSurvey is used for conducting
surveys since 2008. Since the end of 2011, it is used for all ca. 500
course evaluations of the Faculty of Science per year. In addition, it
is also used by departments (as PUC, ISIS and the Education Center) and
individual employees and students of the Faculty of Science. For more
info, see our [LimeSurvey](/en/howto/limesurvey/) page.
