---
author: polman
date: 2018-06-20 16:15:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2018
---
The most recent version of the [TeX
Collection](http://www.tug.org/texcollection), 2018, is available for
MS-Windows, Mac OS X and Linux. It can be found on the
[Install](/en/howto/install-share/) network share.
