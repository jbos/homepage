---
author: polman
date: 2014-11-22 17:16:00
tags:
- medewerkers
- studenten
title: Test van Ubuntu 16.04 met Linux loginserver lilo5
---
Als test voor de nieuwe versie van Ubuntu, 16.04 LTS, hebben we een
nieuwe loginserver beschikbaar, onder de naam `lilo5.science.ru.nl`.
Over enige tijd zal deze de vijf jaar oude loginserver `lilo3`
vervangen. De naam `lilo`, die altijd naar de nieuwste/snelste
loginserver wijst, zal pas dan omgezet worden van de twee jaar oude
`lilo4` naar de nieuwe `lilo5`. De nieuwe `lilo5` is een Dell PowerEdge
R430 met 2 Xeon E5-2630L v4 10-core processoren en 64 GB geheugen. Met
hyperthreading aan lijkt dit dus een 40-processor machine. Voor wie de
identiteit van deze server wil controleren alvorens het Science
wachtwoord aan de nieuwe server te geven:
`ECDSA key fingerprint is 7d:08:27:61:ac:c8:f1:05:4f:c9:91:2a:83:e5:c3:4f.`.
Alle problemen met deze nieuwe versie van Ubuntu Linux [graag
melden](/nl/howto/contact/).
