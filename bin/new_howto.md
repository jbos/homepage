---
title: $title
author: $author
date: $date
keywords: []
tags: []
cover:
  image: img/$year/$slug.png
ShowToc: false
TocOpen: false
---
Het artikel komt hier.
> denk ook aan de vertaling!

Some example elements:

1. [a link to wikipedia](https://www.wikipedia.org)
1. you can [contact](../contact) us!
1. or link to [another paragraph](#h2) in the same article
1. ![alt text](/img/$year/image.png)


# H1
Some blocks to get some attention:

{{< notice tip >}}
If you're writing a lenghty article, turn on the table of contents! Set `ShowToc` in frontmatter to `true`.
{{< / notice >}}

Or maybe warn about something:

{{< notice warning >}}
Een gewaarschuwd mens, telt voor `10`
{{< / notice >}}

All notice types: `warning`, `info`, `note`, `tip`

## H2

A table:
| header | another header | right alignment | centered |
| ------ | -------------- | --------------: | :------: |
| row1   |                |              aa |   wow    |

### H3

Linken naar de profielpagina van een auteur kan zo: {{< author bram>}}.

#### H4

Geef bij een code-blokje aan welke taal het is:

``` go
package main

import (
	"fmt"
	"log"
	"os"
)
```