# C&CZ Homepage

Deze site werkt met hugo (apt-get install). En kan getest worden met `make test` er loopt dan een
test site op `localhost:1313`.

## TLDR

Maak nieuwe content met:

```
$ bin/new_cpk Network downtime announcement
$ bin/new_news Core router FNWI upgraded
$ bin/new_howto How to connect to the new Terrabit network
```

In de uitvoer staan de namen van de aangemaakte Markdown-bestanden. Wat er te raden viel, staat al
in de nieuwe content-files, de inhoud moet je zelf nog even typen.

## Initialisatie

In je eigen branch, maak een .env file:

```
cp .env.dist .env
```

En pas de waardes aan.

Content van `.env.dist`:

```
# your name as used in front matter and data/cncz.toml
AUTHOR=jenaam

# where to put cpk mail text: 'thunderbird', 'clipboard' or 'stdout'
MAILER=thunderbird
```

## Front Matter

* `noage = true`
  Berichten (nieuws/howto) worden na 1825 dagen (5 jaar) (de waarde van data/cncz.toml, `ageDays` key)
  gemarkeerd als out-of-data. Mocht je dat _niet_ willen moet in de front matter `noage: true`
  zetten.
* `ShowToc = true`
  Heb je een lange post met veel kopjes dan kun je deze optie aanzetten zodat er een (uitklapbare)
  inhouds opgave wordt getoond.

### CPKs

Nieuwe CPKs (C&CZ Probleem Kaart) hebben veel extra metadata in de front matter, veel van die zaken
kunnen gezet worden, hier worden ze gedocumenteerd. Note dat als een CPK _geen_ einddatum heeft
`cpk_end` het wordt gezien als een actieve storing. Dit geldt ook voor onderhoud; je zult dus dan
twee keer de CPK moeten editen.

| metadata       | waarde                | beschrijving                                                                                                                   |
| -------------- | --------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| `onderhoud`    | `true`, `false`       | dit is een onderhoud CPK, die iets anders gestijld  wordt dan een problem.  Vooralsnog krijgt deze melding een ander icoontje. |
| `cpk_begin`    | `YYYY-MM-DD hh:mm:ss` | begin van de verstoring/onderhoud                                                                                              |
| `cpk_end`      | `YYYY-MM-DD hh:mm:ss` | eind van de verstoring/onderhoud. Als deze wordt weggelaten, staat de cpk als actieve storing vermeld.                         |
| `cpk_affected` | text                  | hier kan ook markdown worden gebruikt.                                                                                         |

#### CPK versturen

Nadat je de CPK hebt aangemaakt, verstuur 'm met `mailcpk` (eventueel met het cpknummer erachter). Maar maak eerst een 
bestandje `.mailer` waarin staat hoe je de mail wilt versturen. De inhoud kan zijn: `thunderbird`, `clipboard` of `stdout`.

## Berichten

Berichten worden in markdown geschreven. De frontmatter is in yaml (of toml) maar we gebruiken nu
yaml.

Downloads horen in `static/downloads/<jaar>` en afbeeldingen in `static/img/<jaar>`. Afbeeldingen kunnen
met een shortcode in een post worden gezet, zie
<https://gohugo.io/content-management/shortcodes/#figure>

Auteurs en gebruikers kunnen met een 'author' shortcode worden benoemd: `{{< author simon >}}`, de
bestaande *authors* staan in data/cncz.toml. Auteurs in de frontmatter komen na: `authors`, meerdere
auteurs per bericht wordt ondersteund.

Blokken die extra aandacht moeten trekken kunnen met de [notice
shortcode](https://github.com/martignoni/hugo-notice) worden gemaakt:

    {{< notice warning >}}
    This is a warning notice. Be warned!
    {{< /notice >}}

Waarbij: info, warning, note en info worden ondersteund.

Includen van andere files kan met de *include* shortcode: `{{<include example.md>}}`

Super- en subscript is niet ondersteund in the markdown versie die Hugo gebruikt, je kunt daar
omheen werken met de de shortcodes `sup` and `sub`, ala: `H{{< sub 2 >}}O` - wat ietwat onhandig
is.

## Mirror Site

Een van de ideeën achter de verhuizing naar hugo is om een site achter de hand te hebben als de
hoofd site eruit ligt; deze `home.cncz.nl` haalt het (publieke) git repo
<https://gitlab.science.ru.nl/cncz/homepage> op en bouwt dan de site met
`hugo -b home.cncz.nl -d <www-dir>` [TBD].

De `<www-dir>` is waar de site's HTML staat, deze wordt geserveerd met *apache*(?). [TBD]. Elke
*xx minuten* wordt via CRON [TBD] een nieuwe *pull* gedaan en een nieuwe website gemaakt.

### Updaten van de mirror site

Log in op de machine en maak rechtstreeks een edit in het git repo en roep dan `make mirror` aan.

## Notes

### Afbeeldingen
Afbeeldingen worden 720 pixels breed afgebeeld. Zorg dat je de plaatjes met deze breedte aanlevert. Met
imagemagick kun je in 1 klap plaatjes breder dan 720 pixels verkleinen:

``` console
./bin/resize_images static/img/2022/*
```

### Pagina hernoemen?
Bij hernoemen van een pagina is het handig om de bestandsnaam van de oude pagina op te nemen in de
`aliases` van de front-matter. Bijvoorbeeld de pagina `phone-and-mail-guide` bevat bovenin:

``` yaml
---
aliases:
- /en/howto/fnwi-telefoon-en-e-mail-gids
---
```
Dit mag overigens ook een relatieve link zijn:
``` yaml
---
aliases:
- fnwi-telefoon-en-e-mail-gids
---
```

Waardoor deze ook via de oude link te benaderen is. De bezoeker wordt geredirect naar de nieuwe pagina.

### Link checking

Kan goed met [Lychee](https://lychee.cli.rs).

Run lychee on the content:

```
lychee content
```

List all links still pointing to the wiki

```
lychee --dump content | grep wiki.science
```
